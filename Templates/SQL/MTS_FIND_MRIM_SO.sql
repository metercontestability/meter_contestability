select SERVICEPOINT 
from (
select ss.SERVICEPOINT 
    from SERVICEPOINT_STATUS ss
    join STATUS s on ss.STATUS_ID = s.ID
    join SERVICEPOINT_REF_ATTRIBUTE sra on sra.SERVICEPOINT = ss.SERVICEPOINT
    join SERVICEPOINT_MP_ROLE smr on smr.SERVICEPOINT = ss.SERVICEPOINT 
    join SERVICEPOINT_MP_ROLE smr2 on smr2.SERVICEPOINT = ss.SERVICEPOINT
    where s.NAME = 'A'  
      and ss.END_DT > sysdate
      and sra.ATTRIBUTE_TYPE_ID = 1 
      and sra.VALUE = 'MRIM'
      and sra.END_DT > sysdate
      and 0 = (select count(*)      
               from SERVICEPOINT_REF_ATTRIBUTE sra_2
               where sra_2.SERVICEPOINT = ss.SERVICEPOINT
                 and sra_2.ATTRIBUTE_TYPE_ID = 2
                 and sra_2.VALUE = 'RWD'
                 and sra_2.END_DT > sysdate)
      and smr.ROLE_ID = 1       
      and smr.END_DT > sysdate
      and smr2.ROLE_ID = 13     
      and smr2.END_DT > sysdate
      and ss.SERVICEPOINT not in (select sc.SERVICEPOINT
                                  from SPECIAL_CONDITION sc
                                  where sc.END_DT > sysdate
                                  union
                                  select sh.NMI || sh.NMI_CHECKSUM
                                  from B2B_SERVICE_ORDER_HEADER sh
                                  join B2B_SERVICE_ORDER so on so.SERVICE_ORDER_HEADER_ID = sh.ID
                                  join TRANSACTION_STATUS ts on ts.TRANSACTION_DETAIL_ID = so.TRANSACTION_DETAIL_ID
                                  join STATUS st on st.ID = ts.STATUS_ID
                                  where 1=1
                                    and sh.NMI = substr(ss.SERVICEPOINT,1,10)
                                    and st.BUSINESS_STATUS in ('EXTERNAL', 'AQ', 'ERROR', 'RAISED'))
      and ss.SERVICEPOINT not in 
          (select NMI FROM TEST_DATA_KEY_REGISTER 
            WHERE IS_RETIRED_YN = 'N'
                AND IS_IN_USE_YN  = 'Y') /* the sub-query returns a row if the NMI is in use */
      and rownum <= 1)