SELECT 
CASE WHEN A.USER_DATA.TEXT_LOB LIKE '%<ReturnCode>0</ReturnCode>%'
       THEN 'PASS' ELSE 'FAIL' END AS OUTPUT
       FROM CIS_Q_TABLE A
       WHERE 1=1
       AND Q_NAME LIKE 'CIS_%'       
       AND CORRID = '<param_correlation_id>'