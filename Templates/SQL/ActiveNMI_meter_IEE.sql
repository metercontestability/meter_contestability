select SERVICEPOINTID as NMI
from (
    select fcp_1.SERVICEPOINTID
    from (
        select distinct fcp.SERVICEPOINTID
             , count(distinct met.METERID) as METER_COUNT
            from FLATCONFIGPHYSICAL fcp
           , METER met
           , ENTITYSTATUS es
           , VALIDATIONSET vs
        where fcp.MTRSTATUS = 'A'       
          and fcp.MTRCHNLTYPE = 'I'     
          and fcp.EFFECTIVEENDDATE > sysdate
          and fcp.METERID = met.METERID
          and met.EFFECTIVEENDDATE > sysdate
          and met.ENTITYSTATUSKEY = es.ENTITYSTATUSKEY
          and es.STATUSID = 'Active'
          and fcp.validationsetkey = vs.validationsetkey
          and vs.validationsetid like ('<VALIDATION_SET_ID>') and fcp.SERVICEPOINTID NOT IN (<NMI_LIST_FROM_KDR>)                        group by fcp.SERVICEPOINTID
    ) nmi_det
    , FLATCONFIGPHYSICAL fcp_1    
        where nmi_det.SERVICEPOINTID = fcp_1.SERVICEPOINTID
      and nmi_det.METER_COUNT = 1
      and fcp_1.MTRSTATUS = 'A'
      and fcp_1.MTRCHNLTYPE = 'I'
            and rownum < 2)