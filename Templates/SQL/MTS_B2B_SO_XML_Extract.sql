SELECT 
 EXTRACTVALUE(xmltype(CONTENTS), '*/Header/From') as FROM_PARTICIPANT
, EXTRACTVALUE(xmltype(CONTENTS), '*/Header/From') as XML_EXTRAC_FROM
, EXTRACTVALUE(xmltype(CONTENTS), '*/Header/To') as XML_EXTRAC_TO
, EXTRACTVALUE(xmltype(CONTENTS), '*/Header/MessageID') as MessageID
, EXTRACTVALUE(xmltype(CONTENTS), '*/Header/MessageDate') as MessageDate
, EXTRACTVALUE(xmltype(CONTENTS), '*/Transactions/Transaction/@transactionID') as transactionID
, (SUBSTR(EXTRACTVALUE (xmltype (CONTENTS),'*/Transactions/Transaction/@transactionDate'),1,10)) AS XML_EXTRAC_Date
, EXTRACTVALUE(xmltype(CONTENTS), '*/Transactions/Transaction/ServiceOrderRequest/@actionType') as XML_EXTRAC_ACTION
, EXTRACTVALUE(xmltype(CONTENTS), '*/Transactions/Transaction/ServiceOrderRequest/ServiceOrder/ServiceOrderNumber') as ServiceOrderNumber
, EXTRACTVALUE(xmltype(CONTENTS), '*/Transactions/Transaction/ServiceOrderRequest/ServiceOrder/ServiceOrderType/WorkType') as XML_EXTRAC_SO_TYPE
, EXTRACTVALUE(xmltype(CONTENTS), '*/Transactions/Transaction/ServiceOrderRequest/ServiceOrder/ServiceOrderType/WorkType/@workSubType') as XML_EXTRAC_SO_SUB_TYPE
, EXTRACTVALUE(xmltype(CONTENTS), '*/Transactions/Transaction/ServiceOrderRequest/RequestData/ServiceTime') as ServiceTime
from GW_DOCUMENT_CONTENTS GDC
LEFT JOIN GW_FILE GF ON GF.DOCUMENT_ID = GDC.DOCUMENT_ID
LEFT JOIN GW_MESSAGE   GM on GM.DOCUMENT_ID = GDC.DOCUMENT_ID
where 1=1
    and GM.INIT_MESSAGE_ID ='<INIT_TRANSACTION_ID>' 
    and GF.FILE_TYPE_ID = 1
    and GF.FILE_STATUS_ID = 1
    and GM.MESSAGE_DT > sysdate - 1
    and rownum < 2