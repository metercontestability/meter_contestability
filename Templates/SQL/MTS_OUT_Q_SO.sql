'-- check MTS outbound queue
select sq.ENQ_UID, sq.ENQ_TID, sq.ENQ_TIME
, sq.DEQ_UID, sq.DEQ_TID, sq.DEQ_TIME
, sq.DEQUEUE_MSGID, sq.MSGID
, sq.CORRID
,sq.Q_NAME
, sq.SENDER_NAME, sq.STATE
, sq.user_data.payload_data as xml_PayloadData
from mts.SQT_INTERFACE_OUT sq
where sq.ENQ_TIME >= to_date('<SubmittedAfterDt_dd-mmm-ccyyy>')
and instr(sq.user_data.payload_data.getclobval(), '<NMI>')>0 
--AND CORRID = '233321932'
order by sq.enq_time desc;