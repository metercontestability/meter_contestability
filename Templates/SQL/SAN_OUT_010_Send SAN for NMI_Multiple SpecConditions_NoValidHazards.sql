WITH RECEIVED_TODAY
     AS (
        SELECT DISTINCT SAR.NMI || SAR.NMI_CHECKSUM as NMI
        FROM SITE_ACCESS_REQ SAR, TRANSACTION_DETAIL TD
        WHERE SAR.TRANSACTION_DETAIL_ID = TD.ID
            AND TD.GW_FILE_TIMESTAMP > TRUNC(SYSDATE)
        ),
     RESERVED_NMIS
     AS (
        SELECT DISTINCT NMI
        FROM TEST_DATA_KEY_REGISTER TDR
        WHERE   (TDR.IS_RETIRED_YN = 'Y'
            or TDR.IS_IN_USE_YN  = 'Y'
            or   TDR.IS_RESERVED_YN ='Y')
       AND TDR.CREATED_TS > TRUNC (SYSDATE)
        ),
     NMI_DETAILS
     AS (
        SELECT SS.SERVICEPOINT as NMI
        FROM STATUS ST,
            SERVICEPOINT_STATUS SS,
            SERVICEPOINT_REF_ATTRIBUTE SRA,
            SERVICEPOINT_REF_ATTRIBUTE SRA2
        WHERE SS.STATUS_ID = ST.ID
            AND ST.NAME = 'A'
            AND SS.END_DT > SYSDATE
            AND SRA.SERVICEPOINT = SS.SERVICEPOINT
            AND SRA.VALUE =  '<Meter_Type>'
            AND SRA.END_DT > SYSDATE
            AND SRA2.SERVICEPOINT = SS.SERVICEPOINT
            AND SRA2.VALUE = '<NMI_Size>'
            AND SRA2.END_DT > SYSDATE
        MINUS SELECT NMI FROM RECEIVED_TODAY
        MINUS SELECT NMI FROM RESERVED_NMIS
        ),
     NMI_TO_ROLE
     AS (
        SELECT MP.PARTICIPANT_NAME as MP_TO, R.NAME, MP.ID as MP_ID, r.id as R_ID
        FROM CONFIGURATION_VARIABLE CV,
             ORGANISATION ORG,
             ORGANISATION_PARTICIPANT ORG_PARTI,
             MARKET_PARTICIPANT MP,
             MARKET_PARTICIPANT_ROLE MPR,
             ROLE R
        WHERE  CV.NAME = 'Organisation'
            AND    ORG.NAME = CV.VALUE
            AND    ORG_PARTI.ORGANISATION_ID = ORG.ID
            AND    ORG_PARTI.PARTICIPANT_ID = MP.ID
            AND  MP.ID = MPR.MARKET_PARTICIPANT_ID
            AND  MPR.ROLE_ID = R.ID
            AND R.NAME = '<To_Role>'
        ),
     NMI_FROM_ROLE
     AS (
        SELECT
            NMI,
            MP.PARTICIPANT_NAME as MP_FROM, RO.NAME as FROM_ROLE
            , NTR.MP_TO       
            , '2004-10-01T00:00:00+10:00' AS LASTMODDATE
			, '01/10/2004 00:00:00' AS LASTMODDATEGUI			
        FROM
            NMI_DETAILS ND,
            NMI_TO_ROLE NTR,
            SERVICEPOINT_MP_ROLE SMR,
            SERVICEPOINT_MP_ROLE SMR2,
            MARKET_PARTICIPANT MP,
            ROLE RO,            
            SPECIAL_CONDITION SC,
            CIS_SPECIAL_CONDITION_CODE SCC
        WHERE SMR.SERVICEPOINT = ND.NMI
            AND SMR2.SERVICEPOINT = SMR.SERVICEPOINT
            AND SMR2.MARKET_PARTICIPANT_ID = NTR.MP_ID
            and SMR2.ROLE_ID = NTR.R_ID
            and SMR2.end_dt > SYSDATE
            AND SMR.END_DT > SYSDATE
            AND SMR.MARKET_PARTICIPANT_ID = MP.ID
            AND RO.ID = SMR.ROLE_ID
            AND RO.NAME = '<From_Role>'
            AND ND.NMI NOT IN (SELECT SAD.SERVICEPOINT
                                 FROM SITE_ACCESS_DETAILS SAD                    
                               )               
            AND (SELECT COUNT(1)
                   FROM SPECIAL_CONDITION SCA,   
                        CIS_SPECIAL_CONDITION_CODE SCCA            
                  WHERE SCA.SERVICEPOINT = ND.NMI
                    AND SCA.CODE = SCCA.SPECIAL_CONDITION_CODE  
                    AND SCCA.HAZARD_YN = 'Y'
                    AND SCA.END_DT > SYSDATE
                    ) = 0        
            AND (SELECT COUNT(1)
                   FROM SPECIAL_CONDITION SCA,   
                        CIS_SPECIAL_CONDITION_CODE SCCA            
                  WHERE SCA.SERVICEPOINT = ND.NMI
                    AND SCA.CODE = SCCA.SPECIAL_CONDITION_CODE  
                    AND SCCA.HAZARD_YN = 'N'
                    AND SCA.END_DT > SYSDATE
                    ) > 0                      
         AND MP.ID not in (  select distinct MP.ID FROM CONFIGURATION_VARIABLE CV,
                ORGANISATION ORG,
                ORGANISATION_PARTICIPANT ORG_PARTI,
                MARKET_PARTICIPANT MP,
                MARKET_PARTICIPANT_ROLE MPR,
                ROLE R
          WHERE     CV.NAME = 'Organisation'
                AND ORG.NAME = CV.VALUE
                AND ORG_PARTI.ORGANISATION_ID = ORG.ID
                AND ORG_PARTI.PARTICIPANT_ID = MP.ID
                AND MP.ID = MPR.MARKET_PARTICIPANT_ID
                AND MPR.ROLE_ID = R.ID)
         )
SELECT NFR.NMI as NMI, NFR.MP_FROM as FROM_XML, NFR.MP_TO as TO_XML, 'Customer reports no access requirements' AS ACCESS_DETAILS,'Customer Reports No Hazard' as HAZARD,NFR.LASTMODDATE AS LASTMODIFIEDDATE,LASTMODDATEGUI
FROM  NMI_FROM_ROLE NFR
WHERE 1=1
AND ROWNUM <= 1