select replace(rtrim(xmlagg(xmlelement(NMI, ':'||SERVICEPOINTID||':,')).extract('//text()'),','),':',chr(39)) as NMI
from (
    select DISTINCT Fcp_1.SERVICEPOINTID
    from (
         select distinct fcp.SERVICEPOINTID
             , count(distinct met.METERID) as METER_COUNT
            from FLATCONFIGPHYSICAL fcp
           , METER met
           , ENTITYSTATUS es
           , VALIDATIONSET vs
        where fcp.MTRSTATUS in 'I'    
          and fcp.MTRCHNLTYPE = 'I'     
          and fcp.METERID = met.METERID
          and met.ENTITYSTATUSKEY = es.ENTITYSTATUSKEY
          and es.STATUSID = <'MeterStatusin IEE'>
          and fcp.validationsetkey = vs.validationsetkey
          and vs.validationsetid like ('Type5%')   ----'<Derive Validation Set based on Meter Type from the Driver sheet. If E4 - Use Type5 If E2- use Type4  
     -- and fcp.SERVICEPOINTID NOT IN (Select NMI from TEST_DATA_KEY_REGISTER)                          
        group by fcp.SERVICEPOINTID
    ) nmi_det
    , FLATCONFIGPHYSICAL fcp_1    
     where nmi_det.SERVICEPOINTID = fcp_1.SERVICEPOINTID
      and nmi_det.METER_COUNT = 1 --<'Number of Meters'>
      and fcp_1.MTRCHNLTYPE = 'I'
       and fcp_1.MTRSTATUS = 'I'
            and rownum < 2
)