select replace(rtrim(xmlagg(xmlelement(NMI, ':'||SERVICEPOINTID||':,')).extract('//text()'),','),':',chr(39)) as NMI
from (
    select fcp_1.SERVICEPOINTID
    from (
        select distinct fcp.SERVICEPOINTID
             , count(distinct met.METERID) as METER_COUNT
             , count(distinct fcp.CHANNELNUMBER) as CHANNEL_COUNT
        from FLATCONFIGPHYSICAL fcp
           , METER met
           , ENTITYSTATUS es
           , VALIDATIONSET vs
        where fcp.MTRSTATUS = 'A'       
          and fcp.MTRCHNLTYPE = 'I'     
          and fcp.EFFECTIVEENDDATE > sysdate
          and fcp.METERID = met.METERID
          and met.EFFECTIVEENDDATE > sysdate
          and met.ENTITYSTATUSKEY = es.ENTITYSTATUSKEY
          and es.STATUSID = 'Active'
          and fcp.validationsetkey = vs.validationsetkey
          and vs.validationsetid in ('Type5ValidationSet')    
	  and fcp.SERVICEPOINTID NOT IN (Select NMI from TEST_DATA_KEY_REGISTER)                          
        group by fcp.SERVICEPOINTID
    ) nmi_det
    , FLATCONFIGPHYSICAL fcp_1    
    , SERVICEPOINTCHANNEL spc
    , NODEATTRIBUTEVALUE ndav
    where nmi_det.SERVICEPOINTID = fcp_1.SERVICEPOINTID
      and nmi_det.METER_COUNT = 1
      and nmi_det.CHANNEL_COUNT = 1
      and fcp_1.MTRSTATUS = 'A'
      and fcp_1.MTRCHNLTYPE = 'I'
      and fcp_1.EFFECTIVEENDDATE > sysdate
      and fcp_1.CHANNELNUMBER = '1101'
      and fcp_1.spcnodeid = spc.spcnodeid
      and spc.nodekey = ndav.nodekey
      and ndav.datavalue = '<tariff_type>'
      and rownum < 101
)

