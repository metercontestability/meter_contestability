select SERVICEPOINT 
from (
    select ss.SERVICEPOINT 
    from SERVICEPOINT_STATUS ss
    join STATUS s on ss.STATUS_ID = s.ID
    join SERVICEPOINT_REF_ATTRIBUTE sra on sra.SERVICEPOINT = ss.SERVICEPOINT
    where sra.ATTRIBUTE_TYPE_ID = 1     
      and sra.VALUE = 'COMMS4'
      and sra.END_DT > sysdate
      and s.NAME = 'A'     
      and ss.END_DT > sysdate
      and sra.SERVICEPOINT in (select distinct frmp.SERVICEPOINT
                               from SERVICEPOINT_MP_ROLE frmp, 
                                    SERVICEPOINT_MP_ROLE lr
                               where frmp.SERVICEPOINT = lr.SERVICEPOINT
                                 and frmp.ROLE_ID = 6
                                 and lr.ROLE_ID = 8
                                 and frmp.END_DT > sysdate
                                 and lr.END_DT > sysdate                                 
                                 and frmp.MARKET_PARTICIPANT_ID = lr.MARKET_PARTICIPANT_ID)
     and ss.SERVICEPOINT not in
           (select NMI FROM TEST_DATA_KEY_REGISTER 
             WHERE IS_RETIRED_YN = 'N'
               AND IS_IN_USE_YN  = 'Y') /* the sub-query returns a row if the NMI is in use */
      and rownum <=  <rownum>
)