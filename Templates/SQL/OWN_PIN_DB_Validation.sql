Select BPD.TRANSACTION_DETAIL_ID as PIN_DB_Transaction_Detail_ID,
BPD.NMI as PIN_DB_NMI, 
BPD.NMI_CHECKSUM as PIN_DB_NMICHECKSUM,
BPD.START_DATE as PIN_DB_StartDate, /* Check against XML */
BPD.START_TIME as PIN_DB_StartTime, /* Check against XML */
BPD.END_DATE as PIN_DB_EndDate, /* Check against XML */
BPD.DURATION as PIN_DB_Duration, /* Check against WS */
BRI.NAME as PIN_DB_ReasonForInter, /* ReasonForInter from WS */
BPD.NOTES as PIN_DB_Notes, /* Notes from WS */
BPD.UPDATED_USER,
BPD.UPDATED_DT         
from TRANSACTION_DETAIL TD, B2B_PIN_DATA BPD, B2B_REASON_FOR_INTERRUPTIONS BRI
where TD.id = BPD.TRANSACTION_DETAIL_ID
and BRI.id = BPD.REASON_FOR_INTER_ID
And BPD.NMI = '<NMI>'
and BPD.TRANSACTION_DETAIL_ID in (select id from transaction_detail
where transactionid = '<INIT_TRANSACTION_ID>')


