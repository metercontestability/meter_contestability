SELECT   MTD.TRANSACTIONID
       , gf.FILE_TIME_STAMP 
       , nmid.NMI || nmid.CHECKSUM as NMI 
       , gm.TRANSACTION_GRP
       , AQ.ID AS AQ_ID 
       , AQ.CREATED_DT 
       , AQRC.NAME AS AQ_NAME
        , mdic.investigation_cd
    FROM gw_message gm
       , gw_document gd
       , gw_file gf
       , gw_transaction gt
       , transaction_type tt
       , b2b_md_transaction_detail mtd2
       , b2b_md_investigation_code mdic
       , transaction_detail mtd
       , nmi_standing_data nmid
       , ACTIVITY_QUEUE       AQ
       , ACTIVITY_QUEUE_REASON_CODE AQRC  
   WHERE gf.document_id = gd.ID
     AND gt.message_id = gm.ID
     AND tt.ID = gt.TRANSACTION_TYPE_ID
     AND gd.ID = gm.document_id
     AND mtd.GW_TRANSACTION_ID = gt.ID
     AND mtd.ID = mtd2.TRANSACTION_DETAIL_ID
     AND nmid.ID = mtd2.NMI_STANDING_DATA_ID
     AND MTD2.INVESTIGATION_ID = mdic.id
     AND AQ.REASON_CODE_ID = AQRC.ID
     AND mtd.ID = AQ.TRANSACTION_DETAIL_ID
     AND gd.direction = '<document_Direction>'
     AND gm.TRANSACTION_GRP = '<TRANSACTION_GRP>'
     AND tt.id = 75
     AND investigation_cd in ('<Investigation_Code>') 
     and rownum < 20