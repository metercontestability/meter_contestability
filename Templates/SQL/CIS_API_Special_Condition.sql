declare
  queue_options      DBMS_AQ.ENQUEUE_OPTIONS_T;
  message_properties DBMS_AQ.MESSAGE_PROPERTIES_T;
  message_id         RAW(16);
  my_message         SYS.AQ$_JMS_TEXT_MESSAGE;
begin
  my_message := SYS.AQ$_JMS_TEXT_MESSAGE.construct;
  my_message.header.clear_properties();
  my_message.header.set_string_property('CISOVEnableDiagnostics', 'TRUE');
  message_properties.correlation := 'PRE_'||to_char(systimestamp,'dd_mm_yyyy_hh24_mi_ss');
  my_message.set_text('
<CreateSpecialConditionsRequest xmlns="http://www.logica.com/CISOV/API">
      <RequestHeader>
        <Version>0</Version>
        <CompanyCode>^param_company_code_egCITI^</CompanyCode>
                                <UserName>^param_user_name_eg_RECONECT^</UserName>
    </RequestHeader>
    <RequestBody>
        <Site>
            <PropertyNo><param_PropertyNum></PropertyNo>
            <ServiceProvisionNo><param_SPNum></ServiceProvisionNo>
        </Site>
        <SpecialCondition>
            <SpecialConditionType><param_Special_Condition></SpecialConditionType>        
            <EffectiveDate><param_SplCond_EffectiveDate></EffectiveDate>
        </SpecialCondition>
    </RequestBody>
</CreateSpecialConditionsRequest> 
');
  DBMS_AQ.ENQUEUE(queue_name         => 'WLAQ.CIS_IN_Q',
                  enqueue_options    => queue_options,
                  message_properties => message_properties,
                  payload            => my_message,
                  msgid              => message_id);
  commit;
end;