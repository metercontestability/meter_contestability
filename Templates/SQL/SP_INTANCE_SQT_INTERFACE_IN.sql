SELECT
EXTRACTVALUE(T.USER_DATA.PAYLOAD_DATA, '//iee:ConfigurationUpdate/iee:ServicePointID', 'xmlns:iee="http://www.itron.com/mdm/configuration/2008/04"') AS NMI,
EXTRACTVALUE(T.USER_DATA.PAYLOAD_DATA, '//mts:State/mts:StartDate', 'xmlns:mts="http://www.itron.com/mts/2008/08"') AS NMISTATEDATE,
EXTRACTVALUE(T.USER_DATA.PAYLOAD_DATA, '//mts:State/mts:StatusCode', 'xmlns:mts="http://www.itron.com/mts/2008/08"') AS NMISTATUSCODE,
EXTRACTVALUE(T.USER_DATA.PAYLOAD_DATA, '//mts:State/mts:DeenergisationMethod', 'xmlns:mts=""http://www.itron.com/mts/2008/08""') AS DEENERGISATIONMETHOD
FROM MTS.SQT_INTERFACE_IN T
WHERE DBMS_LOB.INSTR(T.USER_DATA.PAYLOAD_DATA.GETCLOBVAL(), '<SP_INSTANCE_CORRID>') > 0