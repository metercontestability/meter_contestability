  select  sum(R.DATAVALUE)
    from servicepoint sp 
    join nodelink nl on nl.leftnodekey = sp.nodekey
    join servicepointchannel spc on spc.nodekey = nl.rightnodekey
    join reading r on r.nodeid = spc.spcnodeid
    join readinggroup rg on rg.readinggroupalternatekey = r.readinggroupalternatekey
    join ched_user.reading_src_map rm on rm.readingsourceid = rg.readingsourceid
    join readingreadingstatus rrs on rrs.statusid = r.statusid
    where upper (sp.servicepointid) = upper('<NMI>')  -- NMI with checksum
      and r.endtime > to_date('<Aggs_start_Date>','dd/mm/yyyy')+1 - (10/24)   -- agg request start date
      and r.endtime <= to_date('<Aggs_End_Date>','dd/mm/yyyy') +1 - (10/24)    -- agg request end date
      and spc.channelnumber = '1101'
     and pkg_ched_utils.  fn_decode_reading_status(  rrs.  status,  rrs.  statusext1)  like  '%VEESET1PV%'