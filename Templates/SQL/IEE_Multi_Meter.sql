SELECT DISTINCT s.servicepointid AS NMI,
                         UPPER (M.METERNUMBER ) AS MeterNumber,
                         MSS.SUFFIX AS RegisterId,
                         MSS.SUFFIX AS Suffix,
                         spc.spcnodeid,
                         U.UNITID AS UnitOfMeasure,
                         UOM.UNITOFMEASUREID,
                         nl2.effectivestartdate + 10 / 24 AS nodelink_start,
                         nl2.effectiveenddate + 10 / 24 AS nodelink_end,
                         es.statusid,
                         m.effectivestartdate + 10 / 24 AS meter_start,
                         m.effectiveenddate + 10 / 24 AS meter_end,
                         M.MULTIPLIER AS MULTIPLIER,
                         'C' AS STATUS,
                         SUBSTR (M.MANUFACTURER, 1, 15) AS MANUFACTURER,
                         M.MODEL AS MODEL,
                         NVL(IUAD.DATAVALUE,' ') AS ControlledLoad,
                         NVL(IUAD2.DATAVALUE, ' ') AS NetworkTariffCode,
                         'INTERVAL' AS TimeOfDay /*HARDCODED AS THESE ARE INTERVAL METERS*/,
                         IC.PULSEMISCMULTIPLIER AS ConstantCAT
           FROM servicepoint s
                JOIN nodelink nl ON nl.leftnodekey = s.nodekey
                JOIN servicepointchannel spc ON spc.nodekey = nl.rightnodekey
                JOIN nodelink nl2 ON nl2.leftnodekey = spc.nodekey
                JOIN intervalchannel ic ON ic.nodekey = nl2.rightnodekey
                JOIN nodelink nl3 ON nl3.rightnodekey = ic.nodekey
                JOIN meter m ON m.nodekey = nl3.leftnodekey
                INNER JOIN MTS_SPC_SUFFIX_MAP MSS
                   ON MSS.SPC_ID = spc.channelnumber
                JOIN spcvalidationset svs
                   ON svs.servicepointchannelkey = spc.servicepointchannelkey
                JOIN validationset vs
                   ON vs.validationsetkey = svs.validationsetkey
                JOIN entitystatus es
                   ON es.entitystatuskey = m.entitystatuskey
                 JOIN IEE_SERVICEPOINTCHANNELUDAS iuad
                   ON IUAD.SERVICEPOINT = S.SERVICEPOINTID
                 JOIN IEE_SERVICEPOINTCHANNELUDAS iuad2
                   ON IUAD2.SERVICEPOINT = S.SERVICEPOINTID
                JOIN UNITOFMEASURE uom
                   ON UOM.UNITOFMEASUREKEY = SPC.UNITOFMEASUREKEY
                JOIN UNIT u ON UOM.UNITKEY = U.UNITKEY
          WHERE     1 = 1
                AND (    svs.effectivestartdate < nl2.effectiveenddate
                     AND svs.effectiveenddate > nl2.effectivestartdate)
                AND (    m.effectivestartdate < nl2.effectiveenddate
                     AND m.effectiveenddate > nl2.effectivestartdate)
                AND S.SERVICEPOINTID = '<NMI>' /*AND S.SERVICEPOINTID = 'VAAA0021074'*/
                AND M.EFFECTIVEENDDATE > SYSDATE
                AND UPPER (IUAD.UDANAME) = 'CONTROLLED_LOAD'
                AND UPPER (IUAD2.UDANAME) = 'NTC'
                AND IUAD.SERVICEPOINTCHANNELNUMBER = spc.channelnumber
                AND IUAD2.SERVICEPOINTCHANNELNUMBER = SPC.CHANNELNUMBER
                AND IUAD.EFFECTIVEENDLINKDATE > SYSDATE
                AND IUAD2.EFFECTIVEENDLINKDATE > SYSDATE
                AND 1 = 1
            order by MeterNumber, RegisterId