select SERVICEPOINT 
from (
select ss.SERVICEPOINT 
    from SERVICEPOINT_STATUS ss
    join STATUS s on ss.STATUS_ID = s.ID
    join SERVICEPOINT_REF_ATTRIBUTE sra on sra.SERVICEPOINT = ss.SERVICEPOINT
    join SERVICEPOINT_REF_ATTRIBUTE sra1 on sra1.SERVICEPOINT = ss.SERVICEPOINT
    join SERVICEPOINT_MP_ROLE smr on smr.SERVICEPOINT = ss.SERVICEPOINT 
    join SERVICEPOINT_MP_ROLE smr2 on smr2.SERVICEPOINT = ss.SERVICEPOINT
    where s.NAME = 'A'  
      and ss.END_DT > sysdate
      and sra.ATTRIBUTE_TYPE_ID = 1 
      and sra.VALUE = 'COMMS4'
      and sra.END_DT > sysdate
      and sra1.ATTRIBUTE_TYPE_ID = 5 
      and sra1.VALUE = 'LARGE'  /*  <<<===== */
      and sra1.END_DT > sysdate      
      and smr.ROLE_ID = 1       
      and smr.END_DT > sysdate
      and smr2.ROLE_ID = 13     
      and smr2.END_DT > sysdate    
      and 0 = (select count(1)
                    from cat_change_request
                    where 1=1
                    and UPDATED_DT > trunc(sysdate-10)
                    and nmid = substr(ss.SERVICEPOINT,1,10))
   and ss.SERVICEPOINT not in
           (select NMI FROM TEST_DATA_KEY_REGISTER 
             WHERE IS_RETIRED_YN = 'N'
               AND IS_IN_USE_YN  = 'Y') /* the sub-query returns a row if the NMI is in use */
and SS.SERVICEPOINT NOT IN (SELECT DISTINCT (CCD.NMID) FROM CAT_CR_DATA CCD WHERE CCD.UPDATED_DT >= SYSDATE - 10)
      and rownum <= <rownum>)