'select CCR.TRANSACTION_DETAIL_ID       
from cat_change_request     ccr
     , transaction_status     ts
     , status                 s
     , cat_change_reason_code crc
     , transaction_detail td
where ccr.change_reason_code_id = crc.id
  and ts.transaction_detail_id  = ccr.transaction_detail_id
  and TS.TRANSACTION_DETAIL_ID = TD.ID
  and ts.status_id = s.id
  and ccr.nmid = '<NMI>'
  and ccr.UPDATED_DT > trunc(sysdate)