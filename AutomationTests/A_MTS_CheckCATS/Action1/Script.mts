﻿
Window("MTS_Window").WinMenu("Menu").Select "Transactions;CATS;Search CATS Notifications Received"
 
strDate = Right("0" & DatePart("d",Date), 2) &"/"& Right("0" & DatePart("m",Date), 2)&"/"&DatePart("yyyy",Date)


PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_search").SetCellData "#1","created_dt_from",strDate @@ hightlight id_;_460280_;_script infofile_;_ZIP::ssf1.xml_;_
PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_search").SetCellData "#1","created_dt_to",strDate @@ hightlight id_;_460280_;_script infofile_;_ZIP::ssf1.xml_;_
PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_search").SetCellData "#1","NMI",Parameter.Item("pNMI") @@ hightlight id_;_460280_;_script infofile_;_ZIP::ssf2.xml_;_
PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_search").SetCellData "#1","change_requestid",Parameter.Item("pRequestId") @@ hightlight id_;_198510_;_script infofile_;_ZIP::ssf12.xml_;_

PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbButton("Display").Click @@ hightlight id_;_329572_;_script infofile_;_ZIP::ssf22.xml_;_

strChangeStatusCode = PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_slct").GetCellData ("#1","change_request_status") @@ hightlight id_;_264144_;_script infofile_;_ZIP::ssf15.xml_;_

strProsRetailer = PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_slct").GetCellData ("#1","raised_by") @@ hightlight id_;_264144_;_script infofile_;_ZIP::ssf17.xml_;_

strCATSStatus = PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_slct").GetCellData ("#1","status") @@ hightlight id_;_264144_;_script infofile_;_ZIP::ssf19.xml_;_
strObjectionFlag = PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_slct").GetCellData ("#1","objected") @@ hightlight id_;_264144_;_script infofile_;_ZIP::ssf20.xml_;_

PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbButton("Details").Click @@ hightlight id_;_329572_;_script infofile_;_ZIP::ssf22.xml_;_

strObjectionCode = PbWindow("MTS").PbWindow("w_cats_notifi_recevd_details").PbDataWindow("dw_bottom").GetCellData ("#1","objection_reason_code") @@ hightlight id_;_198398_;_script infofile_;_ZIP::ssf6.xml_;_

strExplanation = PbWindow("MTS").PbWindow("w_cats_notifi_recevd_details").PbDataWindow("dw_bottom").GetCellData ("#1","explanation") @@ hightlight id_;_198398_;_script infofile_;_ZIP::ssf8.xml_;_

PbWindow("MTS").PbWindow("w_cats_notifi_recevd_details").PbButton("Close").Click @@ hightlight id_;_262208_;_script infofile_;_ZIP::ssf10.xml_;_
PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbButton("Close").Click @@ hightlight id_;_329572_;_script infofile_;_ZIP::ssf22.xml_;_

If strChangeStatusCode = Parameter.Item("pChangeStatusCode") and strProsRetailer = Parameter.Item("pProsRetailer") and  strCATSStatus = Parameter.Item("pCATSStatus")  and strObjectionFlag = "ON" Then
	Reporter.ReportEvent micPass,"PASS","CATS successfully Objected"
Else
	Reporter.ReportEvent micFail,"FAIL","CATS objected failed"
	If strChangeStatusCode <> pChangeStatusCode Then
		Reporter.ReportEvent micFail,"FAIL",strChangeStatusCode&" <> "&Parameter.Item("pChangeStatusCode")
	ElseIf strProsRetailer <> pProsRetailer Then
		Reporter.ReportEvent micFail,"FAIL",strProsRetailer&" <> "&Parameter.Item("pProsRetailer")
	ElseIf strCATSStatus <> pCATSStatus Then
		Reporter.ReportEvent micFail,"FAIL",strCATSStatus&" <> "&Parameter.Item("pCATSStatus") 
	ElseIf strObjectionFlag <> "ON" Then
		Reporter.ReportEvent micFail,"FAIL",strObjectionFlag&" <> ON" 

	End If
End If

If strObjectionCode = Parameter.Item("pObjectionCode") and strExplanation = Parameter.Item("pObjectionExplanation") Then
	Reporter.ReportEvent micPass,"PASS","CATS Objection codes are as expected"
Else 
	Reporter.ReportEvent micFail,"FAIL","CATS Objection codes are NOT as expected"
	If strObjectionCode <> Parameter.Item("pObjectionCode") Then
		Reporter.ReportEvent micFail,"FAIL",strObjectionCode&" <> "&Parameter.Item("pObjectionCode")
	ElseIf strExplanation <> Parameter.Item("pObjectionExplanation") Then
		Reporter.ReportEvent micFail,"FAIL",strExplanation&" <> "&Parameter.Item("pObjectionExplanation")
	End If
End If