﻿'Get the input parameters
'Company: CITI, PCOR, SAPN
'ENV: DEV, SYSTEST, UAT
'RunCount: Number of iterations
Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")
Environment.Value("RUNCOUNT")=Parameter.Item("RunCount")

Set uftapp = CreateObject("QuickTest.Application") 
strTest = uftapp.Test.Name

Environment.Value("TESTFOLDER") = "C:\_data\CATS\"

strCATSStatus = "OBJECTED"
strObjectionCode = "BADMETER"
strObjectionExplanation = "Invalid combination of crcode, meter type and meter read type code"

'Set the folder for results
'Environment.Value("TEST_RUN_SOURCE_FOLDER")="C:\_data\CATS\"

Environment.Value("DbQuery_Scenario") = "Find_RRIM"
'Load the environment details for database access and get the database queries
Loaddata
'Save Datamine query in a variable to use it for all iterations
Environment.Value("FindNMI") = Environment.Value("DBQuery")

'Open MTS
RunAction "A_MTS_OpenMTS [A_MTS_OpenMTS]", oneIteration

For i = 1 To Environment.Value("RUNCOUNT")
Environment.Value("DBQuery") = Environment.Value("FindNMI")
'Get the RRIM Meter
strNMI = ExecuteDatabaseQuery

'Get the query for fetching LNSP
GetRole strNMI,"FRMP"
strFRMP = ExecuteDatabaseQuery

'Get the query for fetching LNSP
GetRole strNMI,"LNSP"
strLNSP = ExecuteDatabaseQuery

'Get the query for fetching MDP
GetRole strNMI,"MDP"
strMDP = ExecuteDatabaseQuery

'Get the query for fetching RP
GetRole strNMI,"RP"
strRP = ExecuteDatabaseQuery'

'CATS CR Code
strCRcode = "1040"


'Get the prospective retailer that is different to current FRMP
GetProspectiveRetailer strFRMP
strProsRetailer = ExecuteDatabaseQuery

strProposedDate = date
strReadTypeCode= "PR"
strInitTxnID = strProsRetailer&"-"&strNMI&"-"&datepart("d",date)&hour(time)&minute(time)&second(time)

strRequestId = datepart("y",date)&Right("0" & DatePart("m",Date), 2)&Right("0" & DatePart("d",Date), 2)&hour(time)&minute(time)&second(time)

'Create xml REQ MDP
'strRole = "MDP"
'strChangeStatusCode = "REQ"
'strFileName = "CATS_"&strNMI&"_"&strRole&"_"&strCRcode&"_"&strReadTypeCode&"_"&strChangeStatusCode&"_"&datepart("d",date)&hour(time)&minute(time)&second(time)
'CreateCATSXML strNMI,strLNSP,strMDP,strRP,strCRcode,strProsRetailer,strChangeStatusCode,strProposedDate,strReadTypeCode,strRole,strRequestId,strInitTxnID,strFileName
''msgbox strFileName
'ZipFile strFileName&".xml",Environment.Value("TESTFOLDER")

'Create xml REQ LNSP
strRole = "LNSP"
strChangeStatusCode = "REQ"
strFileName = "CATS_"&strNMI&"_"&strRole&"_"&strCRcode&"_"&strReadTypeCode&"_"&strChangeStatusCode&"_"&datepart("d",date)&hour(time)&minute(time)&second(time)
CreateCATSXML strNMI,strLNSP,strMDP,strRP,strCRcode,strProsRetailer,strChangeStatusCode,strProposedDate,strReadTypeCode,strRole,strRequestId,strInitTxnID,strFileName
ZipFile strFileName&".xml",Environment.Value("TESTFOLDER")

'strCATSDestinationFolder = 
GetCATSDestinationFolder Environment.Value("COMPANY"),Environment.Value("ENV"),strRole
'msgbox Environment.Value("CATSDestinationFolder")
copyfile strFileName&".zip",Environment.Value("TESTFOLDER"),Environment.Value("CATSDestinationFolder")

wait 25


RunAction "A_MTS_CheckCATS [A_MTS_CheckCATS]", oneIteration,strRequestId,strNMI,strChangeStatusCode,strProsRetailer,strCATSStatus,strObjectionCode,strObjectionExplanation

Next


RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration