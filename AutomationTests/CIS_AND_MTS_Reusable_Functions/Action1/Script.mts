﻿' 6102500638 - AMI Meter NMI - <> E
' 61023730086 - = E

int_mini_wait_time = 500
gFWbln_ExitIteration = "N"

' Unit_Test_Create_Service_Order "e1"

' Unit_Test_Cancel_Service_Order


' fn_MTS_Search_Site_Access_Request_Received "123456", "02/02/2010","04/04/2016","","","","","Y","",""

fn_MTS_Search_Site_Access_Request_Received_SelectandVerify_Record_Results "","TxnID_20170406145401","","61023331121","","","","","","","","",""
gFWbln_ExitIteration = "n"
fn_MTS_Search_Site_Access_Request_Received_SelectandVerify_Record_Results "","TxnID_201704061452401","","61023331121","","","","","","","","",""
gFWbln_ExitIteration = "n"
fn_MTS_Search_Site_Access_Request_Received_SelectandVerify_Record_Results "first","","","","","","","","","","","",""
gFWbln_ExitIteration = "n"
fn_MTS_Search_Site_Access_Request_Received_SelectandVerify_Record_Results "last","","","","","","","","","","","",""
gFWbln_ExitIteration = "n"
fn_MTS_Search_Site_Access_Request_Received_SelectandVerify_Record_Results "","TxnID_20170406142002","","6102333111","","","","","","","","",""
gFWbln_ExitIteration = "n"
fn_MTS_Search_Site_Access_Request_Received_SelectandVerify_Record_Results "","TxnID_20170406144601","","6102333112","","","","","","","","",""
gFWbln_ExitIteration = "n"
fn_MTS_Search_Site_Access_Request_Received_SelectandVerify_Record_Results "","TxnID_20170406145401","","6102333112","","","","","","","","",""
gFWbln_ExitIteration = "n"








''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' UNIT TEST SCENARIOS - START ''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub Unit_Test_Cancel_Service_Order()

' Flow to check service order in CIS  - START 
    ' Login to application
    fnCIS_Login "y", "UANAND", "qwerty123", "CCISMD"
    ' Click on Menu File -> Open Explorer
    fn_CIS_MenuNavigation "File;Open Explorer    Ctrl+O"
    
    ' Search for a record
    fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria "61020000040", "NMI Number.", "All Roles","y","n",""
    
    ' Select the first open from displayed record
    fnCIS_Select_Record_Search_Results_Post_Legal_Entry_Property_Search "", "first", "", "n",""
    
    ' Click on Menu Property -> Service Order
    fn_CIS_MenuNavigation "Property;Service Orders"

    fnCIS_Service_Order_Selection "DN","","", "14/03/2017", "","","", "", "Working","","","y","n","",""
    
    ' cancel all service orders
    fn_CIS_Customer_Service_Order_Cancel_Service_Order "y", "CitiPower WG", "Electricity", "Unable to Access", "all",  "n",  "n"
    
    fn_CIS_Close_Window "Customer_Service_Order_Screen"
    fn_CIS_Close_Window "search_results"

' Flow to check service order in CIS  - END 


End Sub


Sub Unit_Test_Check_Service_Order()

' Flow to check service order in CIS  - START 
    ' Login to application
    fnCIS_Login "y", "UANAND", "qwerty123", "CCISMD"
    ' Click on Menu File -> Open Explorer
    fn_CIS_MenuNavigation "File;Open Explorer    Ctrl+O"
    
    ' Search for a record
    fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria "61020000040", "NMI Number.", "All Roles","y","n",""
    
    ' Select the first open from displayed record
    fnCIS_Select_Record_Search_Results_Post_Legal_Entry_Property_Search "", "first", "", "n",""
    
    ' Click on Menu Property -> Service Order
    fn_CIS_MenuNavigation "Property;Service Orders"

    fnCIS_Service_Order_Selection "OF","","","13/03/2017","","","","","","","","y","n","",""
    
    ' Click on Order Tab
    fn_CIS_Customer_Service_Order_Select_Record_Order_Tab "y", "n", "","y", ""
    
    ' Click on Task Tab"
    fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "y", "first", "n", "n", "y",""

    fn_CIS_Close_Window "Customer_Service_Order_Screen"
    fn_CIS_Close_Window "search_results"

' Flow to check service order in CIS  - END 


End Sub

Sub Unit_Test_Create_Service_Order(strscenario)

    Select Case strscenario
            
        Case "e"
        
            ' Flow to create a Service Order - Start
            ' Login to application
            fnCIS_Login  "y", "UANAND", "qwerty123", "CCISMD"
            ' fnCIS_Login  "y", "UANAND", "qwerty123", "CCISMT"
            
            ' Click on Menu File -> Open Explorer
            fn_CIS_MenuNavigation "File;Open Explorer    Ctrl+O"
            
            ' Search for a record
            fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria "6102500638", "NMI Number.", "All Roles","y","n",""
            ' 6102500638-7
            
            ' Select the first open from displayed record
            fnCIS_Select_Record_Search_Results_Post_Legal_Entry_Property_Search "", "first", "", "n",""
            
            ' Click on Menu Property -> Service Order
            fn_CIS_MenuNavigation "Property;Service Orders"
            
            ' Click on New button
            fn_CIS_Click_button_any_screen "Service_Order_Selection", "btn_New"
            
            ' Fill Service Order Type selection details 
            fnCIS_Service_Order_Type_Selection "Disconnect", "Retailer Req -Remote", "Retailer", "03/03/2017", "", "", "ON",  "y", "n", ""
            
            ' Click on task tab
            fn_CIS_Click_Tab_any_screen "customer_service_order_Screen", "tasks"

            ' Select the first record (SP level)
            fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "n", "first", "","", "y",""

            ' Edit -> Modify - pass the task from sheet
            fn_CIS_MenuNavigation "Edit;Modify"
            
            ' Enter details on Update_Service_Provision_Task screen
            fn_CIS_Update_Service_Provision_Task "", "03/03/2017", "", "03/03/2017", "", "", "", "", "y", "n", ""

            
            ' Select the meter level record - reword
            fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "n", "first", "","", "y",""
            
            ' click approve (either button or from Menu Edit -> Approve) - - reword
            fn_CIS_MenuNavigation "Edit;Approve"


            ' First complete the meter level record 
                
                ' Click on somewhere on the screen to enable the menu Edit -> Complete
                fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "n", "last", "","", "n",""
                fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "n", "first", "","", "n",""
                fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "n", "last", "","", "n",""
                
                ' click approve (either button or from Menu Edit -> complete)
                fn_CIS_MenuNavigation "Edit;Complete"
                
                ' Click OK on task completion
                fn_CIS_Task_Completion "Reading Taken", "y", "n", ""
            
                ' Write meter reading
                fn_CIS_Register_Read_Entry_Update "",""

            ' Second - complete the SP level record 
            
                ' Click on somewhere on the screen to enable the menu Edit -> Complete
                fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "n", "last", "","", "y",""
                fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "n", "first", "","", "y",""
                
                ' click approve (either button or from Menu Edit -> complete)
                fn_CIS_MenuNavigation "Edit;Complete"
                
                ' Click OK on task completion
                fn_CIS_Task_Completion "", "y", "n", ""
            
            fn_CIS_Close_Window "Customer_Service_Order_Screen"
            fn_CIS_Close_Window "search_results"
        
            ' Flow to create a Service Order - End            

        Case else ' mainly <> e
        
            ' Flow to create a Service Order - Start
            ' Login to application
            fnCIS_Login  "y", "UANAND", "qwerty123", "CCISMD"
            
            ' Click on Menu File -> Open Explorer
            fn_CIS_MenuNavigation "File;Open Explorer    Ctrl+O"
            
            ' Search for a record
            fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria "61030408858", "NMI Number.", "All Roles","y","n",""
            
            ' Select the first open from displayed record
            fnCIS_Select_Record_Search_Results_Post_Legal_Entry_Property_Search "", "first", "", "n",""
            
            ' Click on Menu Property -> Service Order
            fn_CIS_MenuNavigation "Property;Service Orders"
            
            ' Click on New button
            fn_CIS_Click_button_any_screen "Service_Order_Selection", "btn_New"
            
            ' Fill Service Order Type selection details 
            ' fnCIS_Service_Order_Type_Selection "Turn-Off", "Ret Request - BH", "Retailer", "03/03/2017", "", "", "ON",  "y", "n", ""
            ' fnCIS_Service_Order_Type_Selection "General Servicing", "Business Request", "Business", "11/03/2017", "", "", "ON",   "y", "n", ""
            fnCIS_Service_Order_Type_Selection "Disconnect", "", "", "11/03/2017", "", "", "ON",   "y", "n", ""
            
            ' Click on task tab
            fn_CIS_Click_Tab_any_screen "customer_service_order_Screen", "tasks"
            
            ' Select the first record (SP level)
            fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "n", "first", "","", "y","" ' ' NOT WORKING AS EXPECTED
            
            ' Edit -> Modify - pass the task from sheet
            fn_CIS_MenuNavigation "Edit;Modify"
            
            ' Enter details on Update_Service_Provision_Task screen
            fn_CIS_Update_Service_Provision_Task "", "10/03/2017", "", "10/03/2017", "", "", "", "", "y", "n", ""
            
            ' Select the first record (SP level)
            fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "n", "first", "","", "y",""
            
            ' click approve (either button or from Menu Edit -> Approve)
            fn_CIS_MenuNavigation "Edit;Approve"
            
            ' check for Probe and complete that particular row
            fn_CIS_Customer_Service_Order_Complete_Service_Order  "y", "all", "n" ' NOT TESTED
            
            fn_CIS_Close_Window "Customer_Service_Order_Screen"
            fn_CIS_Close_Window "search_results"
        
            ' Flow to create a Service Order - End


    End Select



End Sub 
    











'
'
'Function fn_CIS_Customer_Service_Order_Complete_Service_Order (WhetherClickTaskTab_YN, str_RecordNo_tocomplete, WhetherCaptureScreenshot_YN)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_Customer_Service_Order_Complete_Service_Order
'    '  Description:-  Function to capture value from Order Tab on Customer Service Order screen
'    '  Parameters:-    1)
'    '  Return Value:-
'    '  Creator:- Umesh Anand
'    '################################################################################
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_Customer_Service_Order_Complete_Service_Order function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
'
' ' On error resume next 
'    wait 0,int_mini_wait_time
'    Dim temp_task_Status, temp_task, temp_schedule_Date, temp_effective_date
'    Dim int_ctr, int_last_rec, temp_task_name
'
'    If trim(lcase(WhetherClickTaskTab_YN)) = "y" Then
'        fn_CIS_Click_Tab_any_screen "customer_service_order_Screen", "tasks"
'    End If
'
'
'
'    If trim(str_RecordNo_tocomplete) <> "" Then
'        Select Case lcase(str_RecordNo_tocomplete)
'            Case "first"
'                temp_var_record_nr = 1
'            Case "last"
'                temp_var_record_nr = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").RowCount
'            Case Else
'                temp_var_record_nr = str_RecordNo_tocomplete
'        End Select
'
'        If lcase(temp_var_record_nr) = "all" Then
'        
'            ' We need to loop through all the rows and cancel one by one
'            int_last_rec = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").RowCount
'            
'            For int_ctr = int_last_rec To 1 step -1 ' Select the record
'                If int_ctr = 1 Then
'                    temp_task_name = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").GetCellData("#1","cd_work_ord_task")
'                    If trim(temp_task_name) <> "" Then
'                    PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").SelectCell "#" & int_ctr,"cd_work_ord_task"
'                    temp_task_name = "SPLEVEL"
'                    End If 
'                Else
'                    ' Capture the task name
'                    temp_task_name = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").GetCellData("#2","cd_work_ord_task")
'                    PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").SelectCell "#" & int_ctr,"ds_serv_prov"
'                End If
'                
'                If trim(temp_task_name) <> "" Then
'                    If lcase(trim(temp_task_name)) = "probe" or int_ctr = 1 Then
'                        wait (1)
'                        ' edit -> complete
'                        fn_CIS_MenuNavigation "Edit;Complete"
'                        ' Click OK on task completion
'                        fn_CIS_Task_Completion "", "y", "n", ""
'                        
'                        ' Click on somewhere on the screen to enable the menu Edit -> Complete
'                        fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "n", "last", "","", "y",""
'                        fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "n", "first", "","", "y",""
'                    End If
'                End If
'            Next
'            
'        End If' end of If temp_var_record_nr = "all" Then
'        
'    End If' end of     If trim(str_RecordNo) <> "" Then
'    
'    
'    ' Take screenshot
'    if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'        ' First add timestamp to filename
'        Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'        ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'        ' On error resume next 
'          call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'        ' On error goto 0
'    End If
'
'
'    ' At the end of the function, check if there was any error during any operation and if error needs to be generated
'
'
'    ' Write information to Run Log (database)
'
'
' ' On error goto 0
'
'End Function ' end of Function fn_CIS_Customer_Service_Order_Complete_Service_Order
'
'

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' UNIT TEST SCENARIOS - ENDS ''''''''''''''''''''''''''''''''''''''''''''''''''''







' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
'                                                                     AUT FUNCTIONS MOVED TO LIBRARY
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
'
'
'
'Function fnCIS_Service_Order_Selection (str_Type, int_Number, str_Reason, var_Recd_Date, var_Recd_Time, str_Recd_By, var_Wanted_Date, var_Wanted_Time, str_Status, var_Status_Date, var_Status_Time, _
'                                        whetherClick_OK_YN, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation, dict_datacontext)
'
'    '################################################################################
'    '
'    '  Function Name:- fnCIS_Service_Order_Selection
'    '  Description:-  Function to select a particular service order on Service Order Selection screen
'    '   Parameters:-    1) 
'    ' Return Value:-  None
'    ' Creator:- Umesh Anand
'    '
'    '################################################################################
'    
'    Dim temp_error_message, temp_var_record_nr
'    Dim temp_str_Type, temp_int_Number, temp_str_Reason, temp_var_Recd_Date, temp_var_Recd_Time, temp_str_Recd_By, temp_var_Wanted_Date, temp_var_Wanted_Time, temp_str_Status, temp_var_Status_Date, temp_var_Status_Time
'    Dim int_ctr, int_row_cnt
'    ' On error resume next 
'    
'        If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'            Reporter.ReportEvent micDone, "fnCIS_Service_Order_Selection function", "Exiting function as there was some error in previous step"
'            Exit function
'        End If
'    
'        ' If there is more than 1 row in the screen, traverse through each and find the appropriate record
'        
'        
'        int_row_cnt = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").RowCount
'        If int_row_cnt > 0  Then
'        
'            For int_ctr = 1 To int_row_cnt
'                
'                ' Type
'                temp_str_Type = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").GetCellData("#" & int_ctr,"tp_work_order")
'                
'                ' Number - we don't need at this time
'                ' temp_int_Number = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").GetCellData "#" & int_ctr,"cd_work_order" 
'                
'                ' Reason - we don't need at this time
'                ' temp_str_Reason = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").GetCellData "#" & int_ctr,"ds_wo_reason" 
'                
'                ' Date Received
'                temp_var_Recd_Date = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").GetCellData("#" & int_ctr,"ts_created")
'                
'                ' Received Time, we don't need
'                ' temp_var_Recd_Time = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").GetCellData "#" & int_ctr,"ts_created"
'                
'                ' Received by - we don't need at this time
'                ' temp_str_Recd_By = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").GetCellData "#" & int_ctr,"nm_preferred"
'                
'                ' Wanted date - we don't need 
'                'temp_var_Wanted_Date = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").GetCellData "#" & int_ctr,"dt_scheduled"
'                ' Wanted time - we don't need
'                'temp_var_Wanted_Time = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").GetCellData "#" & int_ctr,"ds_tm_call"
'                
'                ' status - we don't need
'                'temp_str_Status = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").GetCellData "#" & int_ctr,"ds_work_order"
'                
'                ' status date - we don't need
'                'temp_var_Status_Date = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").GetCellData "#" & int_ctr,"ts_status_changed"
'                
'                ' status time - we don't need
'                'temp_var_Status_Time = PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").GetCellData "#" & int_ctr,"ts_status_changed"
'                
'            
'                If str_Type <> "" and var_Recd_Date <> "" Then
'                    If trim(lcase(str_Type)) = trim(lcase(temp_str_Type))  and  trim(lcase(var_Recd_Date)) = trim(lcase(temp_var_Recd_Date))   Then
'                        PbWindow("CIS").PbWindow("Service_Order_Selection").PbDataWindow("result_Service_Order_Selection").SelectCell "#" & int_ctr,"ds_wo_reason"
'                        int_ctr = int_row_cnt + 1
'                    End If
'                End If
'
'            Next
'            
'            If lcase(whetherClick_OK_YN) = "y" Then
'                fn_CIS_Click_button_any_screen "Service_Order_Selection", "btn_OK"
'            End If
'        
'        Else' Write an error
'            temp_error_message =  "No record on Service Order Selection Screen"
'            gFWbln_ExitIteration = "y"
'            gFWstr_ExitIteration_Error_Reason = "fnCIS_Service_Order_Selection function - Error (" & temp_error_message & ") while selecting the appropriate service order"
'            Reporter.ReportEvent micFail, "fnCIS_Service_Order_Selection function", gFWstr_ExitIteration_Error_Reason
'            fnCIS_Service_Order_Selection = "FAIL"
'        End If    
'    
'    
'        ' Take screenshot
'        if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'            ' First add timestamp to filename
'            Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'            ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'            ' On error resume next 
'              call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'            ' On error goto 0
'        End If
'        
'        ' Save the capture data to dictionary - IMPLEMENTATION PENDING
'         
'    ' On error goto 0
'
'    
'End Function ' end of Function fnCIS_Service_Order_Selection
'
'
'
'Function fn_CIS_Task_Completion (str_Action, WhetherClickOK_YN, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_Task_Completion
'    '  Description:-  Function to select action on the Task Completion screen
'    '  Parameters:-    1)
'    '  Return Value:-
'    '  Creator:- Umesh Anand
'    '################################################################################
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_Task_Completion function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
' ' On error resume next 
'    wait 0,int_mini_wait_time
'    Dim temp_error_message
'    
'    If str_Action <> "" Then
'        PbWindow("CIS").PbWindow("Task_Completion").PbDataWindow("dropdn_action").SelectCell "#1","#9"
'        PbWindow("CIS").PbWindow("Task_Completion").PbDataWindow("dropdn_action").SetCellData "#1","#9",str_Action ' "DE-EN Fuse In"
'    End If
'
'    If trim(lcase(WhetherClickOK_YN)) = "y" Then
'        fn_CIS_Click_button_any_screen "Task_Completion", "btn_OK"
'    End If
'
'    ' Check if there is any error message
'    If PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Message").exist(1) Then
'        temp_error_message =  PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Message").PbDataWindow("text_message").GetCellData("#1","message")
'        PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Message").PbButton("btn_OK").Click
'        gFWbln_ExitIteration = "y"
'        gFWstr_ExitIteration_Error_Reason = "fn_CIS_Task_Completion function - Error (" & temp_error_message & ") while performing task completion"
'        Reporter.ReportEvent micFail, "fn_CIS_Task_Completion function", gFWstr_ExitIteration_Error_Reason
'        fn_CIS_Task_Completion = "FAIL"
'    End If
'
'    If PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Task_Completion").Exist(1) Then
'        temp_error_message =  PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Task_Completion").PbDataWindow("text_message").GetCellData("#1","message")
'        PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Task_Completion").PbButton("btn-OK").Click
'        gFWbln_ExitIteration = "y"
'        gFWstr_ExitIteration_Error_Reason = "fn_CIS_Task_Completion function - Error (" & temp_error_message & ") while performing task completion"
'        Reporter.ReportEvent micFail, "fn_CIS_Task_Completion function", gFWstr_ExitIteration_Error_Reason
'        fn_CIS_Task_Completion = "FAIL"
'    End If
'
'    
'    ' Take screenshot
'    if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'        ' First add timestamp to filename
'        Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'        ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'        ' On error resume next 
'          call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'        ' On error goto 0
'    End If
'
'
'    ' At the end of the function, check if there was any error during any operation and if error needs to be generated
'    If gFWbln_ExitIteration = "y" Then ' Case when an error might have occured, close all windows and leave the application on base state
'    
'    End If
'
'    ' Write information to Run Log (database)
'
'
' ' On error goto 0
'
'End Function ' end of Function fn_CIS_Task_Completion
'
'Function fn_CIS_Customer_Service_Order_Select_Record_Order_Tab (WhetherClickOrderTab_YN, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation, whetherCapture_Runtime_Actuals, dict_datacontext)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_Customer_Service_Order_Select_Record_Order_Tab
'    '  Description:-  Function to capture value from Order Tab on Customer Service Order screen
'    '  Parameters:-    1)
'    '  Return Value:-
'    '  Creator:- Umesh Anand
'    '################################################################################
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_Customer_Service_Order_Select_Record_Order_Tab function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
' ' On error resume next 
'    wait 0,int_mini_wait_time
'    Dim temp_Account_Requested_By
'
'    If trim(lcase(WhetherClickOrderTab_YN)) = "y" Then
'        fn_CIS_Click_Tab_any_screen "customer_service_order_Screen", "order"
'    End If
'
'    If lcase(trim(whetherCapture_Runtime_Actuals)) = "y" Then
'        ' capture Account -> Requested By
'        temp_Account_Requested_By = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_Order_top_panel").GetCellData("#1","nm_legal_entity")
'        
'        ' Write in dict_datacontext
'        
'    End If
'    
'
'    ' Take screenshot
'    if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'        ' First add timestamp to filename
'        Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'        ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'        ' On error resume next 
'          call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'        ' On error goto 0
'    End If
'
'
'    ' At the end of the function, check if there was any error during any operation and if error needs to be generated
'
'
'    ' Write information to Run Log (database)
'
'
' ' On error goto 0
'
'End Function ' end of Function fn_CIS_Customer_Service_Order_Select_Record_Order_Tab
'
'Function fn_CIS_Customer_Service_Order_Select_Record_Task_Tab (WhetherClickTaskTab_YN, str_RecordNo, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation, whetherCapture_Runtime_Actuals, dict_datacontext)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_Customer_Service_Order_Select_Record_Task_Tab
'    '  Description:-  Function to capture value from Order Tab on Customer Service Order screen
'    '  Parameters:-    1)
'    '  Return Value:-
'    '  Creator:- Umesh Anand
'    '################################################################################
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_Customer_Service_Order_Select_Record_Task_Tab function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
' ' On error resume next 
'    wait 0,int_mini_wait_time
'    Dim temp_task_Status, temp_task, temp_schedule_Date, temp_effective_date
'
'    If trim(lcase(WhetherClickTaskTab_YN)) = "y" Then
'        fn_CIS_Click_Tab_any_screen "customer_service_order_Screen", "tasks"
'    End If
'
'    If trim(str_RecordNo) <> "" Then
'        Select Case lcase(str_RecordNo)
'            Case "first"
'                temp_var_record_nr = 1
'            Case "last"
'                temp_var_record_nr = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").RowCount
'            Case Else
'                temp_var_record_nr = str_RecordNo
'        End Select
'
'        If temp_var_record_nr = 1 Then
'            PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").SelectCell "#" & temp_var_record_nr,"cd_work_ord_task"
'        Else
'            PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").SelectCell "#" & temp_var_record_nr,"ds_serv_prov"
'        End If
'            
'        If lcase(trim(whetherCapture_Runtime_Actuals)) = "y" Then
'            ' Once the tab is clicked, capture data 
'            
'            ' capture task status (first row)
'            temp_task_Status = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").GetCellData("#1","ds_wrk_ord_det_112")
'            ' capture task (first row)
'            temp_task = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").GetCellData("#1","cd_work_ord_task")
'            ' capture schedule date
'            temp_schedule_Date = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").GetCellData("#1","dt_scheduled")
'            ' capture effective date
'            temp_effective_date = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").GetCellData("#1","dt_ord_effective")
'            
'            ' Write in dict_datacontext
'            
'        End If
'    End If
'    
'    
'    ' Take screenshot
'    if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'        ' First add timestamp to filename
'        Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'        ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'        ' On error resume next 
'          call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'        ' On error goto 0
'    End If
'
'
'    ' At the end of the function, check if there was any error during any operation and if error needs to be generated
'
'
'    ' Write information to Run Log (database)
'
'
' ' On error goto 0
'
'End Function ' end of Function fn_CIS_Customer_Service_Order_Select_Record_Task_Tab
'
'
'Function fnCIS_Service_Order_Type_Selection (str_SO_Type, str_SO_Reason, str_Requestor, var_Date_Wanted, var_Time_Wanted, str_Primary_Service_Order, CB_Work_Completed, whether_Click_OK_YN, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation)
'
'    '################################################################################
'    '
'    '  Function Name:- fnCIS_Service_Order_Type_Selection
'    '  Description:-  Function to fill Service Order Type selection details 
'    '   Parameters:-    1) str_SO_Type
'    ' Return Value:-  None
'    ' Creator:- Umesh Anand
'    '
'    '################################################################################
'    
'    Dim temp_error_message, temp_var_record_nr
'    ' On error resume next 
'    
'        If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'            Reporter.ReportEvent micDone, "fnCIS_Select_Record_Search_Results_Post_Legal_Entry_Property_Search function", "Exiting function as there was some error in previous step"
'            Exit function
'        End If
'    
'        If str_SO_Type <> "" Then
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbDataWindow("panel_Details").SelectCell "#1","#1"
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbDataWindow("panel_Details").SetCellData "#1","#1", str_SO_Type
'        End If
'        
'        If str_SO_Reason <> ""  Then
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbDataWindow("panel_Details").SelectCell "#1","cd_wo_reason"
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbDataWindow("panel_Details").SetCellData "#1","#7",str_SO_Reason
'        End If
'    
'        If str_Requestor <> ""  Then
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbDataWindow("panel_Details").SelectCell "#1","tp_requestor"
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbDataWindow("panel_Details").SetCellData "#1","#14",str_Requestor 
'        End If
'        
'        
'        If var_Date_Wanted <> ""  Then
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbDataWindow("panel_Details").SelectCell "#1","dt_scheduled"
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbDataWindow("panel_Details").SetCellData "#1","dt_scheduled",var_Date_Wanted ' "15/03/2017"
'        End If
'        
'        If var_Time_Wanted <> ""  Then
'        End If
'        
'        If str_Primary_Service_Order <> ""  Then
'        End If
'        
'        If lcase(CB_Work_Completed) = "on"  Then
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbDataWindow("panel_Details").SelectCell "#1","fg_work_completed"
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbDataWindow("panel_Details").SetCellData "#1","fg_work_completed","ON"
'            ' a dialog would appear where we need to click yes
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbWindow("Work_Completed").PbButton("btn_Yes").Click
'        ElseIf lcase(CB_Work_Completed) = "off"  Then
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbDataWindow("panel_Details").SetCellData "#1","fg_work_completed","OFF"
'        End If
'        
'        If lcase(whether_Click_OK_YN) = "y" Then
'            fn_CIS_Click_button_any_screen "Service_Order_Type_Selection", "btn_OK"
'        End If
'    
'        ' Take screenshot
'        if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'            ' First add timestamp to filename
'            Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'            ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'            ' On error resume next 
'              call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'            ' On error goto 0
'        End If
'         
'    ' On error goto 0
'
'    
'End Function ' end of Function fnCIS_Service_Order_Type_Selection
'    
'
'Function fnCIS_Select_Record_Search_Results_Post_Legal_Entry_Property_Search (strTabname, str_RecordNo, str_Address, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation)
'
'    '################################################################################
'    '
'    '  Function Name:- fnCIS_Select_Record_Search_Results_Post_Legal_Entry_Property_Search
'    '  Description:-  Function to select the record post entering other search criteria on the legal_entity_property_search screen
'    '   Parameters:-    1) strTabname - Name of tab to click
'    '   Parameters:-    2) str_RecordNo - can be first, last or a specific record number 
'    '   Parameters:-    3) str_Address - address to select
'    '   Parameters:-    4) WhetherCaptureScreenshot_YN - Whether we need to take a screenshot (Y or N)
'    '   Parameters:-    5) str_screenshot_uniqueFileNamewithFolderLocation - Location and filename of where the screenshot has to be placed
'    ' Return Value:-  None
'    ' Creator:- Umesh Anand
'    '
'    '################################################################################
'    
'    Dim temp_error_message, temp_var_record_nr
'    ' On error resume next 
'    
'        If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'            Reporter.ReportEvent micDone, "fnCIS_Select_Record_Search_Results_Post_Legal_Entry_Property_Search function", "Exiting function as there was some error in previous step"
'            Exit function
'        End If
'    
'        If trim(strTabname) <> "" Then
'            ' code to be written when we use this
'        End If
'
'        If trim(str_RecordNo) <> "" Then
'            Select Case lcase(str_RecordNo)
'                Case "first"
'                    temp_var_record_nr = 1
'                Case "last"
'                    temp_var_record_nr = PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").RowCount
'                Case Else
'                    temp_var_record_nr = str_RecordNo
'            End Select
'            
'            If temp_var_record_nr = 1 Then
'                PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#"& temp_var_record_nr ,"c0_col1"
'            Else
'                PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#"& temp_var_record_nr ,"c1_col1"
'            End If
'                ' 
'        End If
'
'        If trim(str_Address) <> "" Then
'            ' code to be written when we use this
'        End If
'
'
'        ' Take screenshot
'        if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'            ' First add timestamp to filename
'            Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'            ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'            ' On error resume next 
'              call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'            ' On error goto 0
'        End If
'         
'    ' On error goto 0
'    
'    ' Doco below 
'        ' 
'        '' Row 1
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#1","c0_col1"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#1","c0_col1"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#1","c0_col1"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#1","c0_col1"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#1","c0_col2"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#1","c0_col2"
'        '' Row 2
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#2","c1_col1"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#2","c1_col2"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#2","c1_col2"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#2","c1_col3"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#2","c1_col4"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#2","c1_col6"
'        '' Row 3
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#3","c1_col1"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#3","c1_col2"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#3","c1_col3"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#3","c1_col4"
'        'PbWindow("CIS").PbWindow("search_results").PbDataWindow("Search_Result_Tree").SelectCell "#3","c1_col6"
'    
'End Function ' end of Function fnCIS_Select_Record_Search_Results_Post_Legal_Entry_Property_Search
'    
'
'Function fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria (strValue, strType, str_Account_Role, WhetherClickSearch_YN, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation)
'
'    '################################################################################
'    '
'    '  Function Name:- fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria
'    '  Description:-  Function to perform search using other search criteria on "Legal Entry Property Search" screen
'    '   Parameters:-    1) strValue - a valid string of values
'    '   Parameters:-    2) strType - a value that is present in drop down
'    '   Parameters:-    3) Account_Role - a value that is present in drop down
'    ' Return Value:-  None
'    ' Creator:- Umesh Anand
'    '
'    '################################################################################
'    
'    Dim temp_error_message
'    ' On error resume next 
'    
'        If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'            Reporter.ReportEvent micDone, "fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria function", "Exiting function as there was some error in previous step"
'            Exit function
'        End If
'    
'        If trim(strType) <> "" Then
'            PbWindow("CIS").PbWindow("legal_entity_property_search").PbDataWindow("Search_Criteria").SelectCell "#1","other_search_type"
'            PbWindow("CIS").PbWindow("legal_entity_property_search").PbDataWindow("Search_Criteria").SetCellData "#1","#16",strType ' "NMI Number."
'        End If
'
'        If trim(strValue) <> "" Then
'            PbWindow("CIS").PbWindow("legal_entity_property_search").PbDataWindow("Search_Criteria").SelectCell "#1","other_search_value"
'            PbWindow("CIS").PbWindow("legal_entity_property_search").PbDataWindow("Search_Criteria").SetCellData "#1","other_search_value", strValue ' "NMI"
'        End If
'
'        If trim(str_Account_Role) <> "" Then
'            PbWindow("CIS").PbWindow("legal_entity_property_search").PbDataWindow("Search_Criteria").SelectCell "#1","other_search_account_role"
'            ' PbWindow("CIS").PbWindow("legal_entity_property_search").PbDataWindow("Search_Criteria").SetCellData "#1","#75","All Roles"
'            PbWindow("CIS").PbWindow("legal_entity_property_search").PbDataWindow("Search_Criteria").SetCellData "#1","other_search_account_role", str_Account_Role ' "All Roles"
'        End If
'
'        If lcase(WhetherClickSearch_YN) = "y" Then
'            PbWindow("CIS").PbWindow("legal_entity_property_search").PbButton("btn_Search").Click
'        End If
'
'        ' Check if there is any error related to insufficient information entered
'        
'        If PbWindow("CIS").PbWindow("legal_entity_property_search").PbWindow("Search_Result_Error").exist(1) Then
'            temp_error_message =  PbWindow("CIS").PbWindow("legal_entity_property_search").PbWindow("Search_Result_Error").PbDataWindow("panel_message").GetCellData("#1","message")
'            PbWindow("CIS").PbWindow("legal_entity_property_search").PbWindow("Search_Result_Error").PbButton("btn_OK").Click
'            gFWbln_ExitIteration = "y"
'            gFWstr_ExitIteration_Error_Reason = "fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria function - Error (" & temp_error_message & ") while performing search"
'            Reporter.ReportEvent micFail, "fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria function", gFWstr_ExitIteration_Error_Reason
'            fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria = "FAIL"
'        End If
'
'
'        ' Check if there is any error screen
'        If PbWindow("CIS").PbWindow("Error_Message").Exist(1) Then
'            temp_error_message =  PbWindow("CIS").PbWindow("Error_Message").PbDataWindow("panel_message").GetCellData("#1","message")
'            PbWindow("CIS").PbWindow("Error_Message").PbButton("btn_OK").Click
'            gFWbln_ExitIteration = "y"
'            gFWstr_ExitIteration_Error_Reason = "fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria function - Error (" & temp_error_message & ") while performing search"
'            Reporter.ReportEvent micFail, "fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria function", gFWstr_ExitIteration_Error_Reason
'            fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria = "FAIL"
'        End If
'
'        If PbWindow("CIS").PbWindow("Search_Result_Error").Exist(1) Then
'            temp_error_message =  PbWindow("CIS").PbWindow("Search_Result_Error").PbDataWindow("panel_message").GetCellData("#1","message")
'            PbWindow("CIS").PbWindow("Search_Result_Error").PbButton("btn_OK").Click
'            gFWbln_ExitIteration = "y"
'            gFWstr_ExitIteration_Error_Reason = "fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria function - Error (" & temp_error_message & ") while performing search"
'            Reporter.ReportEvent micFail, "fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria function", gFWstr_ExitIteration_Error_Reason
'            fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria = "FAIL"
'        End If
'            
'        ' Take screenshot
'        if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'            ' First add timestamp to filename
'            Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'            ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'            ' On error resume next 
'              call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'            ' On error goto 0
'        End If
'         
'    ' On error goto 0
'End Function ' end of Function fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria
'    
'
'Function fnCIS_WinClose
'
'    '################################################################################
'    '
'    '  Function Name:- fnCIS_WinClose
'    '  Description:-  Function to Close CIS
'    '   Parameters:-    1) str_StringFormat - a template string format. An e.g. of template is
'    '                        cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^CRCode`{4}^Phase`{5}^Role`{6}^Stage`{7}^RequestID`{8}^NMI`{9} ."
'    '                        The template should have arguments within {} and the numbering should start from 0
'    '                    2) str_ArrayReplacementValues - array containing data which would be used for string replacement
'    ' Return Value:-  None
'    ' Creator:- Umesh Anand
'    '
'    '################################################################################
'    
'    ' On error resume next 
'    
'    ' Incase the login screen is opened, press cancel button
'    If PbWindow("CIS").PbWindow("Login").Exist(0) Then
'        fn_CIS_Click_button_any_screen  "Login", "btn_Cancel"
'    Else
'        ' Incase the login screen is not there, 
'        fn_CIS_MenuNavigation "File;Exit    Alt+F4"
'    End If
'        
'    '    in case the GUI-commands above don't succeed in closing the app cleanly, 
'    '        now Kill the application process if it still persists
'         KillProcesses "cisov.exe"
'         
'    ' On error goto 0
'End Function ' end of Function fnCIS_WinClose
'    
'
'
'Function fnCIS_OpenApplication()
'    '################################################################################
'    '
'    '  Function Name:- fnCIS_OpenApplication
'    '  Description:-  Function to launch CIS application
'    ' Return Value:-  None
'    ' Creator:- Umesh Anand
'    '
'    '################################################################################
'    
'
'    ' Close any open process
'    KillProcesses "cisov.exe"
'    wait 0,500
'    
'    SystemUtil.Run "C:\Program Files (x86)\STS\CISOV\cisov.exe"
'
'End Function ' end of Function fnCIS_OpenApplication()
'    
'
'
'
'Function fnCIS_Login(strUserName, strPassword, strCompany)
'    '################################################################################
'    '
'    '  Function Name:- fnCIS_OpenApplication
'    '  Description:-  Function to login to  CIS application
'    '   Parameters:-    1) strUserName - user id
'    '                   2) strPassword - password
'    '                   2) strCompany - company
'    ' Return Value:-  None
'    ' Creator:- Umesh Anand
'    '
'    '################################################################################
'    Dim temp_error_Message 
'    ' On error resume next  
'        fnCIS_OpenApplication
'        
'        PbWindow("CIS").RefreshObject
'        PbWindow("CIS").PbWindow("Login").RefreshObject
'        PbWindow("CIS").PbWindow("Login").PbDataWindow("panel_Login_Details").RefreshObject
'        
'        If trim(strUserName) <> "" Then
'            PbWindow("CIS").PbWindow("Login").PbDataWindow("panel_Login_Details").SelectCell "#1","user_id"    
'            PbWindow("CIS").PbWindow("Login").PbDataWindow("panel_Login_Details").SetCellData "#1","user_id",strUserName
'        End If
'    
'        If trim(strPassword) <> "" Then
'            PbWindow("CIS").PbWindow("Login").PbDataWindow("panel_Login_Details").SelectCell "#1","password"    
'            PbWindow("CIS").PbWindow("Login").PbDataWindow("panel_Login_Details").SetCellData "#1","password",strPassword
'            ' PbWindow("CIS").PbWindow("Login").WinEdit("Edit").SetSecure "58c1cd462ae4c5e753715b7b78d1"
'        End If
'    
'        If trim(strCompany) <> "" Then
'            PbWindow("CIS").PbWindow("Login").PbDataWindow("panel_Login_Details").SelectCell "#1","Tuxedo"    
'            PbWindow("CIS").PbWindow("Login").PbDataWindow("panel_Login_Details").SetCellData "#1","Tuxedo",strCompany
'        End If
'        
'        ' click on Ok button
'        PbWindow("CIS").PbWindow("Login").PbButton("btn_OK").Click
'        
'        ' Check if invalid user details window is displayed
'        
'        If PbWindow("CIS").PbWindow("Login").PbWindow("Invalid_User_ID").Exist(0) Then
'            ' capture error message
'            temp_error_Message = PbWindow("CIS").PbWindow("Login").PbWindow("Invalid_User_ID").PbButton("txt_message").GetVisibleText
'            Reporter.ReportEvent micFail, "fnCIS_Login function", "Unable to login because of error (" & temp_error_Message & ")"
'            gFWbln_ExitIteration = "Y"
'        End If 
'    
'    ' On error goto 0
'
'End Function ' end of Function fnCIS_Login(strUserName, strPassword, strCompany)
'
'
'Function fn_CIS_MenuNavigation (strMenu_Navigation_Path)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_MenuNavigation
'    '  Description:-  This function would be open a particular menu in CIS application
'    '  Parameters:-    1)strMenu_Navigation_Path - Semi colon seperated menu navigation path E.g. "File;Open Explorer    Ctrl+O"
'    '  Return Value:-  
'    '  Creator:- Umesh Anand
'    '################################################################################
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_MenuNavigation function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
'
'    wait 0,int_mini_wait_time
'    ' On error resume next 
'    Window("CIS_Window").Activate
'    ' On error goto 0
'
'    eval( "Window(""CIS_Window"").WinMenu(""Menu"").Select(""" & strMenu_Navigation_Path & """)" )
'    
'
'End Function ' end of Function fn_CIS_MenuNavigation (strMenu_Navigation_Path) 
'    
'    
'Function fn_CIS_Click_button_any_screen (str_screen_Name, str_button_text)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_Click_button_any_screen
'    '  Description:-  Function used to click any button on any screen. The screen name should be same as in Object Repository
'    '  Parameters:-    1)
'    '  Return Value:-
'    '  Creator:- Umesh Anand
'    '################################################################################
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_Click_button_any_screen function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
' ' On error resume next 
'    wait 0,int_mini_wait_time
'    ' PbWindow("CIS").Activate
'    PbWindow("CIS").RefreshObject
'
'    Select Case trim(str_screen_Name & "-" & str_button_text)
'        Case "Login-btn_Cancel"
'            PbWindow("CIS").PbWindow("Login").PbButton("btn_Cancel").Click
'        Case "Service_Order_Selection-btn_New"
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbButton("btn_New").Click
'        Case "Service_Order_Type_Selection-btn_OK"
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbWindow("Service_Order_Type_Selection").PbButton("btn_OK").Click
'        Case "Service_Order_Selection-btn_OK"
'            PbWindow("CIS").PbWindow("Service_Order_Selection").PbButton("btn_OK").Click
'        Case  "Task_Completion-btn_OK"
'            PbWindow("CIS").PbWindow("Task_Completion").PbButton("btn_OK").Click
'    End Select
'
'    ' At the end of the function, check if there was any error during any operation and if error needs to be generated
'
'
'    ' Write information to Run Log (database)
'
'
' ' On error goto 0
'
'End Function ' end of Function fn_CIS_Click_button_any_screen (str_screen_Name, str_button_text)
'
'
'Function fn_CIS_Close_Window (str_screen_Name)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_Close_Window
'    '  Description:-  Function to close a particular screen
'    '  Parameters:-    1)
'    '  Return Value:-
'    '  Creator:- Umesh Anand
'    '################################################################################
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_Close_Window function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
' ' On error resume next 
'    wait 0,int_mini_wait_time
'    ' PbWindow("CIS").Activate
'    PbWindow("CIS").RefreshObject
'
'    Select Case trim(str_screen_Name)
'        Case "Customer_Service_Order_Screen"
'            PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").close
'        Case "search_results"
'            PbWindow("CIS").PbWindow("search_results").Close
'
'    End Select
'
'    ' At the end of the function, check if there was any error during any operation and if error needs to be generated
'
'
'    ' Write information to Run Log (database)
'
'
' ' On error goto 0
'
'End Function ' end of Function fn_CIS_Close_Window
'
' Function fn_CIS_Click_Tab_any_screen (str_screen_Name, str_Tab_Name)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_Click_Tab_any_screen
'    '  Description:-  Function to click a particular tab on a particular screen
'    '  Parameters:-    1)
'    '  Return Value:-
'    '  Creator:- Umesh Anand
'    '################################################################################
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_Click_Tab_any_screen function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
' ' On error resume next 
'    wait 0,int_mini_wait_time
'    Dim temp_tab_text
'
'    ' Incase the below Select Case create a problem, start clicking tabs and find out the right tab
'    ' Documentation
'    ' Tab 1 - Click 105,30 and Click 100,34
'    ' Tab 2 - Click 167,32 and Click 169,32
'    ' Tab 3 - Click 375,31 and Click 297,29
'
'    Select Case trim(lcase(str_screen_Name & "-" & str_Tab_Name))
'        Case "customer_service_order_screen-tasks" ' not an easy one. qq - need to make this more robust
'            ' coordinates with address tab 
'            PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("tb_tabs").Click 375,31
'            PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("tb_tabs").Click 297,29
'            temp_tab_text = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbButton("btn_Order").GetROProperty("text")
'            
'            If instr(lcase(temp_tab_text), lcase(str_Tab_Name)) <=0 Then
'                ' check for order button text else click on below (without address)
'                PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("tb_tabs").Click 167,32
'                PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("tb_tabs").Click 169,32
'                temp_tab_text = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbButton("btn_Order").GetROProperty("text")
'            End If
'            Reporter.ReportEvent micDone, "fn_CIS_Click_Tab_any_screen function", "Clicked on tab(" & temp_tab_text & ") on screen(" & str_screen_Name & ")"
'            
'        Case "customer_service_order_screen-order" ' not an easy one. qq - need to make this more robust
'            
'            PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("tb_tabs").Click 105,30
'            PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("tb_tabs").Click 100,34
'            temp_tab_text = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbButton("btn_Order").GetROProperty("text")
'            Reporter.ReportEvent micDone, "fn_CIS_Click_Tab_any_screen function", "Clicked on tab(" & temp_tab_text & ") on screen(" & str_screen_Name & ")"
'            
'    End Select
'
'     ' At the end of the function, check if there was any error during any operation and if error needs to be generated
'
'
'    ' Write information to Run Log (database)
'
'
' ' On error goto 0
'
'End Function ' end of Function fn_CIS_Click_Tab_any_screen

'Function fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab (WhetherClickOrderTab_YN, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation, whetherCapture_Runtime_Actuals, dict_datacontext, str_Account_Requested_By)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab
'    '  Description:-  Function to capture value from Order Tab on Customer Service Order screen
'    '  Parameters:-    1)
'    '  Return Value:-
'    '  Creator:- Umesh Anand
'    '################################################################################
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
' ' On error resume next 
'    wait 0,int_mini_wait_time
'    Dim temp_Account_Requested_By
'
'    If trim(lcase(WhetherClickOrderTab_YN)) = "y" Then
'        fn_CIS_Click_Tab_any_screen "customer_service_order_Screen", "order"
'    End If
'
'    If lcase(trim(whetherCapture_Runtime_Actuals)) = "y" Then
'        ' capture Account -> Requested By
'        temp_Account_Requested_By = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_Order_top_panel").GetCellData("#1","nm_legal_entity")
'        
'        ' Write in dict_datacontext
'        
'        If trim(str_Account_Requested_By) = trim(temp_Account_Requested_By)  Then
'            Reporter.ReportEvent micPass, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab function", "Account requested by matched. Expected value (" & str_Account_Requested_By & ") is same as actual value (" & temp_Account_Requested_By & ") on Customer Service Order screen"
'        Else
'            temp_error_message =  "Account requested does not match. Expected value (" & str_Account_Requested_By & ") is different from actual value (" & temp_Account_Requested_By & ") on Customer Service Order screen"
'            gFWbln_ExitIteration = "y"
'            gFWstr_ExitIteration_Error_Reason = "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab function - " & temp_error_message
'            Reporter.ReportEvent micFail, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab function", temp_error_message
'            fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab = "FAIL"
'        End If
'        
'    End If
'    
'
'    ' Take screenshot
'    if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'        ' First add timestamp to filename
'        Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'        ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'        ' On error resume next 
'          call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'        ' On error goto 0
'    End If
'
'
'    ' At the end of the function, check if there was any error during any operation and if error needs to be generated
'
'
'    ' Write information to Run Log (database)
'
'
' ' On error goto 0
'
'End Function ' end of Function fn_CIS_Customer_Service_Order_Select_Record_Order_Tab
'
'Function fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab (WhetherClickTaskTab_YN, str_RecordNo, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation, whetherCapture_Runtime_Actuals, dict_datacontext, task_Status, task, schedule_Date, effective_date)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab
'    '  Description:-  Function to capture value from Order Tab on Customer Service Order screen
'    '  Parameters:-    1)
'    '  Return Value:-
'    '  Creator:- Umesh Anand
'    '################################################################################
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
' ' On error resume next 
'    wait 0,int_mini_wait_time
'    Dim temp_task_Status, temp_task, temp_schedule_Date, temp_effective_date
'
'    If trim(lcase(WhetherClickTaskTab_YN)) = "y" Then
'        fn_CIS_Click_Tab_any_screen "customer_service_order_Screen", "tasks"
'    End If
'
'    If trim(str_RecordNo) <> "" Then
'        Select Case lcase(str_RecordNo)
'            Case "first"
'                temp_var_record_nr = 1
'            Case "last"
'                temp_var_record_nr = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").RowCount
'            Case Else
'                temp_var_record_nr = str_RecordNo
'        End Select
'
'        If temp_var_record_nr = 1 Then
'            PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").SelectCell "#" & temp_var_record_nr,"cd_work_ord_task"
'        Else
'            PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").SelectCell "#" & temp_var_record_nr,"ds_serv_prov"
'        End If
'            
'        If lcase(trim(whetherCapture_Runtime_Actuals)) = "y" Then
'            ' Once the tab is clicked, capture data 
'            
'            ' capture task status (first row)
'            temp_task_Status = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").GetCellData("#1","ds_wrk_ord_det_112")
'            
'            If trim(task_Status) = trim(temp_task_Status)  Then
'                Reporter.ReportEvent micPass, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab function", "Task Status matched. Expected value (" & task_Status & ") is same as actual value (" & temp_task_Status & ") on Customer Service Order screen"
'            Else
'                temp_error_message =  "Task Status different. Expected value (" & task_Status & ") is different from actual value (" & temp_task_Status & ") on Customer Service Order screen"
'                gFWbln_ExitIteration = "y"
'                gFWstr_ExitIteration_Error_Reason = "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab function - " & temp_error_message
'                Reporter.ReportEvent micFail, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab function", temp_error_message
'                fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab = "FAIL"
'            End If
'            
'            ' capture task (first row)
'            temp_task = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").GetCellData("#1","cd_work_ord_task")
'            
'            If trim(task) = trim(temp_task)  Then
'                Reporter.ReportEvent micPass, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab function", "Task matched. Expected value (" & task & ") is same as actual value (" & temp_task & ") on Customer Service Order screen"
'            Else
'                temp_error_message =  "Task different. Expected value (" & task & ") is different from actual value (" & temp_task & ") on Customer Service Order screen"
'                gFWbln_ExitIteration = "y"
'                gFWstr_ExitIteration_Error_Reason = "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab function - " & temp_error_message
'                Reporter.ReportEvent micFail, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab function", temp_error_message
'                fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab = "FAIL"
'            End If
'
'            ' capture schedule date
'            temp_schedule_Date = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").GetCellData("#1","dt_scheduled")
'            
'            If trim(schedule_Date) = trim(temp_schedule_Date)  Then
'                Reporter.ReportEvent micPass, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab function", "Schedule Date matched. Expected value (" & schedule_Date & ") is same as actual value (" & temp_schedule_Date & ") on Customer Service Order screen"
'            Else
'                temp_error_message =  "Schedule Date different. Expected value (" & schedule_Date & ") is same as actual value (" & temp_schedule_Date & ") on Customer Service Order screen"
'                gFWbln_ExitIteration = "y"
'                gFWstr_ExitIteration_Error_Reason = "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab function - " & temp_error_message
'                Reporter.ReportEvent micFail, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab function", temp_error_message
'                fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab = "FAIL"
'            End If
'            
'            ' capture effective date
'            temp_effective_date = PbWindow("CIS").PbWindow("Customer_Service_Order_Screen").PbDataWindow("results_tasks").GetCellData("#1","dt_ord_effective")
'            
'            If trim(effective_date) = trim(temp_effective_date)  Then
'                Reporter.ReportEvent micPass, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab function", "Effective Date matched. Expected value (" & effective_date & ") is same as actual value (" & temp_effective_date & ") on Customer Service Order screen"
'            Else
'                temp_error_message =  "Schedule Date different. Expected value (" & effective_date & ") is same as actual value (" & temp_effective_date & ") on Customer Service Order screen"
'                gFWbln_ExitIteration = "y"
'                gFWstr_ExitIteration_Error_Reason = "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab function - " & temp_error_message
'                Reporter.ReportEvent micFail, "fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab function", temp_error_message
'                fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab = "FAIL"
'            End If
'            
'            ' Write in dict_datacontext
'            
'        End If
'    End If
'    
'    
'    ' Take screenshot
'    if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'        ' First add timestamp to filename
'        Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'        ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'        ' On error resume next 
'          call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'        ' On error goto 0
'    End If
'
'
'    ' At the end of the function, check if there was any error during any operation and if error needs to be generated
'
'
'    ' Write information to Run Log (database)
'
'
' ' On error goto 0
'
'End Function ' end of Function fn_CIS_Customer_Service_Order_Select_Record_Task_Tab

'
'Function fn_CIS_Register_Read_Entry_Update (WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_Register_Read_Entry_Update
'    '  Description:-  Function to capture the last read entry, add 100 and enter it in current reading
'    '  Parameters:-    1)
'    '  Return Value:-
'    '  Creator:- Umesh Anand
'    '################################################################################
'    
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_Register_Read_Entry_Update function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
' ' On error resume next 
'    wait 0,int_mini_wait_time
'    Dim temp_last_reading, temp_len_last_reading, temp_new_reading, temp_error_message
'
'
'
'    temp_last_reading = PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbDataWindow("panel_read_entry").GetCellData("#1","am_last_reading")
'    temp_len_last_reading = len(temp_last_reading)
'    temp_new_reading = temp_last_reading  + 100 
'    
'
'    On error resume next
'        For i  = 10 To 1 step -1
'            temp_number_value = mid(temp_new_reading,i,1) 
'            If temp_number_value > 0  Then
'                PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbDataWindow("panel_read_entry").SetCellData "#1", "col"&i, temp_number_value ' mid(temp_new_reading,i,1) ' "7"
'                err.clear
'            Else
'                PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbDataWindow("panel_read_entry").SetCellData "#1", "col"&i, "0"
'                err.clear
'            End If
'            err.clear
'        Next
'    On error goto 0
'
'    PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbButton("btn_OK").Click
'
'    if PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbWindow("Register_Read_Validation_Warning").exist(0) Then
'        ' capture error/message and write in log
'        Reporter.ReportEvent micDone, "fn_CIS_Register_Read_Entry_Update function", "Clicking on button (yes) - msgbox (Register_Read_Validation_Warning)"
'        PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbWindow("Register_Read_Validation_Warning").PbButton("btn_Yes").Click
'    End If
'
'    
'                    
'    If PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbWindow("Error_Window").exist(1) Then
'        ' capture error/message and write in log
'        Reporter.ReportEvent micDone, "fn_CIS_Register_Read_Entry_Update function", "Clicking on button (OK) - msgbox (Error_Window)"
'        PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbWindow("Error_Window").PbButton("btn_OK").Click
'    End If
'    
'    If PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbWindow("Register_Reads_Hi_Lo_Failure").Exist(1) Then
'        ' capture error/message and write in log
'        Reporter.ReportEvent micDone, "fn_CIS_Register_Read_Entry_Update function", "Clicking on button (Override) - msgbox (Register_Reads_Hi_Lo_Failure)"
'        PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbWindow("Register_Reads_Hi_Lo_Failure").PbButton("btn_Override").Click
'    End If
'    If PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbWindow("Register_Read_Entry_Database_Update_Statistics").exist(1) Then
'        ' capture error/message and write in log
'        Reporter.ReportEvent micDone, "fn_CIS_Register_Read_Entry_Update function", "Clicking on button (OK) - msgbox (Register_Read_Entry_Database_Update_Statistics)"
'        PbWindow("CIS").PbWindow("Task_Completion").PbWindow("Register_Reads_Entry").PbWindow("Register_Read_Entry_Database_Update_Statistics").PbButton("btn_OK").Click
'    End If
'                    
'
''    ' Take screenshot
''    if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
''        ' First add timestamp to filename
''        Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
''        ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
''        ' On error resume next 
''          call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
''        ' On error goto 0
''    End If
''
'
'
'    ' At the end of the function, check if there was any error during any operation and if error needs to be generated
'
'
'    ' Write information to Run Log (database)
'
'
' ' On error goto 0
'
'End Function ' end of Function fn_CIS_Register_Read_Entry_Update
'
'
'
'Function fn_CIS_Update_Service_Provision_Task (str_Priority, var_Date_Scheduled, var_Time, var_Date_Effective, str_Comments, str_Task_Type, str_Work_Group, str_SIC, WhetherClick_OK_YN, WhetherCaptureScreenshot_YN, str_screenshot_uniqueFileNamewithFolderLocation)
'
'    '################################################################################
'    '
'    '  Function Name:- fn_CIS_Update_Service_Provision_Task
'    '  Description:-  Function to enter values on the Update Service Provision Task screen
'    '  Parameters:-    1)
'    '  Return Value:-
'    '  Creator:- Umesh Anand
'    '################################################################################
'    
'    ' Check if there was any error and whether the function should continue
'    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
'        Reporter.ReportEvent micDone, "fn_CIS_Update_Service_Provision_Task function", "Exiting function as there was some error in previous step"
'        Exit function
'    End If
'
' ' On error resume next 
'    wait 0,int_mini_wait_time
'
'
'    If str_Priority <> "" Then
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SelectCell "#1","no_priority"
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SetCellData "#1", "#12",str_Priority ' "Filler - Low"
'    End If
'    
'    If var_Date_Scheduled <> "" Then
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SelectCell "#1","dt_scheduled"
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SetCellData "#1","dt_scheduled",var_Date_Scheduled ' "04/03/2017"
'    End If
'    
'    If var_Time <> "" Then
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SelectCell "#1","tm_call"
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SetCellData "#1", "#3", var_Time ' "Morning"
'    End If
'    
'    If var_Date_Effective <> "" Then
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SelectCell "#1","dt_ord_effective"
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SetCellData "#1","dt_ord_effective",var_Date_Effective ' "04/03/2017"
'    End If
'    
'    If str_Pstr_Commentsriority <> "" Then
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SelectCell "#1","cm_work_order_dtl"
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SetCellData "#1","cm_work_order_dtl",str_Pstr_Commentsriority ' "comments"
'    End If
'    
'    If str_Task_Type <> "" Then
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SelectCell "#1","cd_work_order_task"
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SetCellData "#1","#9",str_Task_Type ' "Disconnect at Pole"
'    End If
'    
'    If str_Work_Group <> "" Then
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SelectCell "#1","no_organisation"
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SetCellData "#1","#10",str_Work_Group ' "CitiPower WG"
'    End If
'    
'    If str_SIC <> "" Then
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SelectCell "#1","cd_sic"
'        PbWindow("CIS").PbWindow("Update_Service_Provision_Task").PbDataWindow("panel_controls").SetCellData "#1","#4", str_SIC ' "Vegetable Growing"
'    End If
'
'    ' Take screenshot
'    if lcase(trim(WhetherCaptureScreenshot_YN)) = "y" and str_screenshot_uniqueFileNamewithFolderLocation <> "" Then
'        ' First add timestamp to filename
'        Dim temp_str_screenshot_uniqueFileNamewithFolderLocation : temp_str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, ".png",(fnTimeStamp(NOw, "yyyy`MMMmM`ddDdD_HH`mm`ss`th`t") &".png"))
'        ' str_screenshot_uniqueFileNamewithFolderLocation =  replace(str_screenshot_uniqueFileNamewithFolderLocation, " ","_")
'        ' On error resume next 
'          call PbWindow("CIS").CaptureBitmap ( temp_str_screenshot_uniqueFileNamewithFolderLocation, True )
'        ' On error goto 0
'    End If
'
'    If trim(lcase(WhetherClick_OK_YN)) = "y" Then
'        fn_CIS_Click_button_any_screen "Update_Service_Provision_Task", "btn_OK"
'    End If
'
'
'    ' At the end of the function, check if there was any error during any operation and if error needs to be generated
'
'
'    ' Write information to Run Log (database)
'
'
' ' On error goto 0
'
'End Function ' end of Function fn_CIS_Update_Service_Provision_Task
'