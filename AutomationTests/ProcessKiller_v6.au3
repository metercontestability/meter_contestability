#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
AutoItSetOption("TrayIconHide",1)
#include <GUIConstants.au3>
#Region ### START Koda GUI section ### Form=
$ProcessKiller = GUICreate("ProcessKiller v6", 307, 160, 550, 240)
;$Process = GUICtrlCreateInput("Process", 24, 24, 257, 21)
;$KillProcess = GUICtrlCreateButton("Kill <process.exe>", 24, 72, 140, 25, 0)
$Process = GUICtrlCreateInput("<program_name e.g. abc.exe>"    , 150,  20, 150, 21)
$KillWhat = GUICtrlCreateButton("&Kill ..."    ,  24,  20, 100, 25, 0)
$KillExcel = GUICtrlCreateButton("Kill All &Excel.exe"    ,  24,  60, 100, 25, 0)
$KillIE = GUICtrlCreateButton("Kill All &IExplore.exe"    , 150,  60, 100, 25, 0)
$KillNode = GUICtrlCreateButton("Kill All &node.exe",  24,  100, 100, 25, 0)
$KillChrome    = GUICtrlCreateButton("Kill all &Chrome.exe"    , 150, 100, 100, 25, 0)
;$KillRV = GUICtrlCreateButton("Kill All RptViewer",  24,  100, 100, 25, 0)
;$krQTP    = GUICtrlCreateButton("Kill & Restart QTP"    , 150, 100, 100, 25, 0)
$CountDown_Label = GUICtrlCreateLabel("Instances Remaining : 0", 24, 140, -1, -1)
$Exit = GUICtrlCreateButton("Exit"    , 200, 180,  81, 25, 0)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###
;    http://www.autoitscript.com/forum/topic/92664-get-domain/
$Domain = RegRead("HKLM\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters", "Domain")
;    msgbox (16, $Domain, "")
;    if StringInStr($Domain, "r.nsw.gov.au") = 0 then Exit
While 1
    $nMsg = GUIGetMsg()
    Switch $nMsg
        Case $GUI_EVENT_CLOSE
            Exit
        Case $KillNode
            _fnKill( "node.exe" )
        ;Case $KillNode
        ;    _fnKill( "chrome.exe" )
        Case $KillChrome
            _fnKill( "chrome.exe" )
        Case $KillIE
            _fnKill( "IExplore.exe" )
        Case $KillWhat
            _fnKill( GUICtrlRead( $Process ) )
        ;Case $KillRV
            ;ProcessClose    ( "reportviewer.exe" )
        ;_fnKill( "QTReport.exe" )
        ;_fnKill( "reportviewer.exe" )
        ;Case $krQTP
        ;ProcessClose    ( "QTPro.exe" )
;Sleep    ( 5000 )
;;;;;;;Run    ( "J:\Auto_Test\Common\Utils\sw\AutoIt\DismissDialogs.3.exe", "", @SW_RESTORE )
;Run    ( "C:\Program Files (x86)\HP\QuickTest Professional\bin\QTPro.exe", "", @SW_RESTORE )
        Case $Exit
             _fnExit()
    EndSwitch
WEnd
Func _fnKill( $ProgramName )
$ArProc_Name_PID = ProcessList( $ProgramName )
For $p = $ArProc_Name_PID[0][0] to 1 Step -1
GUICtrlSetData ( $CountDown_Label, "Instances Remaining : " & $p )
Beep ( 2000, 50 )
Sleep ( 10 )
;    MsgBox(0, $list[$i][0], $list[$p][1])
ProcessClose( $ArProc_Name_PID[$p][1] )
Next
GUICtrlSetData ( $CountDown_Label, "Instances Remaining : 0" )
EndFunc
Func _fnExit()
    Exit
EndFunc