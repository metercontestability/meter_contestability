﻿'Get the input parameters
'Company: CITI, PCOR, SAPN
'ENV: DEV, SYSTEST, UAT
'	RunCount: Number of iterations

' =========  Stage 0   - Technical Setup           =============

Dim micFinalStatus, intStageSuccessCount, intWantedSuccessCount : intStageSuccessCount = 0
Dim ctSucc, intStageSuccessAr : intStageSuccessAr = split(",0,0,0,0", ",") : intWantedSuccessCount = uBound(intStageSuccessAr)

Dim objDBConn_TestData_A, objDBRcdSet_TestData_A 
dim r

Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")

Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")
' Environment.Value("RUNCOUNT")=Parameter.Item("RunCount") ' not required


Dim strAr_listOldNew, intSize_listOldNew, str_listOldNew
str_listOldNew = ucase(Parameter.Item("listOldNew")) ' for debugging
if left(str_listOldNew,1) <> "," then str_listOldNew = "," & str_listOldNew
Environment.Value("listOldNew") = str_listOldNew 

strAr_listOldNew = split(Environment.Value("listOldNew"), ",")  ' this list will be one of :  old  new old,new  so we split the list with a comma to find out what permutations we're going to test
intSize_listOldNew = uBound(strAr_listOldNew)

Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO" ' "MTS_FIND_MRIM_SO"

Dim objXLapp, objWB_Master, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
dim objWS_DataParameter, intLast_DataParm_Row, int_NrOf_InScope_Rows, int_NrOf_NMI_rows_required, dictWSDataParameter_KeyName_ColNr

' Load the MasterWorkBook objWB_Master
fnExcel_CreateAppInstance  objXLapp, true
LoadData_RunContext objXLapp, objWB_Master, objWS_TestRunContext, 1,  BASE_AUTOMATION_DIR & "DataSheets\DataLoader.bj.xls", dictWSTestRunContext_KeyName_ColNr
'fnWS_LoadKey_ColOffsets_toDictionary objWS_TestRunContext, 1, 1, 50, "", dictWSTestRunContext_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
Dim DB_CONNECT_STR_MTS 
DB_CONNECT_STR_MTS = "Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & Environment.Value("HOSTNAME") & ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & Environment.Value("SERVICENAME") & "))); User ID=" & Environment.Value("USERNAME") & ";Password=" & Environment.Value("PASSWORD") & ";"


' load the DataParameter worksheet objWS_DataParameter - this contains the DataScenarios to test
'
set objWS_DataParameter    = objWB_Master.worksheets("CATS_OUT_2001_Objections")
intLast_DataParm_Row       = objWS_DataParameter.range("rgWS_LastRow_PlusOne").row - 1
int_NrOf_InScope_Rows      = objWS_DataParameter.range("rgWS_NumberOf_InScope_Rows").value
int_NrOf_InScope_Rows      = cInt(int_NrOf_InScope_Rows * 1.5) + 5 ' get some extra rows in case some of those identified are reserved for other cases
int_NrOf_NMI_rows_required = int_NrOf_InScope_Rows * (intSize_listOldNew + 1)
'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, 1, 1, 50, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"


Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL")&"SORD\"

Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize 
Dim strQueryTemplate, dtD, Hyph, strNMI

Dim strList_NMIsizes, strAr_NMIsizes, nmiSizes
strList_NMIsizes = ",SMALL,LARGE"
strAr_NMIsizes = split(strList_NMIsizes, ",")

Dim oldnew, oldnewMax, n
oldnewMax = intSize_listOldNew

Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
Dim intColNR_BaseDate, intColNR_DateOffset
intTestData_RowIsInScope_ColNr 	= dictWSDataParameter_KeyName_ColNr("InScope")
intCatsCR_ColNr                	= dictWSDataParameter_KeyName_ColNr("CATS_CR")
intColNR_BaseDate				= dictWSDataParameter_KeyName_ColNr("BaseDate") 
intColNR_DateOffset				= dictWSDataParameter_KeyName_ColNr("DayOffset")

Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0

Dim intSQL_Pkg_ColNr, strSQL_RunPackage

' =========  Stage I   - Gather NMI's to test with =============


Dim intNrOfDays_E_ColNr, intNrOfDays_A_ColNr, strNrOfDays_ColName, strNrOfDays, strNrOfDays_Ignore
strNrOfDays_Ignore = "-"

For nmiSizes = 1 To 2

	intSQL_ColNr     =  dictWSTestRunContext_KeyName_ColNr.item("MTS_FIND_NMI_ACTIVE_OF_SIZE")  
	If UCase(strAr_NMIsizes(nmiSizes)) = "LARGE" Then _
		intSQL_ColNr =  dictWSTestRunContext_KeyName_ColNr.item("MTS_FIND_NMI_ACTIVE_LARGE")  
	strQueryTemplate=objWS_TestRunContext.cells(2,intSQL_ColNr).value
	
	For oldnew  = 1 To oldnewMax
	                                  '                   oLD nEW                               Large Small
		strNrOfDays_ColName = lCase(left(strAr_listOldNew(oldnew),1)) & UCase(left(strAr_NMIsizes(nmiSizes),1)) &  "_Days_E"
		intNrOfDays_E_ColNr = dictWSDataParameter_KeyName_ColNr(strNrOfDays_ColName)
		intNrOfDays_O_ColNr = intNrOfDays_E_ColNr + 1 ' the actuals column is 1-right of the expected
	
		strSQL_FindActiveNmi_ofSize = strQueryTemplate
		strSQL_FindActiveNmi_ofSize = replace ( strSQL_FindActiveNmi_ofSize , "<NMI_Size>", strAr_NMIsizes(nmiSizes), 1, -1, vbTextCompare)
		strSQL_FindActiveNmi_ofSize = replace ( strSQL_FindActiveNmi_ofSize , "<rownum>"  , int_NrOf_InScope_Rows   , 1, -1, vbTextCompare) ' qq
		fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_FindActiveNmi_ofSize, int_NrOf_InScope_Rows
		
		
		str_NMI_ColumnName = "NMI_" & strAr_listOldNew(oldnew) & "`" &strAr_NMIsizes(nmiSizes)
		intTestData_NMI_ColNr = dictWSDataParameter_KeyName_ColNr(str_NMI_ColumnName)
	
		str_dlData = "" :  str_dlSuffix = ""
		for r = 2 to intLast_DataParm_Row
			select case objWS_DataParameter.cells(r, intTestData_RowIsInScope_ColNr).value
				case "Y","y", "" :
				
					strCatsCR   = objWS_DataParameter.cells(r, intCatsCR_ColNr    ).value
					strNrOfDays = objWS_DataParameter.cells(r, intNrOfDays_E_ColNr).value
					' ignore rule-combinations that are out of scope
					If strNrOfDays <> strNrOfDays_Ignore Then  ' the hyphen indicates this combination of CATS_CR and NMI-Size does not occur, so do not test it   <<<<============

						If objDBRcdSet_TestData_A.EOF Then
							Reporter.ReportEvent micFail, "CATS_OUT_2001_Objection_a.Loc`Line127 EOF encountered, sql is : ", strSQL_FindActiveNmi_ofSize
							Exit For ' this query does small & large, one may fail and not the other, so don't do an ExitTest, do an Exit For
						End If

					'	reserve the NMI until the reservation is successful
						strNMI = objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
						intReserveDataRC = fnReserveDataForCase_NMI ( strNMI, "CATS_OUT_2001_Objection_a", "", GstrDate_Run_TS_Suffix, "", strReserveData_ResultDesc)
						While intReserveDataRC < gDataRsvnSt_Approved
							objDBRcdSet_TestData_A.MoveNext	
							strNMI = objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
							intReserveDataRC = fnReserveDataForCase_NMI ( strNMI, "CATS_OUT_2001_Objection_a", "", qqpiStr_RunNR, "", strReserveData_ResultDesc)
						Wend
						
						objWS_DataParameter.cells(r, intTestData_NMI_ColNr).value = strNMI
						
						' add the NMI to the reporting row for reporting at the end of loop
						str_dlLine = fnReplaceElements(str_NMI_ColumnName & "(,,)=,,", ",," & objWS_DataParameter.cells(r, intCatsCR_ColNr).value & ",," & strNMI)
						str_dlData = str_dlData & str_dlSuffix & str_dlLine
						str_dlSuffix = vbCrLf ' "," & vbCrLf
						objDBRcdSet_TestData_A.MoveNext
						
					End if
					
			    Case else
			End select
		next
		Reporter.ReportEvent micDone, "NMIs harvested for `" & str_NMI_ColumnName & "` are : ", str_dlData
		objDBRcdSet_TestData_A.Close
		objDBConn_TestData_A.Close
		objWB_Master.save
	next
next

' set objDBConn_TestData_A = nothing : set objDBRcdSet_TestData_A = nothing
objWB_Master.save


intStageSuccessAr(1) = 1
Reporter.ReportEvent micPass, "Stage 1 of 4 is complete/passed (Data-Reservation)", ""


' =========  Stage II  - insert the NMIS into the CAT_MANUAL_TRANSACTIONS  table =============

Dim strInsert_CMT, strDt_Effective_dd_MMM_yyyy, strDateFormat, str_NMI_withoutCheckDigit
Dim dtCatsManualTxn_BaseFromDt, intDaysOffset, dtCatsManualTxn_FromDt

strDateFormat = "yyyy-MM-dd"
dtCatsManualTxn_BaseFromDt = objWS_DataParameter.cells(2,intColNR_BaseDate	).value

init_ObjDBConnection ( objDBConn_TestData_A )
For nmiSizes = 1 To 2
	
	For oldnew  = 1 To oldnewMax 
	                                  '                  oLD nEW                                Large  Small
		strNrOfDays_ColName = lCase(left(strAr_listOldNew(oldnew),1)) & UCase(left(strAr_NMIsizes(nmiSizes),1)) &  "_Days_E"
		intNrOfDays_E_ColNr = dictWSDataParameter_KeyName_ColNr(strNrOfDays_ColName)
		intNrOfDays_O_ColNr = intNrOfDays_E_ColNr + 1 ' the actuals column is 1-right of the expected

		str_NMI_ColumnName = "NMI_" & strAr_listOldNew(oldnew) & "`" &strAr_NMIsizes(nmiSizes)
		intTestData_NMI_ColNr = dictWSDataParameter_KeyName_ColNr(str_NMI_ColumnName)

		for r = 2 to intLast_DataParm_Row
			select case objWS_DataParameter.cells(r, intTestData_RowIsInScope_ColNr).value
				case "Y","y", "" :
				
					strNrOfDays = objWS_DataParameter.cells(r, intNrOfDays_E_ColNr).value
					If strNrOfDays <> strNrOfDays_Ignore Then  ' the hyphen indicates this combination of CATS_CR and NMI-Size does not occur, so do not test it   <<<<============

						strNMI    = objWS_DataParameter.cells(r, intTestData_NMI_ColNr).value
						strCatsCR = objWS_DataParameter.cells(r, intCatsCR_ColNr      ).value
						
						If not isEmpty(strNMI)  Then ' sometimes the NMI-list runs out !
						'	INSERT INTO CAT_MANUAL_TRANSACTIONS (NMI,CRCODE,EFFECTIVE_DATE) VALUES(:NMI, :CR_CODE, :EFFECTIVE_DT)
							strInsert_CMT = GstrSQL_TestData_Create_Cats_Manual_Transactions_Row	
							str_NMI_withoutCheckDigit = left(strNMI, len(strNMI)-1) ' this assumes that all inbound NMIS have a check digit that must be removed QQ
							strInsert_CMT = replace ( strInsert_CMT , ":NMI"         , str_NMI_withoutCheckDigit       , 1, -1, vbTextCompare)
							strInsert_CMT = replace ( strInsert_CMT , ":CR_CODE"     , strCatsCR                       , 1, -1, vbTextCompare)
							
							intDaysOffset = objWS_DataParameter.cells(r, intColNR_DateOffset).value
							dtCatsManualTxn_FromDt = DateAdd("d", intDaysOffset, dtCatsManualTxn_BaseFromDt)
							dtCatsManualTxn_FromDt = fnTimeStamp(dtCatsManualTxn_FromDt, strDateFormat)
							strInsert_CMT = replace ( strInsert_CMT , ":EFFECTIVE_DT", dtCatsManualTxn_FromDt, 1, -1, vbTextCompare)
							
							fn_DbExec_v1 DB_CONNECT_STR_MTS, objDBConn_TestData_A, strInsert_CMT, true
						End If ' no NMI
					End if ' not relevant
					
				Case else
			End select
			
		Next ' WS_DataParameter rows
	Next ' old new
Next ' nmiSizes

'now verify they were all inserted, using a specific timestamp           gDt_Run_TS
 '                                                                       gDt_Run_TS

intStageSuccessAr(2) = 1
Reporter.ReportEvent micPass, "Stage 2 of 4 is complete/passed (Create NMIs in the CAT_MANUAL_TRANSACTIONS table)", ""
objWB_Master.save

' =========  Stage III - Run the package pkg_change_request_creator.pro_create_manual_CR to to qq??DoWhat? =============

intSQL_Pkg_ColNr  = dictWSTestRunContext_KeyName_ColNr.item("MTS_PKG_CR__PRO_CREATE_MANUAL_CR")
strSQL_RunPackage = objWS_TestRunContext.cells(2,intSQL_Pkg_ColNr).value
Dim strSQL_ExecCmds_Ar, ec, ecMax, strExec
strSQL_ExecCmds_Ar = split(strSQL_RunPackage, ";") ' the split command will remove semi-colons, so there's no need to remove them manually
ecMax = uBound(strSQL_ExecCmds_Ar)
For ec = 0 to ecMax
	strExec = trim(strSQL_ExecCmds_Ar(ec)) ' qq & ";"  '' qq suffix semi-colon ?
	if ucase(left(strExec, 5)) = "BEGIN" then _
		strExec = mid ( strExec , 6 )
	While ((left(strExec,1) = vbCr) or (left(strExec,1)=vbLf)) ' handles "CrLfEND"
		strExec = mid(strExec,2)
	Wend
	Select Case Ucase(trim(strExec))
		Case "BEGIN", "END", "" : ' do nothing
		Case else :
			init_ObjDBConnection ( objDBConn_TestData_A )
			fn_DbExec_v2 DB_CONNECT_STR_MTS, objDBConn_TestData_A, strExec, true 
	End Select
Next


intStageSuccessAr(2) = 1 ' qq how do we measure this ?
Reporter.ReportEvent micPass, "Stage 3 of 4 is complete/passed (Data-Reservation)", ""

objWB_Master.save

' =========  Stage IV  - Verify that the status in CATS is SENT, and is the correct NrOfDays since the base-date =============
                              '                          * note the initial comma to make the split-array zero-based
dim strToSplit_VerifyStages : strToSplit_VerifyStages = ",mts_verify_CATS_Sent,MTS_retrieve_Txn_Details,MTS_Retrieve_the_Logging_End_Date_value"
Dim strAr_Stages            : strAr_Stages            = split ( strToSplit_VerifyStages, ",")
Dim stg, stgMax             : stgMax                  = uBound( strAr_Stages)
Dim intAr_Stg_ColNrs        : intAr_Stg_ColNrs        = split ( ",0,0,0", ",")
for stg = 1 to stgMax                                                               ' get the column numbers for the Verify-SQL's
	intAr_Stg_ColNrs(stg) = dictWSTestRunContext_KeyName_ColNr.item(strAr_Stages(stg))
next



Dim intTotalSecondsWaited, intSecondsToWait, intWaitSeconds_Limit, tsVfyStart, tsVfyEnd
Dim strCatsCRsent_Status, strQueryReturned_Value
Dim vntQueryResult, intNumberOfDays_Expected, intNumberOfDays_Actual
dim intDaysBetween_Actual, intDaysBetween_Expected, micRet_st9

Dim strSQL_Verify
For nmiSizes = 1 To 2
	
	For oldnew  = 1 To oldnewMax ' <<<<======================
	                                  '                   oLD nEW                               Large Small
		strNrOfDays_ColName = lCase(left(strAr_listOldNew(oldnew),1)) & UCase(left(strAr_NMIsizes(nmiSizes),1)) &  "_Days"
		intNrOfDays_ColNr = dictWSDataParameter_KeyName_ColNr(strNrOfDays_ColName)
		
		str_NMI_ColumnName = "NMI_" & strAr_listOldNew(oldnew) & "`" &strAr_NMIsizes(nmiSizes)
		intTestData_NMI_ColNr = dictWSDataParameter_KeyName_ColNr(str_NMI_ColumnName)

		for r = 2 to intLast_DataParm_Row
		
			select case objWS_DataParameter.cells(r, intTestData_RowIsInScope_ColNr).value
				case "Y","y", "" :
				
					strNrOfDays = objWS_DataParameter.cells(r, intNrOfDays_E_ColNr).value
					If strNrOfDays <> strNrOfDays_Ignore Then  ' the hyphen indicates this combination of CATS_CR and NMI-Size does not occur, so do not test it   <<<<============
					
						strNMI    				  = objWS_DataParameter.cells(r, intTestData_NMI_ColNr).value
						str_NMI_withoutCheckDigit = left(strNMI, len(strNMI)-1) ' this assumes that all inbound NMIS have a check digit that must be removed QQ
											
						For stg = 1 To stgMax  ' for all the verification stages (as of 2016`11Nov`18Fri, these are 3 queries)
						
							strSQL_Verify = objWS_TestRunContext.cells(2, intAr_Stg_ColNrs(stg)).value ' qq this hard-coded "2" could be dangerous ?
							strSQL_Verify = replace ( strSQL_Verify , "<NMI>", str_NMI_withoutCheckDigit, 1, -1, vbTextCompare)

							init_ObjDBConnection objDBConn_TestData_A   
							init_ObjDBRecordSet  objDBRcdSet_TestData_A
							
							If stg = 1 Then
								
								tsVfyStart = now 
								intWaitSeconds_Limit = 10 * 60 ' 10 minutes
								intTotalSecondsWaited = 0
								intSecondsToWait = 10
								Do
									fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_Verify, 0
									If objDBRcdSet_TestData_A.eof Then
										strCatsCRsent_Status = "Row.Absent" 
										wait intSecondsToWait : intTotalSecondsWaited = intTotalSecondsWaited + intSecondsToWait
									Else 
										strQueryReturned_Value = objDBRcdSet_TestData_A.Fields("Txn_Status").Value
										If strQueryReturned_Value = "SENT" Then 
											strCatsCRsent_Status = "Row.Sent"  '  <<<=== this is the sought-outcome
										else 
											strCatsCRsent_Status = strQueryReturned_Value
											wait intSecondsToWait : intTotalSecondsWaited = intTotalSecondsWaited + intSecondsToWait
										End if
									End if
									objDBRcdSet_TestData_A.close  : objDBConn_TestData_A.close
									
								Loop While ((intTotalSecondsWaited < intWaitSeconds_Limit) and (strCatsCRsent_Status <> "Row.Sent"))
								tsVfyEnd = now 
									
								if strCatsCRsent_Status <> "Row.Sent" then
									Reporter.ReportEvent micFail, "NMI.Status `" & strNMI & "." & strCatsCRsent_Status & "` was not located ..", ".. within `" & intWaitSeconds_Limit & "` seconds."
									Exit for   ' do no further verification-stages for this NMI
								End if
								fnReportTimeDelta  _
									"NMI.Status `" & strNMI & "." & strCatsCRsent_Status & "` - Time taken to locate was : ", tsVfyStart, tsVfyEnd
							End if	
							If ((stg > 1) and (strCatsCRsent_Status <> "Row.Sent")) Then ' qq only if the row was present and sent
							
							'	the row is present and has status SENT, so verify that the 
							'	its OBJECTION_LOGGING_END_DATE is set to the correct number of days
								fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_Verify, 0
								If objDBRcdSet_TestData_A.eof Then 
									vntQueryResult = "Row.None"
									reporter.ReportEvent micFail, strAr_Stages(stg) & " - failed to return any rows, sql is : " , strSQL_Verify
								else
									vntQueryResult = objDBRcdSet_TestData_A.Fields(0).value
									If     stg = 2 Then 
									'	here, the vntQueryResult contains the CR_UPDATE_DT - save this in this row of the RunSheet
										objWS_DataParameter.cells(r, intTestData_NMI_ColNr + 1).value = vntQueryResult 
									ElseIf stg = 3 Then 
									'	here, the vntQueryResult contains the OBJECTION_LOGGING_END_DATE
										intDaysBetween_Actual   = DateDiff("d", vntQueryResult, dtCatsManualTxn_BaseFromDt)
										intDaysBetween_Expected = objWS_DataParameter.cells(r, intNrOfDays_ColNr).value
										micRet_st9 = fn_micTF_Result ( intDaysBetween_Actual = intDaysBetween_Expected, micPass, micFail)
										Reporter.ReportEvent micRet_st9, "Days-between expected/actual ?equal? - `" & _
											intDaysBetween_Expected & "/" & intDaysBetween_Actual & "`", _
											"Base/Days/Result are `" & dtCatsManualTxn_BaseFromDt & "/" & intDaysBetween_Expected & "/" & vntQueryResult & "`"
										objWS_DataParameter.cells(r, intNrOfDays_ColNr     + 2).value = vntQueryResult ' this is the ExpiryDt as TimeStamp
										objWS_DataParameter.cells(r, intNrOfDays_ColNr     + 1).value = intDaysBetween_Actual
									End If
								End if
								
								objDBRcdSet_TestData_A.close  : objDBConn_TestData_A.close
								
							End if ' stg >  1
							
						Next ' for all the verification stages (as of 2016`11Nov`18Fri, these are 3 queries)
						
					End if ' only if the days are not "-"
						
			end select ' only for in-scope rows

		Next ' WS_DataParameter rows
	Next ' old new
Next ' nmiSizes

objWB_Master.save

'  <<<<======================

intStageSuccessAr(4) = 1 ' qq how do we measure this ?
Reporter.ReportEvent micPass, "Stage 4 of 4 is complete/passed (Data-Reservation)", ""


' =========  Final Stage  =============
'  Report Overall Success / Fail

intStageSuccessCount = 0
For ctSucc = 1 To intWantedSuccessCount
	intStageSuccessCount = intStageSuccessCount + intStageSuccessAr(ctSucc)
Next
micFinalStatus = micFail 
If intStageSuccessCount = intWantedSuccessCount Then _
   micFinalStatus = micPass ' qq log the statuses above

Reporter.ReportEvent micFinalStatus, "EndOfCase Status, SuccessCount Expected/Actual is " & intStageSuccessCount & "/" & intWantedSuccessCount & ".", ""

objWB_Master.save
objWB_Master.close