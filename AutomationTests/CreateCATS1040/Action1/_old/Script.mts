﻿' Option explicit ' qq reinstate later as a coding standard

' =========  Stage 0   - Technical Setup           =============

'Get the input parameters
'Company: CITI, PCOR, SAPN
'ENV: DEV, SYSTEST, UAT
'RunCount: Number of iterations

Dim micFinalStatus, intStageSuccessCount, intWantedSuccessCount : intStageSuccessCount = 0
Dim ctSucc, intStageSuccessAr : intStageSuccessAr = split(",0,0,0,0", ",") : intWantedSuccessCount = uBound(intStageSuccessAr)

Dim strColNm_Expected_CatsResponse_AllValues, strColNm_Actual_CatsResponse_AllValues

Dim objDBConn_TestData_A, objDBRcdSet_TestData_A 
Dim strExpectedMessage, strFinalMessage, strDateDiff
dim r

Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")

Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")

' qq delete Environment.Value("RUNCOUNT")=Parameter.Item("RunCount")

Dim strAr_listOldNew, intSize_listOldNew, str_listOldNew
str_listOldNew = ucase(Parameter.Item("listOldNew")) ' for debugging
if left(str_listOldNew,1) <> "," then str_listOldNew = "," & str_listOldNew
Environment.Value("listOldNew") = str_listOldNew 

strAr_listOldNew = split(Environment.Value("listOldNew"), ",")  ' this list will be one of :  old  new old,new  so we split the list with a comma to find out what permutations we're going to test
intSize_listOldNew = uBound(strAr_listOldNew)

Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO" ' "MTS_FIND_MRIM_SO"

Dim objXLapp, objWB_Master, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
dim objWS_DataParameter, intLast_DataParm_Row, int_NrOf_InScope_Rows, int_NrOf_NMI_rows_required, dictWSDataParameter_KeyName_ColNr

' Load the MasterWorkBook objWB_Master
Environment.Value("DbQuery_Scenario") = "MTS_FIND_RRIM" ' "Find_RRIM"
fnExcel_CreateAppInstance  objXLapp, true
LoadData_RunContext objXLapp, objWB_Master, objWS_TestRunContext, 1,  BASE_AUTOMATION_DIR & "DataSheets\DataLoader.xls", dictWSTestRunContext_KeyName_ColNr

'fnWS_LoadKey_ColOffsets_toDictionary objWS_TestRunContext, 1, 1, 50, "", dictWSTestRunContext_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
Dim DB_CONNECT_STR_MTS 
DB_CONNECT_STR_MTS = "Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & Environment.Value("HOSTNAME") & ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & Environment.Value("SERVICENAME") & "))); User ID=" & Environment.Value("USERNAME") & ";Password=" & Environment.Value("PASSWORD") & ";"


' load the DataParameter worksheet objWS_DataParameter - this contains the DataScenarios to test
'
set objWS_DataParameter    = objWB_Master.worksheets("CATS_OUT_2001_Objections")
intLast_DataParm_Row       = objWS_DataParameter.range("rgWS_LastRow_PlusOne").row - 1
int_NrOf_InScope_Rows      = objWS_DataParameter.range("rgWS_NumberOf_InScope_Inbound_Rows").value ' excel named ranges : alt-I-N-D
int_NrOf_InScope_Rows      = cInt(int_NrOf_InScope_Rows * 1.5) + 5 ' get some extra rows in case some of those identified are reserved for other cases
int_NrOf_NMI_rows_required = int_NrOf_InScope_Rows * (intSize_listOldNew + 1)
'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, 1, 1, 50, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"

Dim intSQL_ColNr, strActive_Tier_1_NMIs 
Dim strQueryTemplate, dtD, Hyph, strNMI

Dim strList_NMIsizes, strAr_NMIsizes, nmiSizes
strList_NMIsizes = ",SMALL"  ' for this case, there are no large or small NMIS, but we re-use the SMALL NMI columns in the datasheet  ",SMALL,LARGE"
strAr_NMIsizes = split(strList_NMIsizes, ",")

Dim oldnew, oldnewMax, n
oldnewMax = intSize_listOldNew

Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_InOut_Bound

intTestData_RowIsInScope_ColNr 	= dictWSDataParameter_KeyName_ColNr("InScope")
intCatsCR_ColNr                	= dictWSDataParameter_KeyName_ColNr("CATS_CR")
'intColNR_BaseDate				= dictWSDataParameter_KeyName_ColNr("BaseDate") 
'intColNR_DateOffset				= dictWSDataParameter_KeyName_ColNr("DayOffset")
intColNr_InOut_Bound             = dictWSDataParameter_KeyName_ColNr("InboundOutbound")

Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0

Dim intSQL_Pkg_ColNr, strSQL_RunPackage

dim uftapp : Set uftapp = CreateObject("QuickTest.Application") 
strTest = uftapp.Test.Name

Environment.Value("TESTFOLDER") = "C:\_data\CATS\"

strCATSStatus = "OBJECTED"
strObjectionCode = "BADMETER"
						'strObjectionExplanation = "Invalid combination of crcode, meter type and meter read type code" ----as explanation can change 

'Set the folder for results
'Environment.Value("TEST_RUN_SOURCE_FOLDER")="C:\_data\CATS\"

'				Environment.Value("DbQuery_Scenario") = "Find_RRIM"
'				'Load the environment details for database access and get the database queries
'				 Loaddata
'


' =========  Stage I   - Find NMI's in qq??? state to create Incoming CATS requests against =============

Dim intNrOfDays_E_ColNr, intNrOfDays_A_ColNr, strNrOfDays_ColName, strNrOfDays, strNrOfDays_Ignore, intColNr_Actual_CatsResponse_AllValues, intColNr_Expected_CatsResponse_AllValues, intColNr_Actual_CatsResponse_DateDiff
strNrOfDays_Ignore = "-"

fnMTS_WinClose ' qq cleanup RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration
fnMTS_Open_wEnvVars ' qq remove following :   RunAction "A_MTS_OpenMTS [A_MTS_OpenMTS]", oneIteration

For nmiSizes = 1 To Ubound(strAr_NMIsizes)

	intSQL_ColNr     = dictWSTestRunContext_KeyName_ColNr.item("MTS_FIND_RRIM")  
	strQueryTemplate = objWS_TestRunContext.cells(2,intSQL_ColNr).value
	
	For oldnew  = 1 To oldnewMax
	
		strColNm_Expected_CatsResponse_AllValues    = left(strAr_listOldNew(oldnew),1) & "CstRA_E" ' "_Cats Status Response All"	
		intColNr_Expected_CatsResponse_AllValues	= dictWSDataParameter_KeyName_ColNr(strColNm_Expected_CatsResponse_AllValues)
		strColNm_Actual_CatsResponse_AllValues      = left(strAr_listOldNew(oldnew),1) & "CstRA_A" ' "_Cats Status Response All"	
		intColNr_Actual_CatsResponse_AllValues 		= dictWSDataParameter_KeyName_ColNr(strColNm_Actual_CatsResponse_AllValues)
		strColNm_Expected_CatsResponse_DateDiff      = left(strAr_listOldNew(oldnew),1) & "S_Days_E" ' "_Cats Status Response All"	
		intColNr_Expected_CatsResponse_DateDiff 		= dictWSDataParameter_KeyName_ColNr(strColNm_Expected_CatsResponse_DateDiff)
		strColNm_Actual_CatsResponse_DateDiff      = left(strAr_listOldNew(oldnew),1) & "S_Days_A" ' "_Cats Status Response All"	
		intColNr_Actual_CatsResponse_DateDiff 		= dictWSDataParameter_KeyName_ColNr(strColNm_Actual_CatsResponse_DateDiff)

	                                  '                   oLD nEW                               Large Small
		strNrOfDays_ColName = lCase(left(strAr_listOldNew(oldnew),1)) & UCase(left(strAr_NMIsizes(nmiSizes),1)) &  "_Days_E"
		intNrOfDays_E_ColNr = dictWSDataParameter_KeyName_ColNr(strNrOfDays_ColName)
		intNrOfDays_O_ColNr = intNrOfDays_E_ColNr + 1 ' the actuals column is 1-right of the expected
	
		strActive_Tier_1_NMIs = strQueryTemplate
		'strActive_Tier_1_NMIs = replace ( strActive_Tier_1_NMIs , "<NMI_Size>", strAr_NMIsizes(nmiSizes), 1, -1, vbTextCompare)
		strActive_Tier_1_NMIs = replace ( strActive_Tier_1_NMIs , "<rownum>"  , int_NrOf_InScope_Rows   , 1, -1, vbTextCompare) ' qq
		fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strActive_Tier_1_NMIs, int_NrOf_InScope_Rows
		
		
		str_NMI_ColumnName = "NMI_" & strAr_listOldNew(oldnew) & "`" &strAr_NMIsizes(nmiSizes)
		intTestData_NMI_ColNr = dictWSDataParameter_KeyName_ColNr(str_NMI_ColumnName)
	
		str_dlData = "" :  str_dlSuffix = ""
		for r = 3 to intLast_DataParm_Row ' Starting from row 3 as first 2 rows are static
			
			reporter.ReportEvent micPass, "Executing row " & r ,"Executing row " & r 
			
			
			if trim(lcase(objWS_DataParameter.cells(r, dictWSDataParameter_KeyName_ColNr("InScope")).value)) = "y" and _
				trim(lcase(objWS_DataParameter.cells(r, dictWSDataParameter_KeyName_ColNr("InboundOutbound")).value)) = "inbound" then
			
			
			
			' ====  Stage Ia - get a NMI  =====
			
				strCatsCR   = objWS_DataParameter.cells(r, intCatsCR_ColNr    ).value
				strNrOfDays = objWS_DataParameter.cells(r, intNrOfDays_E_ColNr).value
				
				' ignore rule-combinations that are out of scope
				If strNrOfDays <> strNrOfDays_Ignore Then  ' the hyphen indicates this combination of CATS_CR and NMI-Size does not occur, so do not test it   <<<<============
			'	qq this may not be required for CATS1040, qq CHECK !!
					
					If objDBRcdSet_TestData_A.EOF Then
						Reporter.ReportEvent micFail, "CATS_OUT_2001_Objection_a.Loc`Line127 EOF encountered, sql is : ", strSQL_FindActiveNmi_ofSize
						' qq change msg in line above
						ExitTest 
					End If
	
				'	reserve the NMI until the reservation is successful
					strNMI = objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
'									intReserveDataRC = fnReserveDataForCase_NMI ( strNMI, "CATS_OUT_2001_Objection_a", "", GstrDate_Run_TS_Suffix, "", strReserveData_ResultDesc)
'									While intReserveDataRC < gDataRsvnSt_Approved
'										objDBRcdSet_TestData_A.MoveNext	
'										strNMI = objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
'										intReserveDataRC = fnReserveDataForCase_NMI ( strNMI, "CATS_OUT_2001_Objection_a", "", qqpiStr_RunNR, "", strReserveData_ResultDesc)
'									Wend
					Environment.Value("NMI")=strNMI
					
					objWS_DataParameter.cells(r, intTestData_NMI_ColNr).value = strNMI
					
					' add the NMI to the reporting row for reporting at the end of loop
					str_dlLine = fnReplaceElements(str_NMI_ColumnName & "(,,)=,,", ",," & objWS_DataParameter.cells(r, intCatsCR_ColNr).value & ",," & strNMI)
					str_dlData = str_dlData & str_dlSuffix & str_dlLine
					str_dlSuffix = vbCrLf ' "," & vbCrLf
					
					objDBRcdSet_TestData_A.MoveNext
					
					
					' ====  Stage Ib - Create an Inbound-XML around the NMI retrieved in stage Ia, and put it on the Inbound Queue =====
					
		
					
					reporter.ReportEvent micPass, "Starting with row [" & r &"] having NMI [" & strNMI & "]", "Starting with row [" & r &"] having NMI [" & strNMI & "]"
					
					gInt_SQL_Fail_Count = 0
					
					'Get the query for fetching LNSP
					GetRole strNMI,"FRMP"
					strFRMP = ExecuteDatabaseQuery
					
					'Get the query for fetching LNSP
					GetRole strNMI,"LNSP"
					strLNSP = ExecuteDatabaseQuery
					
					'Get the query for fetching MDP
					GetRole strNMI,"MDP"
					strMDP = ExecuteDatabaseQuery
					
					'Get the query for fetching RP
					GetRole strNMI,"RP"
					strRP = ExecuteDatabaseQuery'
					
					'CATS CR Code
					' strCRcode = "1040"
					strCRcode = trim(lcase(objWS_DataParameter.cells(r, intCatsCR_ColNr).value))
					
					
					'Get the prospective retailer that is different to current FRMP
					GetProspectiveRetailer strFRMP
					strProsRetailer = ExecuteDatabaseQuery
					
					strProposedDate = date
					strReadTypeCode= "PR"
					strInitTxnID = strProsRetailer&"-"&strNMI&"-"&datepart("d",date)&hour(time)&minute(time)&second(time)
					
					
					If gInt_SQL_Fail_Count > 0 Then
'						Reporter.ReportEvent micFail,, "Nr of fails was : `" & gInt_SQL_Fail_Count
						Reporter.ReportEvent micFail,"Fail","Nr of fails was : `" & gInt_SQL_Fail_Count
						
					else
										
						strRequestId = datepart("y",date)&Right("0" & DatePart("m",Date), 2)&Right("0" & DatePart("d",Date), 2)&hour(time)&minute(time)&second(time)
						
						'Create xml REQ LNSP
						strRole = "LNSP"
						strChangeStatusCode = "REQ"
						strFileName = "CATS_"&strNMI&"_"&strRole&"_"&strCRcode&"_"&strReadTypeCode&"_"&strChangeStatusCode&"_"&datepart("d",date)&hour(time)&minute(time)&second(time)
						CreateCATSXML strNMI,strLNSP,strMDP,strRP,strCRcode,strProsRetailer,strChangeStatusCode,strProposedDate,strReadTypeCode,strRole,strRequestId,strInitTxnID,strFileName
						ZipFile strFileName&".xml",Environment.Value("TESTFOLDER")
						
						'strCATSDestinationFolder = 
						GetCATSDestinationFolder Environment.Value("COMPANY"),Environment.Value("ENV"),strRole
						'msgbox Environment.Value("CATSDestinationFolder")
						copyfile strFileName&".zip",Environment.Value("TESTFOLDER"),Environment.Value("CATSDestinationFolder")
						
						wait 3
	
						' ====  Stage Ic - In MTS, immediately lookup the NMI and verify that its ObjectionCode matches the ExpectedValue from the DriverSheet =====
						
						strExpectedMessage = strChangeStatusCode & "`" & strCATSStatus & "`" & "ON" & "`" & strObjectionCode & "`" _
						& "`Date Diff=" &  strNrOfDays
						objWS_DataParameter.cells(r, intColNr_Expected_CatsResponse_AllValues).value = strExpectedMessage ' capture expected message in data sheet
						
						fnCheckCats strRequestId, strNMI, strChangeStatusCode, strProsRetailer, strCATSStatus, strObjectionCode, strFinalMessage, strDateDiff 
						
						objWS_DataParameter.cells(r, intColNr_Actual_CatsResponse_AllValues).value = strFinalMessage ' capture final message in data sheet
		
						objWS_DataParameter.cells(r, intColNr_Actual_CatsResponse_DateDiff).value = strDateDiff  'capture datediff in data sheet
						'	qq comment this, and remove below
					'	RunAction "A_MTS_CheckCATS [A_MTS_CheckCATS]", oneIteration,strRequestId,strNMI,strChangeStatusCode,strProsRetailer,strCATSStatus,strObjectionCode,strObjectionExplanation

					End If
			
				End if
			End if

		Next ' rows
	Next ' old/new
Next ' large / small


Reporter.ReportEvent micDone, "NMIs harvested for `" & str_NMI_ColumnName & "` are : ", str_dlData

'Next

fnMTS_WinClose ' qq cleanup RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration



		'	PbWindow("w_frame").Activate @@ hightlight id_;_3737214_;_script infofile_;_ZIP::ssf1.xml_;_
		'	PbWindow("w_frame").Move 199,25 @@ hightlight id_;_3737214_;_script infofile_;_ZIP::ssf2.xml_;_
		'	PbWindow("w_frame").Resize 1344,937 @@ hightlight id_;_3737214_;_script infofile_;_ZIP::ssf3.xml_;_
		'	PbWindow("w_frame").Move 199,25 @@ hightlight id_;_3737214_;_script infofile_;_ZIP::ssf4.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 1313,53 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf5.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 53,813 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf6.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 1277,810 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf7.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Resize 1503,875 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf8.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Move -366,2 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf9.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 787,837 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf10.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 1239,812 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf11.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Resize 1320,875 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf12.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Move -198,2 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf13.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 105,806 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf14.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 1298,806 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf15.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 1298,806 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf16.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Resize 1253,875 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf17.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Move -14,2 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf18.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 1316,791 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf19.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 1316,791 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf20.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 1316,791 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf21.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Resize 1253,683 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf22.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Move -14,-78 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf23.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Resize 1257,646 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf24.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Move -14,-78 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf25.xml_;_
		'	PbWindow("w_frame").Resize 1287,767 @@ hightlight id_;_3737214_;_script infofile_;_ZIP::ssf26.xml_;_
		'	PbWindow("w_frame").Move 199,25 @@ hightlight id_;_3737214_;_script infofile_;_ZIP::ssf27.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 1262,35 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf28.xml_;_
		'	PbWindow("w_frame").Resize 1296,819 @@ hightlight id_;_3737214_;_script infofile_;_ZIP::ssf29.xml_;_
		'	PbWindow("w_frame").Move 199,25 @@ hightlight id_;_3737214_;_script infofile_;_ZIP::ssf30.xml_;_
		'	PbWindow("w_frame").PbObject("mdi_1").Click 19,689 @@ hightlight id_;_1640604_;_script infofile_;_ZIP::ssf31.xml_;_
		'	PbWindow("w_frame").Move 179,24 @@ hightlight id_;_3737214_;_script infofile_;_ZIP::ssf32.xml_;_
		'	PbWindow("w_frame").Resize 1317,798 @@ hightlight id_;_3737214_;_script infofile_;_ZIP::ssf33.xml_;_
		'	PbWindow("w_frame").Move 179,24 @@ hightlight id_;_3737214_;_script infofile_;_ZIP::ssf34.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Resize 1265,657 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf35.xml_;_
		'	PbWindow("w_frame").PbWindow("w_search_cats_notifications_recvd").Move 2,2 @@ hightlight id_;_1443462_;_script infofile_;_ZIP::ssf36.xml_;_