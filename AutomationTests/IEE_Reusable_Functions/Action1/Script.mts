﻿


SwfWindow("Itron_EE_ApplicationWindow").SwfObject("menu_Main").Click 38,11 @@ hightlight id_;_329004_;_script infofile_;_ZIP::ssf16.xml_;_
SwfWindow("SwfWindow").SwfObject("SwfObject").Drag 52,5 @@ hightlight id_;_197054_;_script infofile_;_ZIP::ssf17.xml_;_
SwfWindow("SwfWindow").SwfObject("SwfObject").Drop 72,15 @@ hightlight id_;_197054_;_script infofile_;_ZIP::ssf18.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("CommandDockBarTop").Click 82,33 @@ hightlight id_;_197172_;_script infofile_;_ZIP::ssf19.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("menu_Main").Click 39,12 @@ hightlight id_;_329004_;_script infofile_;_ZIP::ssf20.xml_;_
SwfWindow("SwfWindow").SwfObject("SwfObject").Click 48,33 @@ hightlight id_;_197054_;_script infofile_;_ZIP::ssf21.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("menu_Main").Click 44,12 @@ hightlight id_;_329004_;_script infofile_;_ZIP::ssf22.xml_;_
SwfWindow("SwfWindow").SwfObject("SwfObject").Click 33,49 @@ hightlight id_;_197054_;_script infofile_;_ZIP::ssf23.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("menu_Main").Click 44,7 @@ hightlight id_;_329004_;_script infofile_;_ZIP::ssf24.xml_;_
SwfWindow("SwfWindow").SwfObject("SwfObject").Click 48,67 @@ hightlight id_;_197054_;_script infofile_;_ZIP::ssf25.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("menu_Main").Click 31,12 @@ hightlight id_;_329004_;_script infofile_;_ZIP::ssf26.xml_;_
SwfWindow("SwfWindow").SwfObject("SwfObject").Click 41,92 @@ hightlight id_;_197054_;_script infofile_;_ZIP::ssf27.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("menu_Main").Click 33,11 @@ hightlight id_;_329004_;_script infofile_;_ZIP::ssf28.xml_;_
SwfWindow("SwfWindow").SwfObject("SwfObject").Click 43,114 @@ hightlight id_;_197054_;_script infofile_;_ZIP::ssf29.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("menu_Main").Click 32,17 @@ hightlight id_;_329004_;_script infofile_;_ZIP::ssf30.xml_;_
SwfWindow("SwfWindow").SwfObject("SwfObject").Click 43,138 @@ hightlight id_;_197054_;_script infofile_;_ZIP::ssf31.xml_;_






SwfWindow("Itron_EE_ApplicationWindow").SwfObject("groupBar1").Click 107,673 @@ hightlight id_;_262550_;_script infofile_;_ZIP::ssf32.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("groupBar1").Click 101,697 @@ hightlight id_;_262550_;_script infofile_;_ZIP::ssf33.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("groupBar1").Click 96,725 @@ hightlight id_;_262550_;_script infofile_;_ZIP::ssf34.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("groupBar1").Click 101,743 @@ hightlight id_;_262550_;_script infofile_;_ZIP::ssf35.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("groupBar1").Click 95,771 @@ hightlight id_;_262550_;_script infofile_;_ZIP::ssf36.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("groupBar1").Click 94,796 @@ hightlight id_;_262550_;_script infofile_;_ZIP::ssf37.xml_;_






int_mini_wait_time = 10

'fnIEE_WinClose
'OpenIEE "CITI", "DEVMC"
'fnIEE_Login "testautomation", "testautomatio1n"
'
fnIEE_WinClose
OpenIEE "CITI", "DEVMC"
fnIEE_Login "testautomation", "testautomation"
fnIEE_WinSize "maximize"

' fnIEE_WinClose

sub fnIEE_WinClose
	On error resume next
	 	SystemUtil.CloseProcessByName "UiApplication.exe"
		PrintMessage "i","Close IEE", "Closing all open instances of IEE"
		Err.clear
	On error goto 0
End sub

sub fnIEE_WinSize (strsize)
	On error resume next
		Select Case lcase(strsize)
			Case "maximize"
				SwfWindow("Itron_EE_ApplicationWindow").Maximize
			Case "minimize"
				SwfWindow("Itron_EE_ApplicationWindow").Minimize
		End Select
		Err.clear
	On error goto 0
End sub


Function fn_IEE_MenuNavigation (strMenu_Navigation_Path)

    '################################################################################
    '
    '  Function Name:- fn_IEE_MenuNavigation
    '  Description:-  This function would be open a particular menu in IEE application
    '  Parameters:-    1)strMenu_Navigation_Path - Semi colon seperated menu navigation path E.g. Transactions;Meter Data Sent;Search Verify Meter Data Request Sent
    '  Return Value:-  returns the transaction status for a particular record (value can be OBJECTED, PROCESSED etc)
    '  Creator:- Umesh Anand
    '################################################################################
    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
        Reporter.ReportEvent micDone, "fn_MTS_MenuNavigation function", "Exiting function as there was some error in previous step"
        Exit function
    End If


	wait 0,int_mini_wait_time
	On error resume next
		SwfWindow("Itron_EE_ApplicationWindow").Activate
	on error goto 0



SwfWindow("Itron_EE_ApplicationWindow").SwfObject("menu_Main").Click 26,7 @@ hightlight id_;_1180678_;_script infofile_;_ZIP::ssf12.xml_;_
SwfWindow("SwfWindow").SwfObject("SwfObject").Click 54,28 @@ hightlight id_;_2098984_;_script infofile_;_ZIP::ssf13.xml_;_
SwfWindow("Itron_EE_ApplicationWindow").SwfObject("menu_Main").Click 43,7 @@ hightlight id_;_1180678_;_script infofile_;_ZIP::ssf14.xml_;_
SwfWindow("SwfWindow").SwfObject("SwfObject").Click 55,238 @@ hightlight id_;_2098984_;_script infofile_;_ZIP::ssf15.xml_;_


    Select Case lcase(strMenu_Navigation_Path)

        Case "i5-transactions;cats;search cats notifications received"
            Window("MTS_Window").WinMenu("Menu").Select "Transactions;CATS;Search CATS Notifications Received"
        Case "transactions;meter data sent;search provide meter data request sent"
            Window("MTS_Window").WinMenu("Menu").Select "Transactions;Meter Data Sent;Search Provide Meter Data Request Sent"
        Case "transactions;meter data sent;search verify meter data request sent"
            Window("MTS_Window").WinMenu("Menu").Select "Transactions;Meter Data Sent;Search Verify Meter Data Request Sent"
        Case "transactions;meter data received;search verify meter data request received"
            Window("MTS_Window").WinMenu("Menu").Select "Transactions;Meter Data Received;Search Verify Meter Data Request Received"
        Case "transactions;meter data received;search provide meter data request received"
            Window("MTS_Window").WinMenu("Menu").Select "Transactions;Meter Data Received;Search Provide Meter Data Request Received"
        Case "transactions;customer / site details;search site access notifications received"
            Window("MTS_Window").WinMenu("Menu").Select "Transactions;Customer / Site Details;Search Site Access Notifications Received"
        Case "transactions;customer / site details;search site access request received"
            Window("MTS_Window").WinMenu("Menu").Select "Transactions;Customer / Site Details;Search Site Access Request Received"
        Case "transactions;service orders;service order request search" 
            Window("MTS_Window").WinMenu("Menu").Select "Transactions;Service Orders;Service Order Request Search"
        Case "transactions;all aqs" 
            Window("MTS_Window").WinMenu("Menu").Select "Transactions;All AQs"
    End Select
    
End Function ' end of fn_IEE_MenuNavigation



Function fnIEE_Login(strUserName, strPassword)
	'################################################################################
    '
    '  Function Name:- fnIEE_Login
    '  Description:-  Function to login IEE  CIS application
    '   Parameters:-    1) strUserName - user id
    '                   2) strPassword - password
    ' Return Value:-  None
    ' Creator:- Umesh Anand
    '
    '################################################################################
	Dim temp_error_Message 
	' On error resume next  
	
    	wait 0,int_mini_wait_time

	    If trim(strUserName) <> "" Then
	    	PrintMessage "p", "fnIEE_Login function", "Entering username ("& strUserName &")"
		SwfWindow("Window_Login").SwfEdit("txtUserName").Set strUserName
	    End If
	
	    If trim(strPassword) <> "" Then
		PrintMessage "p", "fnIEE_Login function", "Entering password ("& strPassword &")"
		SwfWindow("Window_Login").SwfEdit("txtPassword").Set strPassword
	    End If
	
		' click on Ok button
		SwfWindow("Window_Login").SwfButton("btn_OK").Click @@ hightlight id_;_3147826_;_script infofile_;_ZIP::ssf11.xml_;_
		
		' Check if invalid user details window is displayed

		If SwfWindow("Itron_IEE_Banner").SwfWindow("Login_Error").Exist(5) Then
			' capture error message
			temp_error_Message = SwfWindow("Itron_IEE_Banner").SwfWindow("Login_Error").SwfLabel("txt_Error").GetVisibleText
			gFWbln_ExitIteration = "Y"
			gFWstr_ExitIteration_Error_Reason = "Unable to login because of error (" & temp_error_Message & ")"
			PrintMessage "f", "fnIEE_Login function - LOGIN ERROR", gFWstr_ExitIteration_Error_Reason 
			' click OK on error message screen
			SwfWindow("Itron_IEE_Banner").SwfWindow("Login_Error").SwfButton("btnOK").Click
			
			' Click cancel on logon screen
			SwfWindow("Window_Login").SwfButton("btn_Cancel").Click

		End If 
	
	' On error goto 0

End Function ' end of Function fnCIS_Login(strUserName, strPassword, strCompany)