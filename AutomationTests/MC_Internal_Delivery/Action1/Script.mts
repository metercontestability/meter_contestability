﻿
Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong

Dim intScratch

gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function

' Dim bln_Scratch :  bln_Scratch = cbool(true)

dim intColNr_InScope, intColNr_RowCompletionStatus
intColNr_InScope = cint(-1) : intColNr_RowCompletionStatus = cint(-1)

Dim str_rowScenarioID, intColNR_ScenarioID

Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true

Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch
Dim thisCell
Dim strPhase, strStage
Dim gFWcell_xferNMI 
Dim strBarText, strBarText_Save, strMsg
Dim cStr_BarText_Format
Dim temp_DataPrep_CIS_SOType, temp_DataPrep_CIS_SOReason , temp_DataPrep_CIS_TaskType, str_Required_SP_Status_MTS, temp_NMI_List

BASE_XML_Template_DIR = cBASE_XML_Template_DIR

cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^RequestID`{4}^NMI`{5} ."

Dim strFolderNameWithSlashSuffix_Scratch
' strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"


' push the RunStats to the DriverWS report-area
Dim iTS, iTS_Max, rowTL, colTL

Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq

gDt_Run_TS = tsOfRun

Dim BASE_GIT_DIR ' : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"
BASE_GIT_DIR =   cBASE_GIT_DIR

Dim strCaseBaseAutomationDir 
strCaseBaseAutomationDir  = BASE_GIT_DIR


' ####################################################################################  UFT configurations at the start of test  - START ####################################################################################  

Dim Temp_Execution_Results_Location  : Temp_Execution_Results_Location = cTemp_Execution_Results_Location
Dim strRunLog_Folder

dim qtTest, qtResultsOpt, qtApp


Set qtApp = CreateObject("QuickTest.Application")    ' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
If qtApp.launched <> True then
qtApp.Launch
End If

qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
qtApp.Options.Run.RunMode = "Fast"
qtApp.Options.Run.ViewResults = False


Set qtTest = qtApp.Test                                            ' http://www.geekinterview.com/question_details/64501
Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")     ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539


Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
str_AnFw_FocusArea = ""
else
str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
End If

strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\", array(fnAttachStr(Temp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
gQtTest.Name                                 , _
Parameter.Item("Environment")                       , _
Parameter.Item("Company")                           , _
fnTimeStamp(gDt_Run_TS, "YYYY`MM`DD_HH`mm`ss")))
' fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))



Path_MultiLevel_CreateComplete strRunLog_Folder
gFWstr_RunFolder = strRunLog_Folder

qtResultsOpt.ResultsLocation = gFWstr_RunFolder
gQtResultsOpt.ResultsLocation = strRunLog_Folder


qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
qtApp.Options.Run.AutoExportReportConfig.ExportLocation = gFWstr_RunFolder
qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
qtApp.Options.Run.ScreenRecorder.RecordSound = False
qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True

'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html

gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
gQtApp.Options.Run.ViewResults                = False

strFolderNameWithSlashSuffix_Scratch = strRunLog_Folder


' ####################################################################################  UFT configurations at the start of test  - END ####################################################################################  

' ####################################################################################  Scratch dictionary to be used across applications - START ####################################################################################  

initialize_scratch_Dictionary

'Set gdict_scratch = CreateObject("Scripting.Dictionary")
'gdict_scratch.CompareMode = vbTextCompare
'
clear_scratch_Dictionary gdict_scratch

' ####################################################################################  Scratch dictionary to be used across applications - End  ####################################################################################  



'  Description:-  Function to replace arguments in a string format
'   Parameters:-    1) str_StringFormat - a template string format. An e.g. of template is 
'						cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^CRCode`{4}^Phase`{5}^Role`{6}^Stage`{7}^RequestID`{8}^NMI`{9} ."
'						The template should have arguments within {} and the numbering should s

If 1 = 1  Then		

	Environment.Value("COMPANY")=Parameter.Item("Company")
	Environment.Value("ENV")=Parameter.Item("Environment")
	
	' Load the MasterWorkBook objWB_Master
	Dim strWB_noFolderContext_onlyFileName  
	strWB_noFolderContext_onlyFileName = Parameter.Item("Plan_Book_Name_With_Ext")
	
	Dim wbScratch
	Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
	dim  dictWSDataParameter_KeyName_ColNr
	
	fnExcel_CreateAppInstance  objXLapp, true
	
	Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO"
	' LoadData_RunContext_V2  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
	LoadData_RunContext_V3  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
	
	' ####################################################################################################################################################################################
	' #####################################################                 LOGGING STEP 1 - SETUP      #################################################################################################
	' ####################################################################################################################################################################################
	
	' fn_setup_Test_Execution_Log Parameter.Item("Company"), Parameter.Item("Environment"), "N", "MC_Internal_Delivery", GstrRunFileName, "USBChanges", strRunLog_Folder, gObjNet.ComputerName
	Printmessage "i", "Start of Execution", "Starting execution on MC Internal Delivery"
	
	Set objWB_Master = wbScratch 
	Set wbScratch = nothing
	
	objXLapp.Calculation = xlManual
	'objXLapp.screenUpdating=False
	
	Dim objWS_DataParameter 						 
	set objWS_DataParameter           					= objWB_Master.worksheets("USBChanges")
	objWS_DataParameter.activate
	
	Dim objWS_templateXML_TagDataSrc_Tables 	
	set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
	set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference
	
	Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
	' qq move this assignment inside the function that uses it,   use    ==>>>    if   gVntAr is nothing then instantiate and initialize it, as used in   fn      fnScenarioOutcome_Verify_verNN
	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
	'	Get headers of table in a dictionary
	Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	
	
	gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
	Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
	gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
	' Load the first/title row in a dictionary
	int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue
	int_MaxColumns = UBound(gvntAr_RuleTable,2)
	
	objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
	objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
	objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
	objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
	' objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= "0"
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= "0"
	objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula = now 
	
	gDt_Run_TS = now
	gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 
	
	Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE
	
	'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
	Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
	dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
	Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
	intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
	intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
	intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
	fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
	
	rowTL = 1 : colTL = 1
	
	' =========
	' =========  FrameworkPhase 0b   - Technical Setup for Running the SingleScenario Rows   =============
	' =========
	
	Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
	
	dim r
	Dim objDBConn_TestData_A, objDBRcdSet_TestData_A
	
	'					On error resume next
	'	add more of these as-required, for querying databases
	'	add more of these as-required, for querying databases
	Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
	r = 0
	set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")
	'	add more of these as-required, for querying databases
	
	
	Dim objDBConn_TestData_B, objDBRcdSet_TestData_B
	Set objDBConn_TestData_B    = CreateObject("ADODB.Connection") 
	r = 0
	set objDBRcdSet_TestData_B  = CreateObject("ADODB.Recordset")
	
	Dim	DB_CONNECT_STR_MTS
	DB_CONNECT_STR_MTS = _
	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
	Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
	Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
	Environment.Value("USERNAME") 	& 	";Password=" & _
	Environment.Value("PASSWORD") 	& ";"
	
	Dim	strEnvCoy
	strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")
	
	Dim	DB_CONNECT_STR_CIS
	DB_CONNECT_STR_CIS = _
	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
	Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
	Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
	Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
	Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"
	'
	'					Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL") & "SORD\"
	'					
	
	Dim objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE
	Set objDBConn_TestData_IEE    = CreateObject("ADODB.Connection")
	set objDBRcdSet_TestData_IEE  = CreateObject("ADODB.Recordset")	

	Dim	DB_CONNECT_STR_IEE
	DB_CONNECT_STR_IEE = _
			"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
		Environment.Value(strEnvCoy & 	"_IEE_HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
		Environment.Value(strEnvCoy & 	"_IEE_SERVICENAME") 	&	"))); User ID=" & _
		Environment.Value(strEnvCoy & 	"_IEE_USERNAME") 	& 	";Password=" & _
		Environment.Value(strEnvCoy & 	"_IEE_PASSWORD") 	& ";"


	Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
	Dim strQueryTemplate, dtD, Hyph, strNMI
	
	Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
	Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
	intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
	
	Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
	Dim intSQL_Pkg_ColNr, strSQL_RunPackage
	Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn
	
	
	intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
	intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
	intColNR_ScenarioID 					= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
	
	'	===>>>   Setting the   intColNr_InScope   colulmn - \EXECUTION flow
	intColNr_InScope = dictWSDataParameter_KeyName_ColNr(Parameter("InScope_Column_Name"))
	sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
	
	Dim strColName
	objWS_DataParameter.calculate	'	to ensure the filename is updated
	
	Dim  str_xlBitNess : str_xlBitNess = ""
	
	sb_File_WriteContents  _
	gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , true, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
	
	Dim intColNr_ScenarioStatus, intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
	
	intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
	intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")
	
	Dim blnColumnSet : blnColumnSet = cbool(true)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
	If IsEmpty(intColNr_InScope) then	
		blnColumnSet =false
	ElseIf intColNr_InScope = -1 then
		blnColumnSet =false
	End if
	If not blnColumnSet then 
		objXLapp.screenUpdating=True
		exittest
	End If
	
	objXLapp.Calculation = xlManual
	objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
	objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
	objWS_DataParameter.calculate
	objxlapp.screenupdating = true
	
	' objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
	
	' =========
	' =========  FrameworkPhase 0c   - Setup the MultiStage Array                                           =============
	' =========
	
	Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
	Dim  intMasterZoomLevel 
	
	Dim rowStart, rowEnd, colStart, colEnd
	Dim RangeAr 

End if

' objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = Parameter("InScope_Column_Name")

' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET
' FIX THIS FROM ORIGINAL SHEET


dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = cint(0)	: intFirst= cint(0)	: intSecond = cint(0)
Dim z : z = "z"
Dim intWS_RowNr : intWS_Scenario_RowNr = cint(1)	' qq


sb_File_WriteContents   	_
str_Fq_LogFileName, gfsoForAppending , true, _
fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf


'	determine the number of in-scope rows
Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun
tsStartOfRun = now()

sb_File_WriteContents   	_
str_Fq_LogFileName, gfsoForAppending , false, _
fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf

dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow

'	now setup to do process the DriverSheet

Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
Const cIncomplete = "Incomplete"
Dim intRowNr_Scenario : intRowNr_CurrentScenario = cInt(0) ' formerly cLng
Dim cInt_MinutesRequired_toComplete_eachScenario, cDbl_ExtraFactor_ToWait_for_RunToComplete, tsStopRunWhenRunningTooLong
Dim cInt_MinutesPerDay : cInt_MinutesPerDay = 24 * 60
Dim cInt_SecondsPerDay : cInt_SecondsPerDay = 60 * cInt_MinutesPerDay 
Dim dt_rgWS_RunWillCeaseAt_TimeStamp : dt_rgWS_RunWillCeaseAt_TimeStamp = now()-7
cInt_MinutesRequired_toComplete_eachScenario = cInt(5)
cDbl_ExtraFactor_ToWait_for_RunToComplete = cdbl(1.2)


Dim str_ScenarioRow_FlowName 
Dim cStr_FlowNumbers 
Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
Dim  intFlowNr_Current 						:	intFlowNr_Current 						= cInt(0)
Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= cInt(-1)

Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = false

Dim intColNr_Request_ID 
intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
intColNr_ScenarioStatus 			= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")
Dim strRole, strInitiator_Role, var_ScheduledDate

Dim strInScope, strRowCompletionStatus

objXLapp.Calculation = xlAutomatic
objXLapp.screenUpdating=true
objWB_Master.save

gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array

' Get headers of table in a dictionary
Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare

' Load the first/title row in a dictionary
int_MaxColumns = UBound(gvntAr_RuleTable,2)

'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue



' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch applications at startup - START xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
' Launch MTS
'fnMTS_WinClose
'OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")
'
' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
''	
' Reset error flags
fn_reset_error_flags
Dim int_Unique_SO_Number, temp_save_SchDate_Original_Value, temp_array_SpecialCOnditions
Dim int_ctr_no_of_Eligible_rows : int_ctr_no_of_Eligible_rows = 0
Dim int_ctr_total_Eligible_rows, int_ctr_no_of_passed_rows, temp_Scenario_ID
Dim str_Company_Name, ctr_Stage, tempInt_RoleIdentificationCounter, Str_Role
Dim temp_Resp_Retailer, strSQL_MTS

int_ctr_total_Eligible_rows = 0
int_ctr_no_of_passed_rows = 0

i = 1

' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PREP - Update company related information in driver sheet %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

If lcase(Environment.Value("COMPANY")) = "sapn" Then
Environment.Value("COMPANY_CODE")="ETSA"
else
Environment.Value("COMPANY_CODE")=Environment.Value("COMPANY")
End If


Dim temp_NEW_METER
Dim strCaseGroup, strCaseNr, strXML_TemplateName_Current, Date_TransactionDate
Dim str_DataParameter_PopulatedXML_FqFileName, str_MTS_Txn_Status
								
' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Do
	
	For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
	
		'	if the scenarioRow is InScope-and-Incomplete                                                                                                                 
		strInScope 							= 	ucase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
		str_ScenarioRow_FlowName 		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
		str_Next_Function 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value
		temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value
		temp_RowType 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowType") ).value 


		' MostRecent_Function_Status - this holds the name of most recent function or FAIL, incase the row has failed. The failure reason would be there in comments cell
		
			If   ucase(strInScope) = ucase(cY)  and str_ScenarioRow_FlowName <> "" and lcase(str_Next_Function) <> "fail" and lcase(str_Next_Function) <> "end" Then

				If i = 1 Then
					If ucase(Parameter.Item("RunMode")) = "N" Then ' Value can be N- New or R - restart
						If instr(lcase(str_ScenarioRow_FlowName),"sit_") > 0 Then
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "IEE_Query"
						Else
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start"						
						End If
						
						str_Next_Function = "start"
						gFWbln_ExitIteration = false
					End If
					fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows + 1
					int_ctr_total_Eligible_rows = int_ctr_total_Eligible_rows + 1
					objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
					' Update the company / state name in worksheet for an eligible row

				End If
			
				' Generate a filename for screenshot file
				gFW_strRunLog_ScreenCapture_fqFileName = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}.png", array(gFWstr_RunFolder, Parameter.Item("Environment"), Parameter.Item("Company"), intRowNr_CurrentScenario, temp_Scenario_ID))

				' ####################################################################################################################################################################################
				' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
				' ####################################################################################################################################################################################
				' fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", "", "", "", "", "", ""
				
			Select Case lcase(str_ScenarioRow_FlowName)
				
				' ################################################################# START of first flow ####################################################################################################
			Case "flow1", "sit_flow1":
				Do
					str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
					Select Case str_Next_Function
					Case "IEE_Query" ' Section to be executed in case of SIT only
						tsNow = now() 
						tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
						IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							Exit do
						End if
						
						
						' First execute the query against MTS to get the list of NMI reserved today
						strSQL_MTS = "KDR_NMI_LIST_TODAY" ' Hardcoding as this would NEVER change

						If trim(strSQL_MTS) <> ""  Then
						
							  PrintMessage "p", "MTS SQL template to get NMI List","Scenario row("& intRowNr_CurrentScenario &") - MTS SQL Template ("& strSQL_MTS &") "
						        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
						        
						        
						        If lcase(strQueryTemplate) <> "fail" Then
						        	
									strSQL_MTS = strQueryTemplate
									
									' Execute MTS SQL
									strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  "", "", "", "")
									
									If strScratch = "PASS" Then
										' Fetch the NMI list in a variable
										str_MTS_NMI_List = objDBRcdSet_TestData_B.Fields ("MTS_NMI_LIST")
										
										If str_MTS_NMI_List = "" or isnull(str_MTS_NMI_List) Then
											str_MTS_NMI_List = "'0'"
										End If
										' Now run the IEE Query to fetch the NMI and write in NMI column
										strSQL_IEE = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_DataMine_NMI_Query")).value 
		
										If trim(strSQL_IEE) <> ""  and trim(str_MTS_NMI_List) <> "" Then
											
											PrintMessage "p", "IEE SQL template","Scenario row("& intRowNr_CurrentScenario &") - IEE SQL Template ("& strSQL_IEE &") "
										    strQueryTemplate = fnRetrieve_SQL(strSQL_IEE)
										        
									        If lcase(strQueryTemplate) <> "fail" Then
												strSQL_IEE = strQueryTemplate
												' Perform replacements
												If objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("METER_TYPE")) = "E4" Then
													strSQL_IEE = replace ( strSQL_IEE , "<VALIDATION_SET_ID>", "Type5%", 1, -1, vbTextCompare)
												Else
													If objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("METER_TYPE")) = "E2" Then
														strSQL_IEE = replace ( strSQL_IEE , "<VALIDATION_SET_ID>", "Type4%", 1, -1, vbTextCompare)
													End If	
												End If
												
									            	strSQL_IEE = replace ( strSQL_IEE , "<NMI_LIST_FROM_KDR>", str_MTS_NMI_List, 1, -1, vbTextCompare)
									            	' Append the query in SQL Populated Coulmn
									            	fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "IEE_DataMine_NMI_Query", strSQL_IEE 
													' Execute IEE SQL
													strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_IEE, objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, strSQL_IEE, intRowNr_CurrentScenario, "2",  "", "", "", dictWSDataParameter_KeyName_ColNr("NMI"))
													' Reserve NMI in MTS
													str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
													If str_NMI <> "" Then
														' ####################################################################################################################################################################################
														' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
														' ####################################################################################################################################################################################
														fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", str_NMI, "", "", "", "", ""
														PrintMessage "p", "Reserve NMI - CANDIDATE","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& str_NMI &") in KDR as candidate"															
														fnKDR_Insert_Data_V1 "n", "n",str_NMI, temp_Scenario_ID, "candidate"
													Else
														fn_set_error_flags true, "NMI not found, exiting test"
														PrintMessage "f", "NMI not found", gFWstr_ExitIteration_Error_Reason
													End If 'end of If str_NMI <> "" Then
													
											 End if ' end of If lcase(strQueryTemplate) <> "fail" Then
									
										  Else' If IEE_DataMine_NMI_Query = "" this will run (To run this test make sure IEE_DataMine_NMI_Query is empty in data sheet)
											strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SQL_Template_Name")).value 
							
											If strSQL_CIS <> ""  Then				
												PrintMessage "p", "SQL template","Scenario row("& intRowNr_CurrentScenario &") - SQL Template for datamine ("& strSQL_CIS &") "							
												strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)							
												' perform all replacements
												strSQL_CIS = replace ( strSQL_CIS  , "<METER_TYPE>", objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("METER_TYPE") ).value, 1, -1, vbTextCompare)
												 If instr(1, strSQL_CIS, "<SP_STATUS>")>0 Then
													strSQL_CIS = replace ( strSQL_CIS  , "<SP_STATUS>",  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SP_STATUS")),  1, -1, vbTextCompare)
												End If
												PrintMessage "p", "SQL Execution","Scenario row("& intRowNr_CurrentScenario &") - Executing SQL ("& strSQL_CIS &") "
												' Execute CIS SQL
												strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI"))
												'strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_IEE, objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, strSQL_IEE, intRowNr_CurrentScenario, "2",  "", "", "", dictWSDataParameter_KeyName_ColNr("NMI"))
												
												If strScratch = "PASS" Then
													' Reserve NMI
													int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
													
													fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", int_NMI, "", "", "", "", ""
													PrintMessage "p", "Reserve NMI","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& int_NMI &") in KDR "															
													fnKDR_Insert_Data_CIS_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
												End If
											
											End If ' end of check related to CIS query strSQL_CIS <> ""	
											

									End If 'end of If trim(strSQL_IEE) <> ""  and trim(str_MTS_NMI_List) <> "" Then
								End if 'end of If strScratch = "PASS" Then
							End If ' end of  If lcase(strQueryTemplate) <> "fail" Then
						End If ' end of If strSQL_MTS <> ""  Then						

						' At the end, check if any error was there and write in corresponding column
						If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Datamine IEE_Query Failed - (" & gFWstr_ExitIteration_Error_Reason & ")"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
							PrintMessage "f", "Issue with Datamine", gFWstr_ExitIteration_Error_Reason
							int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
							Exit do
						Else
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_Create_SO"
							PrintMessage "p", "Data mining complete", "Data mining complete" 
							fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_CIS_SO_RAISED_wait_time, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
						End If
					
					Case "start"
						tsNow = now() 
						tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
						IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							Exit do
						End if
						
						IF tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
							' capture CIS datamine query
							strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SQL_Template_Name")).value 
							
							If strSQL_CIS <> ""  Then				
								PrintMessage "p", "SQL template","Scenario row("& intRowNr_CurrentScenario &") - SQL Template for datamine ("& strSQL_CIS &") "							
								strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)							
								' perform all replacements
								strSQL_CIS = replace ( strSQL_CIS  , "<METER_TYPE>", objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("METER_TYPE") ).value, 1, -1, vbTextCompare)
								 If instr(1, strSQL_CIS, "<SP_STATUS>")>0 Then
									strSQL_CIS = replace ( strSQL_CIS  , "<SP_STATUS>",  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SP_STATUS")),  1, -1, vbTextCompare)
								End If
								PrintMessage "p", "SQL Execution","Scenario row("& intRowNr_CurrentScenario &") - Executing SQL ("& strSQL_CIS &") "
								' Execute CIS SQL
								strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI"))
								
								If strScratch = "PASS" Then
									' Reserve NMI
									int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
									
									fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", int_NMI, "", "", "", "", ""
									PrintMessage "p", "Reserve NMI","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& int_NMI &") in KDR "															
									fnKDR_Insert_Data_CIS_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
								End If
							
							End If ' end of check related to CIS query strSQL_CIS <> ""	
							
							strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_DETAILS_QUERY")).value 
							If strSQL_CIS <> ""  Then				
								PrintMessage "p", "SQL template","Scenario row("& intRowNr_CurrentScenario &") - SQL Template for datamine ("& strSQL_CIS &") "							
								strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)							
								' perform all replacements
								strSQL_CIS = replace ( strSQL_CIS  , "<METER_TYPE>", objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("METER_TYPE") ).value, 1, -1, vbTextCompare)
								PrintMessage "p", "SQL Execution","Scenario row("& intRowNr_CurrentScenario &") - Executing SQL ("& strSQL_CIS &") "
								' Execute CIS SQL
								strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("cs2ListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("cs2ListSqlColumn_Values"),dictWSDataParameter_KeyName_ColNr("New_Meter"))
								
								If strScratch = "PASS" and objDBRcdSet_TestData_A.RecordCount > 0 Then
									objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NEW_METER") ).value = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "cs2ListSqlColumn_Names", "cs2ListSqlColumn_Values", intRowNr_CurrentScenario, "NEW_METER")
									PrintMessage "p", "In Stock Equipment retrived from Database","Meter Number = " & objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NEW_METER") ).value
								Else
									PrintMessage "f", "No Meter found in Stock"
									gFWbln_ExitIteration = true
									gFWstr_ExitIteration_Error_Reason = "No Meter found in Stock"									
								End If
							
							End If ' end of check related to CIS query strSQL_CIS <> ""	
							
							If int_NMI <> ""  and gFWbln_ExitIteration = false Then
								
								If lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Manual_SO_Required"))) = "y" Then
								
									strSQL_CIS = ""
									strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_CreateSO_API_Query")).value 
									strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
									
									strSQL_CIS = replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
									strSQL_CIS = replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
									strSQL_CIS = replace ( strSQL_CIS  , "<param_NMI>", int_NMI,  1, -1, vbTextCompare)
									strSQL_CIS = replace ( strSQL_CIS  , "<param_DataPrep_CIS_SOType_Code>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_SOType_Code" )) ,  1, -1, vbTextCompare)
									If objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SO_ScheduledDate" )).value <> "" Then
										strSQL_CIS = replace ( strSQL_CIS  , "<param_Date-oneweekbefore-eg-2017-02-28>", fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SO_ScheduledDate" )),  "YYYY-MM-DD" ),  1, -1, vbTextCompare)
									End If
									If instr(1, strSQL_CIS, "<param_DataPrep_CIS_SOReason>")>0 Then
										strSQL_CIS = replace ( strSQL_CIS  , "<param_DataPrep_CIS_SOReason>",  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_SOReason")),  1, -1, vbTextCompare)
									End If
									If instr(1, strSQL_CIS, "<NO_ACCOUNT>")>0 Then
										strSQL_CIS = replace ( strSQL_CIS  , "<NO_ACCOUNT>",  fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "NO_ACCOUNT"),  1, -1, vbTextCompare)
									End If
									If instr(1, strSQL_CIS, "<param_DataPrep_CIS_TaskType>")>0 Then
										strSQL_CIS = replace ( strSQL_CIS  , "<param_DataPrep_CIS_TaskType>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_SO_TaskType" )),  1, -1, vbTextCompare)
									End If
									If instr(1, strSQL_CIS, "<param_ResponsibleRetailer>") >0  Then
										temp_Resp_Retailer = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "RESPONSIBLERETAILER")
										strSQL_CIS = replace ( strSQL_CIS  , "<param_ResponsibleRetailer>",  temp_Resp_Retailer,  1, -1, vbTextCompare)
									End If
									
									Select case lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("METER_TYPE" )).value)
									
									case "e"
									strSQL_CIS = replace ( strSQL_CIS  , "<param_Meter_Type_MTS>", "Basic",  1, -1, vbTextCompare)
									case "e1"
									strSQL_CIS = replace ( strSQL_CIS  , "<param_Meter_Type_MTS>", "MRIM",  1, -1, vbTextCompare)
									case "e2"
									strSQL_CIS = replace ( strSQL_CIS  , "<param_Meter_Type_MTS>", "RRIM",  1, -1, vbTextCompare)
									case "e4"
									strSQL_CIS = replace ( strSQL_CIS  , "<param_Meter_Type_MTS>", "AMI",  1, -1, vbTextCompare)
									
									End Select 																										
									
									Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
									PrintMessage "p", "API invoked","API invoked to Create Service Order - " & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_SOType_Code" ))
									
									Wait(5)
	
									strSQL_CIS = ""
									strSQL_CIS = "Find_CIS_SO_Num_DataPrep"												
									strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
									
									' perform all replacements
									strSQL_CIS = replace ( strSQL_CIS  , "<NMI>", int_NMI,  1, -1, vbTextCompare)
									
									strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", dictWSDataParameter_KeyName_ColNr("SQL_Populated"), "", "", "")
									PrintMessage "p", "Search Turn On / Turn Off CIS SO Number","Searching Service Order created via API" 
									
									If strScratch = "PASS" Then
										' execute the API to create data
										strSQL_CIS = ""
										strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_Complete_SO_API_Query")).value 
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
										' perform all replacements
										strSQL_CIS = replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
										strSQL_CIS = replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
										strSQL_CIS = replace ( strSQL_CIS  , "<PARAM_SONUM>", objDBRcdSet_TestData_A.Fields ("PARAM_SONUM"), 1, -1, vbTextCompare)
										If instr(1, strSQL_CIS, "<param_ DataPrep_CIS_Action>")>0 Then 
											strSQL_CIS = replace ( strSQL_CIS  , "<param_ DataPrep_CIS_Action>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_SO_Action" )), 1, -1, vbTextCompare)
										End If
										
										If instr(1, strSQL_CIS, "<NEW_METER>")>0 Then
											temp_NEW_METER = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "cs2ListSqlColumn_Names", "cs2ListSqlColumn_Values", intRowNr_CurrentScenario, "NEW_METER")
											If temp_NEW_METER <> "" Then
												strSQL_CIS = replace ( strSQL_CIS  , "<NEW_METER>", temp_NEW_METER  ,  1, -1, vbTextCompare)
											Else 
												PrintMessage "f", "No Meter found in Stock"
												gFWbln_ExitIteration = true
												gFWstr_ExitIteration_Error_Reason = "No Meter found in Stock"										
											
											End If
										End If
										
										If instr(1, strSQL_CIS, "<PARAM_READINGMETHOD>")>0 Then
										
											Select case lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("METER_TYPE" )).value)
										
												case "e"
												strSQL_CIS = replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Basic Meter",  1, -1, vbTextCompare)
												case "e1"
												strSQL_CIS = replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Manually Read Interval Meter",  1, -1, vbTextCompare)
												case "e2"
												strSQL_CIS = replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Comms4",  1, -1, vbTextCompare)
												case "e4"
												strSQL_CIS = replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Manually Read Interval Meter",  1, -1, vbTextCompare)
											
											End Select
											
										End If
										
										' Execute Query
										Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
										PrintMessage "p", "API invoked","API invoked to Complete Service Order"
										strSQL_CIS = ""
									End If
								
								End If 'end of if Manual_SO_Required ="y"
								
								Wait(5)
								
								If lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ModifySP_Status_Required" )))  = "y" Then
	
									If trim(strScratch) <> "" Then
									strSQL_CIS = ""
									strSQL_CIS = "CIS_API_ModifyServiceProvisionRequest"
									strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
									
									' perform all replacements
									strSQL_CIS = replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
									strSQL_CIS = replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
									strSQL_CIS = replace ( strSQL_CIS  , "<Param_NMI>", int_NMI, 1, -1, vbTextCompare)
									strSQL_CIS = replace ( strSQL_CIS  , "<param_SP_Status>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SP_STATUS_DESCRIPTION" )), 1, -1, vbTextCompare)
									
									wait(1)
									' Execute Query
									Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
									PrintMessage "p", "API invoked","API invoked to Modify SP Status"
									strSQL_CIS = ""
									End If
									
								End If 'end of ModifySP_Status_Required = "y"
						
							Else
								gFWbln_ExitIteration = true
								gFWstr_ExitIteration_Error_Reason = "No NMI found"
								PrintMessage "f", "Datamine", gFWstr_ExitIteration_Error_Reason								
							
							End If	'end of f int_NMI <> "" Then
							
							' At the end, check if any error was there and write in corresponding column
							If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Dtamine Failed - (" & gFWstr_ExitIteration_Error_Reason & ")"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
								PrintMessage "f", "Issue with Datamine", gFWstr_ExitIteration_Error_Reason
								int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
								Exit do
							Else
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "NMIREF_CHECK"
								PrintMessage "p", "Data mining complete", "Data mining complete" 
								fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_NMIREF_wait_time, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
							End If
							strSQL_CIS = ""
							
						End If 'end of IF tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
					

				Case "CIS_API_Create_SO"
				
						int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
						
						If int_NMI <> ""  and gFWbln_ExitIteration = false Then
								
								If lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Manual_SO_Required"))) = "y" Then
								
									strSQL_CIS = ""
									strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_CreateSO_API_Query")).value 
									strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
									
									strSQL_CIS = replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
									strSQL_CIS = replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
									strSQL_CIS = replace ( strSQL_CIS  , "<param_NMI>", int_NMI,  1, -1, vbTextCompare)
									strSQL_CIS = replace ( strSQL_CIS  , "<param_DataPrep_CIS_SOType_Code>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_SOType_Code" )) ,  1, -1, vbTextCompare)
									If objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SO_ScheduledDate" )).value <> "" Then
										strSQL_CIS = replace ( strSQL_CIS  , "<param_Date-oneweekbefore-eg-2017-02-28>", fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SO_ScheduledDate" )),  "YYYY-MM-DD" ),  1, -1, vbTextCompare)
									End If
									If instr(1, strSQL_CIS, "<param_DataPrep_CIS_SOReason>")>0 Then
										strSQL_CIS = replace ( strSQL_CIS  , "<param_DataPrep_CIS_SOReason>",  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_SOReason")),  1, -1, vbTextCompare)
									End If
									If instr(1, strSQL_CIS, "<NO_ACCOUNT>")>0 Then
										strSQL_CIS = replace ( strSQL_CIS  , "<NO_ACCOUNT>",  fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "NO_ACCOUNT"),  1, -1, vbTextCompare)
									End If
									If instr(1, strSQL_CIS, "<param_DataPrep_CIS_TaskType>")>0 Then
										strSQL_CIS = replace ( strSQL_CIS  , "<param_DataPrep_CIS_TaskType>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_SO_TaskType" )),  1, -1, vbTextCompare)
									End If
									If instr(1, strSQL_CIS, "<param_ResponsibleRetailer>") >0  Then
										temp_Resp_Retailer = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "RESPONSIBLERETAILER")
										strSQL_CIS = replace ( strSQL_CIS  , "<param_ResponsibleRetailer>",  temp_Resp_Retailer,  1, -1, vbTextCompare)
									End If
									
									Select case lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("METER_TYPE" )).value)
									
									case "e"
									strSQL_CIS = replace ( strSQL_CIS  , "<param_Meter_Type_MTS>", "Basic",  1, -1, vbTextCompare)
									case "e1"
									strSQL_CIS = replace ( strSQL_CIS  , "<param_Meter_Type_MTS>", "MRIM",  1, -1, vbTextCompare)
									case "e2"
									strSQL_CIS = replace ( strSQL_CIS  , "<param_Meter_Type_MTS>", "RRIM",  1, -1, vbTextCompare)
									case "e4"
									strSQL_CIS = replace ( strSQL_CIS  , "<param_Meter_Type_MTS>", "AMI",  1, -1, vbTextCompare)
									
									End Select 	
									
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_Create_SO_Populated_Query") ).value = strSQL_CIS
									
									Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
									PrintMessage "p", "API invoked","API invoked to Create Service Order - " & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_SOType_Code" ))
									
									Wait(5)
	
									End If
									' At the end, check if any error was there and write in corresponding column
									If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Create SO API Failed - (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										PrintMessage "f", "Issue with Datamine", gFWstr_ExitIteration_Error_Reason
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit do
									Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Create SO API is complete"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_Complete_SO"
										PrintMessage "p", "Create SO API complete", "Create SO API complete" 
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_CIS_SO_RAISED_wait_time, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									End If
									strSQL_CIS = ""
								End If 'end of if Manual_SO_Required ="y"
								




                           Case "CIS_API_Complete_SO"

                            If lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Manual_SO_Required"))) = "y" Then
	                                
	                                strSQL_CIS = ""
									strSQL_CIS = "Find_CIS_SO_Num_DataPrep"												
									strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
									
									' perform all replacements
									strSQL_CIS = replace ( strSQL_CIS  , "<NMI>", int_NMI,  1, -1, vbTextCompare)
									
									strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", dictWSDataParameter_KeyName_ColNr("SQL_Populated"), "", "", "")
									PrintMessage "p", "Search Turn On / Turn Off CIS SO Number","Searching Service Order created via API" 
									
									If strScratch = "PASS" Then
										' execute the API to create data
										strSQL_CIS = ""
										strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_Complete_SO_API_Query")).value 
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										'Recording CIS_SO Number for future use (MTS_Query Verification)
										strSO_CIS = objDBRcdSet_TestData_A.Fields ("PARAM_SONUM")
										' perform all replacements
										strSQL_CIS = replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
										strSQL_CIS = replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
										strSQL_CIS = replace ( strSQL_CIS  , "<PARAM_SONUM>", objDBRcdSet_TestData_A.Fields ("PARAM_SONUM"), 1, -1, vbTextCompare)
										If instr(1, strSQL_CIS, "<param_ DataPrep_CIS_Action>")>0 Then 
											strSQL_CIS = replace ( strSQL_CIS  , "<param_ DataPrep_CIS_Action>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_SO_Action" )), 1, -1, vbTextCompare)
										End If
										
										If instr(1, strSQL_CIS, "<NEW_METER>")>0 Then
											temp_NEW_METER = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "cs2ListSqlColumn_Names", "cs2ListSqlColumn_Values", intRowNr_CurrentScenario, "NEW_METER")
											If temp_NEW_METER <> "" Then
												strSQL_CIS = replace ( strSQL_CIS  , "<NEW_METER>", temp_NEW_METER  ,  1, -1, vbTextCompare)
											Else 
												PrintMessage "f", "No Meter found in Stock"
												gFWbln_ExitIteration = true
												gFWstr_ExitIteration_Error_Reason = "No Meter found in Stock"										
											
											End If
										End If
										
										If instr(1, strSQL_CIS, "<PARAM_READINGMETHOD>")>0 Then
										
											Select case lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("METER_TYPE" )).value)
										
												case "e"
												strSQL_CIS = replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Basic Meter",  1, -1, vbTextCompare)
												case "e1"
												strSQL_CIS = replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Manually Read Interval Meter",  1, -1, vbTextCompare)
												case "e2"
												strSQL_CIS = replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Comms4",  1, -1, vbTextCompare)
												case "e4"
												strSQL_CIS = replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Manually Read Interval Meter",  1, -1, vbTextCompare)
											
											End Select
											
										End If
										
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_Complete_SO_Populated_Query") ).value = strSQL_CIS
										
										' Execute Query
										Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
										PrintMessage "p", "API invoked","API invoked to Complete Service Order"
										strSQL_CIS = ""
									
                             End If
                             
                     		' At the end, check if any error was there and write in corresponding column
							If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Complete SO API Failed - (" & gFWstr_ExitIteration_Error_Reason & ")"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
								PrintMessage "f", "Issue with Complete SO API", gFWstr_ExitIteration_Error_Reason
								int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
								Exit do
							Else
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Complete SO API is complete"
								'Skipping temp as NMI REF Takes 15 mins
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "NMIREF_CHECK"
								'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "IEE_Query_Validation"
								
								PrintMessage "p", "Complete SO API complete", "Complete SO API complete" 
								fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_NMIREF_wait_time, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								'fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_CIS_SO_RAISED_wait_time, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
							End If
							strSQL_CIS = ""
                             
					End If 'End of If lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Manual_SO_Required"))) = "y"

					Case "NMIREF_CHECK"
					
						tsNow = now() 
						tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
						IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							Exit do
						End if
						
						IF tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
						
							If  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Check_SP_INSTANCE" )))  = "y" Then
							
								strSQL_CIS = ""
								strSQL_CIS = "NMIREF_Check"													
								strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
								
								' perform all replacements
								strSQL_CIS = replace ( strSQL_CIS  , "<NMI>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )),  1, -1, vbTextCompare)
								strSQL_CIS = replace ( strSQL_CIS  , "<NMIREF_TYPE>", "SP_INSTANCE",  1, -1, vbTextCompare)
								
								Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", dictWSDataParameter_KeyName_ColNr("SQL_Populated"), "", "", ""
								 
								 If objDBRcdSet_TestData_A.State > 0 Then	
									If objDBRcdSet_TestData_A.RecordCount > 0  Then
										objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CORRID_SP_INSTANCE")).value = objDBRcdSet_TestData_A.Fields ("CORRID")
										
										If instr(lcase(str_ScenarioRow_FlowName),"sit_") > 0 Then
											
											If lcase(objDBRcdSet_TestData_A.Fields ("STATUS")) = "success" Then
												gFWbln_ExitIteration = false
				
											Else
												gFWbln_ExitIteration = true
												gFWstr_ExitIteration_Error_Reason = "Actual STATUS in SP_INSTANCE NMI REF:  " & objDBRcdSet_TestData_A.Fields ("STATUS").Value & ". Expected:  " & "success"
												PrintMessage "f", "Issue with SP_INSTANCE NMI REF Check", gFWstr_ExitIteration_Error_Reason
											End If
										
										Else							
																				
											If lcase(objDBRcdSet_TestData_A.Fields ("NMIREF_SP_STATUS")) = lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_SP_STATUS")).value) Then
												gFWbln_ExitIteration = false
												if  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_DEEN_METHOD")).value) <> "" Then 
													if lcase(objDBRcdSet_TestData_A.Fields ("NMIREF_DEEN_METHOD")) = lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_DEEN_METHOD")).value) Then
														gFWbln_ExitIteration = false																											
													Else
														gFWbln_ExitIteration = true
														gFWstr_ExitIteration_Error_Reason = "Actual DEEN_METHOD in SP_INSTANCE NMI REF:  " & objDBRcdSet_TestData_A.Fields ("NMIREF_DEEN_METHOD").Value & ". Expected:  " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_DEEN_METHOD")).value
														PrintMessage "f", "Issue with SP_INSTANCE NMI REF Check", gFWstr_ExitIteration_Error_Reason
													End if
												End If
											Else
												gFWbln_ExitIteration = true
												gFWstr_ExitIteration_Error_Reason = "Actual SP_STATUS in SP_INSTANCE NMI REF:  " & objDBRcdSet_TestData_A.Fields ("NMIREF_SP_STATUS").Value & ". Expected:  " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_SP_STATUS")).value
												PrintMessage "f", "Issue with SP_INSTANCE NMI REF Check", gFWstr_ExitIteration_Error_Reason
											End If
										End If ' End of instr(lcase(str_ScenarioRow_FlowName),"sit_") > 0 
										
									Else
										gFWbln_ExitIteration = true
										gFWstr_ExitIteration_Error_Reason = "SP_INSTANCE NMI REF is not genereated. Might be an issue with CreateSORequest or CompleteSORequest "
										PrintMessage "f", "Issue with SP_INSTANCE NMI REF Check", gFWstr_ExitIteration_Error_Reason
									End if 'End of objDBRcdSet_TestData_A.RecordCount > 0
								Else
									gFWbln_ExitIteration = true
									gFWstr_ExitIteration_Error_Reason = "SP_INSTANCE NMI REF is not genereated. Might be an issue with CreateSORequest or CompleteSORequest."
									PrintMessage "f", "Issue with SP_INSTANCE NMI REF Check", gFWstr_ExitIteration_Error_Reason		
								End If 'End of objDBRcdSet_TestData_A.State > 0
								
							End If 'end of Check_SP_INSTANCE = "y"
							
							If  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Check_MTR_CHANGE" )))  = "y" or ucase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Check_MTR_CHANGE" ))) =  "NO RESPONSE FROM MTS" Then
							
								strSQL_CIS = ""
								strSQL_CIS = "NMIREF_Check"													
								strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
								
								' perform all replacements
								strSQL_CIS = replace ( strSQL_CIS  , "<NMI>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )),  1, -1, vbTextCompare)
								strSQL_CIS = replace ( strSQL_CIS  , "<NMIREF_TYPE>", "MTR_CHANGE",  1, -1, vbTextCompare)
								
								Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", dictWSDataParameter_KeyName_ColNr("SQL_Populated"), "", "", ""
								
								if ucase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Check_MTR_CHANGE" ))) =  "NO RESPONSE FROM MTS" Then
									If instr(1, lcase(objDBRcdSet_TestData_A.Fields ("STATUS")), lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Check_MTR_CHANGE")).value) , vbTextCompare) > 0 Then
											gFWbln_ExitIteration = false
									Else
											gFWbln_ExitIteration = true
											gFWstr_ExitIteration_Error_Reason = "Actual Status of MTR_CHANGE NMI REF:  " & objDBRcdSet_TestData_A.Fields ("STATUS").Value & ". Expected:  " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Check_MTR_CHANGE")).value
											PrintMessage "f", "Issue with MTR_CHANGE NMI REF Check", gFWstr_ExitIteration_Error_Reason
									End If	
								Else							
								 
								 If objDBRcdSet_TestData_A.State > 0 Then	
									If objDBRcdSet_TestData_A.RecordCount > 0  Then
										objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CORRID_MTR_CHANGE")).value = objDBRcdSet_TestData_A.Fields ("CORRID")
										
										If instr(lcase(str_ScenarioRow_FlowName),"sit_") > 0 Then
											
											If lcase(objDBRcdSet_TestData_A.Fields ("STATUS")) = "success" Then
												gFWbln_ExitIteration = false
				
											Else
												gFWbln_ExitIteration = true
												gFWstr_ExitIteration_Error_Reason = "Actual STATUS in SP_INSTANCE NMI REF:  " & objDBRcdSet_TestData_A.Fields ("STATUS").Value & ". Expected:  " & "success"
												PrintMessage "f", "Issue with SP_INSTANCE NMI REF Check", gFWstr_ExitIteration_Error_Reason
											End If
										Else
										
													If lcase(objDBRcdSet_TestData_A.Fields ("NMIREF_SP_STATUS")) = lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_SP_STATUS")).value) Then
														gFWbln_ExitIteration = false
														if  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_DEEN_METHOD")).value) <> "" Then 
															if lcase(objDBRcdSet_TestData_A.Fields ("NMIREF_DEEN_METHOD")) = lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_DEEN_METHOD")).value) Then
																gFWbln_ExitIteration = false																											
															Else
																gFWbln_ExitIteration = true
																gFWstr_ExitIteration_Error_Reason = "Actual DEEN_METHOD in SP_INSTANCE NMI REF:  " & objDBRcdSet_TestData_A.Fields ("NMIREF_DEEN_METHOD").Value & ". Expected:  " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_DEEN_METHOD")).value
																PrintMessage "f", "Issue with MTR_CHANGE NMI REF Check", gFWstr_ExitIteration_Error_Reason
															End if
														End If
													Else
														gFWbln_ExitIteration = true
														gFWstr_ExitIteration_Error_Reason = "Actual SP_STATUS in MTR_CHANGE NMI REF:  " & objDBRcdSet_TestData_A.Fields ("NMIREF_SP_STATUS").Value & ". Expected:  " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_SP_STATUS")).value
														PrintMessage "f", "Issue with MTR_CHANGE NMI REF Check", gFWstr_ExitIteration_Error_Reason
													End If
										 End If ' End of instr(lcase(str_ScenarioRow_FlowName),"sit_") > 0
										Else
										gFWbln_ExitIteration = true
										gFWstr_ExitIteration_Error_Reason = "MTR_CHANGE NMI REF is not genereated. Might be an issue with CreateSORequest or CompleteSORequest "
										PrintMessage "f", "Issue with MTR_CHANGE NMI REF Check", gFWstr_ExitIteration_Error_Reason
									End if ' objDBRcdSet_TestData_A.RecordCount > 
								Else
									gFWbln_ExitIteration = true
									gFWstr_ExitIteration_Error_Reason = "MTR_CHANGE NMI REF is not genereated. Might be an issue with CreateSORequest API or CompleteSORequest"
									PrintMessage "f", "Issue with MTR_CHANGE NMI REF Check", gFWstr_ExitIteration_Error_Reason		
								End If ' End of objDBRcdSet_TestData_A.State > 0 
							 End If 'end of Check_MTR_CHANGE = "NO RESPONSE FROM MTS"
							End If 'end of Check_MTR_CHANGE = "y" or " No Response"
							
							If  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Check_CONFIG" )))  = "y" Then
							
								strSQL_CIS = ""
								strSQL_CIS = "NMIREF_Check"													
								strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
								
								' perform all replacements
								strSQL_CIS = replace ( strSQL_CIS  , "<NMI>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )),  1, -1, vbTextCompare)
								strSQL_CIS = replace ( strSQL_CIS  , "<NMIREF_TYPE>", "CONFIGURATION",  1, -1, vbTextCompare)
								
								Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", dictWSDataParameter_KeyName_ColNr("SQL_Populated"), "", "", ""
								 
								 If objDBRcdSet_TestData_A.State > 0 Then	
									If objDBRcdSet_TestData_A.RecordCount > 0  Then
										objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CORRID_CONFIGURATION")).value = objDBRcdSet_TestData_A.Fields ("CORRID")
										
										If instr(lcase(str_ScenarioRow_FlowName),"sit_") > 0 Then
											
											If lcase(objDBRcdSet_TestData_A.Fields ("STATUS")) = "success" Then
												gFWbln_ExitIteration = false
				
											Else
												gFWbln_ExitIteration = true
												gFWstr_ExitIteration_Error_Reason = "Actual STATUS in SP_INSTANCE NMI REF:  " & objDBRcdSet_TestData_A.Fields ("STATUS").Value & ". Expected:  " & "success"
												PrintMessage "f", "Issue with SP_INSTANCE NMI REF Check", gFWstr_ExitIteration_Error_Reason
											End If
										Else
										
										
												If lcase(objDBRcdSet_TestData_A.Fields ("NMIREF_SP_STATUS")) = lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_SP_STATUS")).value) Then
													gFWbln_ExitIteration = false
													if  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_DEEN_METHOD")).value) <> "" Then 
														if lcase(objDBRcdSet_TestData_A.Fields ("NMIREF_DEEN_METHOD")) = lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_DEEN_METHOD")).value) Then
															gFWbln_ExitIteration = false																											
														Else
															gFWbln_ExitIteration = true
															gFWstr_ExitIteration_Error_Reason = "Actual DEEN_METHOD in CONFIGURATION NMI REF:  " & objDBRcdSet_TestData_A.Fields ("NMIREF_DEEN_METHOD").Value & ". Expected:  " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_DEEN_METHOD")).value
															PrintMessage "f", "Issue with CONFIGURATION NMI REF Check", gFWstr_ExitIteration_Error_Reason
														End if
													End If
												Else
													gFWbln_ExitIteration = true
													gFWstr_ExitIteration_Error_Reason = "Actual SP_STATUS in CONFIGURATION NMI REF:  " & objDBRcdSet_TestData_A.Fields ("NMIREF_SP_STATUS").Value & ". Expected:  " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NMIREF_SP_STATUS")).value
													PrintMessage "f", "Issue with CONFIGURATION NMI REF Check", gFWstr_ExitIteration_Error_Reason
												End If
												
										End If ' End of instr(lcase(str_ScenarioRow_FlowName),"sit_") > 0
									Else
										gFWbln_ExitIteration = true
										gFWstr_ExitIteration_Error_Reason = "CONFIGURATION NMI REF is not genereated. Might be an issue with CreateSORequest or CompleteSORequest"
										PrintMessage "f", "Issue with CONFIGURATION NMI REF Check", gFWstr_ExitIteration_Error_Reason
									End if
								Else
									gFWbln_ExitIteration = true
									gFWstr_ExitIteration_Error_Reason = "CONFIGURATION NMI REF is not genereated. Might be an issue with CreateSORequest or CompleteSORequest"
									PrintMessage "f", "Issue with CONFIGURATION NMI REF Check", gFWstr_ExitIteration_Error_Reason		
								End If
								
							End If 'end of Check_SP_INSTANCE = "y"
						
							If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "NMIREF_CHECK Failed - (" & gFWstr_ExitIteration_Error_Reason & ")"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
								PrintMessage "f", "Issue with NMIREF_CHECK", gFWstr_ExitIteration_Error_Reason
								int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
								Exit do
							Else
							
								
								If  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Check_CONFIG" )))  = "y" and objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CORRID_CONFIGURATION")).value = "" Then
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CONFIGURATION NMIREF Failed. " & gFWstr_ExitIteration_Error_Reason
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									PrintMessage "f", "Issue with NMIREF_CHECK", gFWstr_ExitIteration_Error_Reason
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit do
								ElseIf  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Check_SP_INSTANCE" )))  = "y" and objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CORRID_SP_INSTANCE")).value = "" Then
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SP_INSTANCE NMIREF Failed. " & gFWstr_ExitIteration_Error_Reason
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									PrintMessage "f", "Issue with NMIREF_CHECK", gFWstr_ExitIteration_Error_Reason
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit do
								ElseIf  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Check_MTR_CHANGE" )))  = "y" and objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CORRID_MTR_CHANGE")).value = "" Then
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTR_CHANGE NMIREF Failed. " & gFWstr_ExitIteration_Error_Reason
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									PrintMessage "f", "Issue with NMIREF_CHECK", gFWstr_ExitIteration_Error_Reason
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit do
								Else
										'working here
										If instr(lcase(str_ScenarioRow_FlowName),"sit_") > 0 Then
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "NMIREF_CHECK complete."
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "IEE_Query_Validation"
										PrintMessage "p", "NMIREF check complete", "NMIREF check complete" 
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_SQT_IN_wait_time, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										
										Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "NMIREF_CHECK complete."
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "SQT_INTERFACE_IN_CHECK"
										PrintMessage "p", "NMIREF check complete", "NMIREF check complete" 
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_SQT_IN_wait_time, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									
										End If								
									End If
							End If
						End If 'end of IF tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
			
					
					case "IEE_Query_Validation" 
                                                                                                                                                                
                                ' fn_compare_Current_and_expected_execution_time
                                
                                tsNow = now() 
                                tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
                                IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
                                                Exit do
                                End if
                                
                				' capture IEE query to fetch Meter Data
                                strSQL_IEE = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_SP_Status_Query")).value 

                                If trim(strSQL_IEE) <> ""  Then
                                                str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))                                                                                                                                                                                                                       
                                                
                                                If str_NMI <> "" Then
                                                  PrintMessage "p", "IEE SQL template","Scenario row("& intRowNr_CurrentScenario &") - IEE SQL Template for verification ("& strSQL_IEE &") "
                                        		  strQueryTemplate = fnRetrieve_SQL(strSQL_IEE)
                                        
                                        			If lcase(strQueryTemplate) <> "fail" Then
                                        
                                                        strSQL_IEE = strQueryTemplate
                                                        ' Perform replacements
                                                                                                                                      
				                                        strSQL_IEE = replace ( strSQL_IEE , "<NMI>", str_NMI, 1, -1, vbTextCompare)
				                                                                         								
				                                        PrintMessage "p", "IEE verification Query Execution","Scenario row("& intRowNr_CurrentScenario &") - Executing IEE Verification SQL ("& strSQL_IEE &") "
                                                        ' Execute IEE SQL
                                                        strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_IEE, objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, strSQL_IEE, intRowNr_CurrentScenario, "2",  "", "", "", "")
                                                        str_Expected_NEWMETERSTATUS= objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("IEE_New_Meter_Status" ))                                                                         
                                                        str_Expected_IEE_SP_Status = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("IEE_SP_Status" ))                        
                                                                                
                                                                                If objDBConn_TestData_IEE.State = 1 and objDBRcdSet_TestData_IEE.RecordCount =1  Then
                                                                                                objDBRcdSet_TestData_IEE.MoveFirst
                                                                                                If lcase(trim(objDBRcdSet_TestData_IEE.Fields("NEWMETERSTATUS"))) = lcase(trim(str_Expected_NEWMETERSTATUS)) Then
                                                                                                                PrintMessage "p", "IEE verification Query Execution","Expected NEWMETERSTATUS (" & str_Expected_NEWMETERSTATUS &") MATCH expected NEWMETERSTATUS (" & trim(objDBRcdSet_TestData_IEE.Fields("NEWMETERSTATUS")) &")  in DB/Table"
                                                                                                Else
                                                                                                                PrintMessage "f", "IEE verification Query Execution","Expected NEWMETERSTATUS (" & str_Expected_NEWMETERSTATUS &") DOES NOT MATCH expected NEWMETERSTATUS (" & trim(objDBRcdSet_TestData_IEE.Fields("NEWMETERSTATUS")) &") in DB/Table"
                                                                                                				fn_set_error_flags true, "Expected NEWMETERSTATUS (" & str_Expected_NEWMETERSTATUS &") DOES NOT MATCH expected NEWMETERSTATUS (" & trim(objDBRcdSet_TestData_IEE.Fields("NEWMETERSTATUS")) &") in DB/Table"
                                                                                                End If
                                                                                                
                                                                                                'objDBRcdSet_TestData_IEE.MoveNext
                                                                                               ' If trim(objDBRcdSet_TestData_IEE.Fields("METERSTARTDATE")) = trim(temp_XML_Date) Then
																								 If lcase(trim(objDBRcdSet_TestData_IEE.Fields("SPSTATUS"))) = lcase(trim(str_Expected_IEE_SP_Status)) Then
                                                                                                                                                                                                      
                                                                                               		PrintMessage "p", "IEE verification Query Execution","Expected SPSTATUS (" & str_Expected_IEE_SP_Status &") MATCH SPSTATUS (" & trim(objDBRcdSet_TestData_IEE.Fields("SPSTATUS")) &")  in DB/Table"
                                                                                                 Else
                                                                                                    PrintMessage "f", "IEE verification Query Execution","Expected SPSTATUS (" & str_Expected_IEE_SP_Status &") DOES NOT MATCH eSPSTATUS (" & trim(objDBRcdSet_TestData_IEE.Fields("SPSTATUS")) &")  in DB/Table"
                                                                                                	fn_set_error_flags true, "Expected SPSTATUS (" & str_Expected_IEE_SP_Status &") DOES NOT MATCH eSPSTATUS (" & trim(objDBRcdSet_TestData_IEE.Fields("SPSTATUS")) &")  in DB/Table"
                                                                                                    
                                                                                                 End If
                                                                                                
                                                                                Else
                                                                                                PrintMessage "f", "IEE verification Query Execution","Failed during execution of IEE Query"
                                                                                                fn_set_error_flags true, "Failed during execution of IEE Query"
                                                                                End If
                                                                                
                                                                                ' Perform verifications of actual value returned by SQL
                                                                                
                                                                                ' Strategy for comparison
                                                                                '''''''''''''''''''''''''''''''''''''''''''''''''''''
                                                                                ' As the values are already there in some cell, as they went in XML
                                                                                ' Pick those values and compare then with the one returned from executing the SQL
                                                                                
                                                                                ' you need to check...for the NMI, Meter, Channel...the last two status fields is updated as what has come thru in the xml
                                                                                
                                                                End If ' end of  If lcase(strQueryTemplate) <> "fail" Then
                                                Else
                                                                PrintMessage "f", "IEE verification Query Execution", "Skipping execution of IEE verification query as there is no NMI found for record ("& intRowNr_CurrentScenario &"). Marking the row as fail and moving to next one"
                                                                fn_set_error_flags true, "IEE verification query is provided however there is no NMI available for replacement. Skipping this row"
                                                End If ' end of If str_NMI <> "" Then
                                End If ' end of If strSQL_IEE <> ""  Then

                                ' At the end, check if any error was there and write in corresponding column
                                If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
                                                objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row("&intRowNr_CurrentScenario&") failed at IEE_Query_Validation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
                                                PrintMessage "f","MILESTONE - IEE_Query_Validation function", "Row("&intRowNr_CurrentScenario&") failed at IEE_Query_Validation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
                                                int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
                                                Exit do
                                Else
                                
									'If lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow7" Then
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "IEE_Query_Validation complete"
										'Removing this temp as the SQT not returning any results
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_SO_Query_Validation"
										'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
										PrintMessage "p", "MILESTONE - IEE_Query_Validation complete", "IEE_Query_Validation complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									
'									Else
'                                        objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
'                                        objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
'                                        objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "IEE_Query_Validation complete"
'                                        PrintMessage "p", "MILESTONE - IEE_Query_Validation complete", "IEE_Query_Validation complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
'                                        objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
'                                        ' Function to write timestamp for next process
'                                        fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
'                                        int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
'                                        int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
'                                        
'                                        ' We need to change the status of NMI as available in KDR
'                                        PrintMessage "p", "Reserve NMI","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& str_NMI &") in KDR as available"                                                                                                                                                                                                                                    
'                                        fnKDR_Insert_Data_V1 "n", "n",str_NMI, temp_Scenario_ID, "available"
'                                        Exit Do
                                		
                                	
							End If

					case "MTS_SO_Query_Validation" 
                                                                                                                                                                
                                ' fn_compare_Current_and_expected_execution_time
                                
                                tsNow = now() 
                                tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
                                IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
                                                Exit do
                                End if
                                
                				' capture IEE query to fetch Meter Data
                                strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SO_Status_Query")).value 

                                If trim(strSQL_MTS) <> ""  Then
                                                'str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))                                                                                                                                                                                                                       
                                                
                                                If strSO_CIS <> "" Then
                                                  PrintMessage "p", "MTS SQL template","Scenario row("& intRowNr_CurrentScenario &") - MTS SQL Template for verification ("& strSQL_MTS &") "
                                        		  strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
                                        
                                        			If lcase(strQueryTemplate) <> "fail" Then
                                        
                                                        strSQL_MTS = strQueryTemplate
                                                        ' Perform replacements
                                                                                                                                      
				                                        strSQL_MTS = replace ( strSQL_MTS , "<CIS_SO_NUM>", strSO_CIS, 1, -1, vbTextCompare)
				                                                                         								
				                                        PrintMessage "p", "MTS verification Query Execution","Scenario row("& intRowNr_CurrentScenario &") - Executing MTS Verification SQL ("& strSQL_MTS &") "
                                                        ' Execute MTS SQL
														strScratch = Execute_SQL_Populate_DataSheet  (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2", "", "","", "")
                                                        
                                                        'strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, strSQL_IEE, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListIEEVerificationSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListIEEVerificationSqlColumn_Values"), "")
                                                        str_Expected_MTS_SO_Verification_String= objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("MTS_SO_Status" ))                                                                         
                                                        'str_Expected_MTS_SP_Verification_String = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("MTS_SP_Status" ))                        
                                                                                
                                                                                If objDBConn_TestData_B.State = 1 and objDBRcdSet_TestData_B.RecordCount =1  Then
                                                                                                objDBRcdSet_TestData_B.MoveFirst
                                                                                                If lcase(trim(objDBRcdSet_TestData_B.Fields("MTS_SO_Status"))) = lcase(trim(str_Expected_MTS_SO_Verification_String)) Then
                                                                                                                PrintMessage "p", "MTS verification Query Execution","Expected MTS_SO_Status (" & str_Expected_MTS_SO_Verification_String &") MATCH expected MTS_SO_Status (" & trim(objDBRcdSet_TestData_B.Fields("MTS_SO_Status")) &")  in DB/Table"
                                                                                                Else
                                                                                                                PrintMessage "f", "MTS verification Query Execution","Expected MTS_SO_Status (" & str_Expected_MTS_SO_Verification_String &") DOES NOT MATCH expected MTS_SO_Status (" & trim(objDBRcdSet_TestData_B.Fields("MTS_SO_Status")) &") in DB/Table"
                                                                                                End If
                                                                                                
                                                                                                'objDBRcdSet_TestData_MTS.MoveNext
                                                                                               
''																								 If (trim(objDBRcdSet_TestData_MTS.Fields("MTS_SP_Status")) = trim(str_Expected_MTS_SP_Verification_String)) Then
''                                                                                                                                                                                                      
''                                                                                               		PrintMessage "p", "MTS verification Query Execution","Expected SPSTATUS ((" & str_Expected_MTS_SP_Verification_String &") MATCH SPSTATUS (" & trim(objDBRcdSet_TestData_MTS.Fields("MTS_SP_Status")) &")  in DB/Table"
''                                                                                                 Else
''                                                                                                    PrintMessage "f", "MTS verification Query Execution","Expected SPSTATUS((" & str_Expected_MTS_SP_Verification_String &") DOES NOT MATCH SPSTATUS (" & trim(objDBRcdSet_TestData_MTS.Fields("MTS_SP_Status")) &")  in DB/Table"
''                                                                                                 End If
                                                                                                
                                                                                Else
                                                                                                PrintMessage "f", "MTS SO verification Query Execution","Failed during execution of MTS SO Query"
                                                                                                fn_set_error_flags true, "Failed during execution of MTS SO Query"
                                                                                End If
                                                                                
                                                                               
                                                                End If ' end of  If lcase(strQueryTemplate) <> "fail" Then
                                                Else
                                                                PrintMessage "f", "MTS_SO_Query_Validation Query Execution", "Skipping execution of MTS verification query as there is no SO found for record ("& intRowNr_CurrentScenario &"). Marking the row as fail and moving to next one"
                                                                fn_set_error_flags true, "MTS_SO_Query_Validation query is provided however there is no SO available for replacement. Skipping this row"
                                                End If ' end of If str_NMI <> "" Then
                                End If ' end of If strSQL_IEE <> ""  Then

                                ' At the end, check if any error was there and write in corresponding column
                                If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
                                                objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row("&intRowNr_CurrentScenario&") failed at MTS_SO_Query_Validation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
                                                PrintMessage "f","MILESTONE - MTS_SO_Query_Validation function", "Row("&intRowNr_CurrentScenario&") failed at MTS_SO_Query_Validation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
                                                int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
                                                Exit do
                                Else
                                
'									'If lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow7" Then
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_SO_Query_Validation complete"
										'Removing this temp as the SQT not returning any results
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_SP_Query_Validation"
										'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
										PrintMessage "p", "MILESTONE - MTS_SO_Query_Validation complete", "MTS_SO_Query_Validation complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									
''									Else
'                                        objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
'                                        objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
'                                        objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_SO_Query_Validation complete"
'                                        PrintMessage "p", "MILESTONE - MTS_Query_Validation complete", "MTS_Query_Validation complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
'                                        objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
'                                        ' Function to write timestamp for next process
'                                        fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
'                                        int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
'                                        int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
'                                        
'                                        ' We need to change the status of NMI as available in KDR
'                                        PrintMessage "p", "Reserve NMI","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& str_NMI &") in KDR as available"                                                                                                                                                                                                                                    
'                                        fnKDR_Insert_Data_V1 "n", "n",str_NMI, temp_Scenario_ID, "available"
'                                        Exit Do
'                                		
                                	
							End If

					
					case "MTS_SP_Query_Validation" 
                                                                                                                                                                
                                ' fn_compare_Current_and_expected_execution_time
                                
                                tsNow = now() 
                                tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
                                IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
                                                Exit do
                                End if
                                
                				' capture IEE query to fetch Meter Data
                                strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SP_Status_Query")).value 

                                If trim(strSQL_MTS) <> ""  Then
                                                str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))                                                                                                                                                                                                                       
                                                
                                                If strSO_CIS <> "" Then
                                                  PrintMessage "p", "MTS SQL template","Scenario row("& intRowNr_CurrentScenario &") - MTS SQL Template for verification ("& strSQL_MTS &") "
                                        		  strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
                                        
                                        			If lcase(strQueryTemplate) <> "fail" Then
                                        
                                                        strSQL_MTS = strQueryTemplate
                                                        ' Perform replacements
                                                                                                                                      
				                                        strSQL_MTS = replace ( strSQL_MTS , "<NMI>", str_NMI, 1, -1, vbTextCompare)
				                                                                         								
				                                        PrintMessage "p", "MTS verification Query Execution","Scenario row("& intRowNr_CurrentScenario &") - Executing MTS Verification SQL ("& strSQL_MTS &") "
                                                        ' Execute MTS SQL
														strScratch = Execute_SQL_Populate_DataSheet  (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2", "", "","", "")
                                                        
                                                        'strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, strSQL_IEE, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListIEEVerificationSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListIEEVerificationSqlColumn_Values"), "")
                                                        'str_Expected_MTS_SO_Verification_String= objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("MTS_SO_Status" ))                                                                         
                                                        str_Expected_MTS_SP_Verification_String = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("MTS_SP_Status" ))                        
                                                                                
                                                                                If objDBConn_TestData_B.State = 1 and objDBRcdSet_TestData_B.RecordCount =1  Then
                                                                                                objDBRcdSet_TestData_B.MoveFirst
                                                                                                                                                      
																								 If lcase(trim(objDBRcdSet_TestData_B.Fields("MTS_SP_Status"))) = lcase(trim(str_Expected_MTS_SP_Verification_String)) Then
                                                                                                                                                                                                      
                                                                                               		PrintMessage "p", "MTS verification Query Execution","Expected SPSTATUS ((" & str_Expected_MTS_SP_Verification_String &") MATCH SPSTATUS (" & trim(objDBRcdSet_TestData_B.Fields("MTS_SP_Status")) &")  in DB/Table"
                                                                                                 Else
                                                                                                    PrintMessage "f", "MTS verification Query Execution","Expected SPSTATUS((" & str_Expected_MTS_SP_Verification_String &") DOES NOT MATCH SPSTATUS (" & trim(objDBRcdSet_TestData_B.Fields("MTS_SP_Status")) &")  in DB/Table"
                                                                                                 End If
                                                                                                 
                                                                                                
                                                                                                
                                                                                Else
                                                                                                PrintMessage "f", "MTS SO verification Query Execution","Failed during execution of MTS SO Query"
                                                                                                fn_set_error_flags true, "Failed during execution of MTS SO Query"
                                                                                End If
                                                                                
                                                                               
                                                                End If ' end of  If lcase(strQueryTemplate) <> "fail" Then
                                                Else
                                                                PrintMessage "f", "MTS_SO_Query_Validation Query Execution", "Skipping execution of MTS verification query as there is no SO found for record ("& intRowNr_CurrentScenario &"). Marking the row as fail and moving to next one"
                                                                fn_set_error_flags true, "MTS_SO_Query_Validation query is provided however there is no SO available for replacement. Skipping this row"
                                                End If ' end of If str_NMI <> "" Then
                                End If ' end of If strSQL_IEE <> ""  Then

                                ' At the end, check if any error was there and write in corresponding column
                                If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
                                                objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row("&intRowNr_CurrentScenario&") failed at MTS_SO_Query_Validation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
                                                PrintMessage "f","MILESTONE - MTS_SO_Query_Validation function", "Row("&intRowNr_CurrentScenario&") failed at MTS_SO_Query_Validation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
                                                int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
                                                Exit do
                                Else
                                
'									'If lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow7" Then
'										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
'										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Query_Validation complete"
'										'Removing this temp as the SQT not returning any results
'										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_SP_Query_Validation"
'										'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
'										PrintMessage "p", "MILESTONE - MTS_SO_Query_Validation complete", "MTS_SO_Query_Validation complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
'										' Function to write timestamp for next process
'										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
'									
''									Else
                                        objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
                                        objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
                                        objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_SP_Query_Validation complete"
                                        PrintMessage "p", "MILESTONE - MTS_Query_Validation complete", "MTS_Query_Validation complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
                                        objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
                                        ' Function to write timestamp for next process
                                        fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
                                        int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
                                        int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
                                        
                                        ' We need to change the status of NMI as available in KDR
                                        PrintMessage "p", "Reserve NMI","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& str_NMI &") in KDR as available"                                                                                                                                                                                                                                    
                                        fnKDR_Insert_Data_V1 "n", "n",str_NMI, temp_Scenario_ID, "available"
                                        Exit Do
                                		
                                	
							End If


					Case "SQT_INTERFACE_IN_CHECK"
					
						tsNow = now() 
						tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
						IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							Exit do
						End if
						
						IF tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
						
							If  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Check_SQT_INTERFACE_IN" )))  = "y" Then
							
								If lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQT_IN_NMISTATUSCODE" )))  <> ""  Then
								
									strSQL_MTS = ""
									strSQL_MTS = "SP_INTANCE_SQT_INTERFACE_IN"													
									strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
									
									' perform all replacements
									strSQL_MTS = replace ( strSQL_MTS  , "<SP_INSTANCE_CORRID>", objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CORRID_SP_INSTANCE")).value,  1, -1, vbTextCompare)
									
									strScratch = Execute_SQL_Populate_DataSheet  (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2", dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), "")
									 
									 If strScratch = "PASS" Then	
									 	If objDBRcdSet_TestData_B.RecordCount> 0  Then	
											If lcase(objDBRcdSet_TestData_B.Fields ("NMISTATUSCODE")) = lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_NMISTATUSCODE")).value) Then
												gFWbln_ExitIteration = false
												if  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_NMISTATEDATE")).value) <> "" Then 
													if fnTimeStamp(objDBRcdSet_TestData_B.Fields ("NMISTATEDATE") , "YYYY-MM-DD" ) = fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_NMISTATEDATE")),  "YYYY-MM-DD" ) Then
														gFWbln_ExitIteration = false				
														if  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_DEENERGISATIONMETHOD")).value) <> "" Then 
															if lcase(trim(objDBRcdSet_TestData_B.Fields ("DEENERGISATIONMETHOD"))) = lcase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_DEENERGISATIONMETHOD")))) Then
																gFWbln_ExitIteration = false	
															Else
																gFWbln_ExitIteration = true
																gFWstr_ExitIteration_Error_Reason = "Actual NMI De-en Method in SQT_INTERFACE_IN:  " & objDBRcdSet_TestData_B.Fields ("DEENERGISATIONMETHOD").Value & ". Expected:  " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_DEENERGISATIONMETHOD")).value
																PrintMessage "f", "Issue with SQT_INTERFACE_IN Check", gFWstr_ExitIteration_Error_Reason
															End if
														End if	
													Else
														gFWbln_ExitIteration = true
														gFWstr_ExitIteration_Error_Reason = "Actual NMI STATE DATE in SQT_INTERFACE_IN:  " & objDBRcdSet_TestData_B.Fields ("NMISTATEDATE").Value & ". Expected:  " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_NMISTATEDATE")).value
														PrintMessage "f", "Issue with SQT_INTERFACE_IN Check", gFWstr_ExitIteration_Error_Reason
													End if
												End If
											Else
												gFWbln_ExitIteration = true
												gFWstr_ExitIteration_Error_Reason = "Actual NMI STATUS in SQT_INTERFACE_IN:  " & objDBRcdSet_TestData_B.Fields ("NMISTATUSCODE").Value & ". Expected:  " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_NMISTATUSCODE")).value
												PrintMessage "f", "Issue with SQT_INTERFACE_IN Check", gFWstr_ExitIteration_Error_Reason
											End If
										Else 
											gFWbln_ExitIteration = true
										 	gFWstr_ExitIteration_Error_Reason = "No Rows found in SQT_INTERFACE_IN. SP_INSTANCE NMI Ref might have failed or not requested"
											PrintMessage "f", "SQT_INTERFACE_IN Failed", gFWstr_ExitIteration_Error_Reason
										End If
									Else
										gFWbln_ExitIteration = true
										gFWstr_ExitIteration_Error_Reason = "No Rows found in SQT_INTERFACE_IN. SP_INSTANCE NMI Ref might have failed or not requested"
										PrintMessage "f", "SQT_INTERFACE_IN Failed", gFWstr_ExitIteration_Error_Reason		
									End If
								End If 'end of SQT_IN_NMISTATUSCODE <> ""		

								If lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQT_IN_METERSTATUS" )))  <> ""  Then
								
									strSQL_MTS = ""
									strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQT_IN_CHECK_QUERY")).value 													
									strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
									
									' perform all replacements
									If instr(1, strSQL_MTS, "<MTR_CHANGE_CORRID>")>0  Then
										strSQL_MTS = replace ( strSQL_MTS  , "<MTR_CHANGE_CORRID>", objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CORRID_MTR_CHANGE")).value,  1, -1, vbTextCompare)
									End if
									If  instr(1, strSQL_MTS, "<CORRID_CONFIGURATION>")>0 Then  
										strSQL_MTS = replace ( strSQL_MTS  , "<CORRID_CONFIGURATION>", objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CORRID_CONFIGURATION")).value,  1, -1, vbTextCompare)
									End If
									If  instr(1, strSQL_MTS, "<NEW_METER>")>0 Then  
										temp_NEW_METER = ""
										temp_NEW_METER =  fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "cs2ListSqlColumn_Names", "cs2ListSqlColumn_Values", intRowNr_CurrentScenario, "NEW_METER")
										strSQL_MTS = replace ( strSQL_MTS  , "<NEW_METER>", temp_NEW_METER,  1, -1, vbTextCompare)
									End If
									
									strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2", dictWSDataParameter_KeyName_ColNr("SQL_Populated"), "", "", "")
									 
									 If strScratch = "PASS" Then
									 	If objDBRcdSet_TestData_B.RecordCount> 0  Then						 	
											If lcase(objDBRcdSet_TestData_B.Fields ("METERSTATUS")) = lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_METERSTATUS")).value) Then
												gFWbln_ExitIteration = false
												if  lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_METEREFFECTIVESTARTDATE")).value) <> "" Then 
													if fnTimeStamp(objDBRcdSet_TestData_B.Fields ("METEREFFECTIVESTARTDATE") , "YYYY-MM-DD" ) = fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_METEREFFECTIVESTARTDATE")),  "YYYY-MM-DD" ) Then
														gFWbln_ExitIteration = false																											
													Else
														gFWbln_ExitIteration = true
														gFWstr_ExitIteration_Error_Reason = "Actual Meter STARTE DATE in SQT_INTERFACE_IN:  " & fnTimeStamp(objDBRcdSet_TestData_B.Fields ("METEREFFECTIVESTARTDATE") , "YYYY-MM-DD" ) & ". Expected:  " & fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_METEREFFECTIVESTARTDATE")),  "YYYY-MM-DD" )
														PrintMessage "f", "Issue with SQT_INTERFACE_IN Check", gFWstr_ExitIteration_Error_Reason
													End if
												End If
											Else
												gFWbln_ExitIteration = true
												gFWstr_ExitIteration_Error_Reason = "Actual Meter STATUS in SQT_INTERFACE_IN:  " & objDBRcdSet_TestData_B.Fields ("METERSTATUS").Value & ". Expected:  " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SQT_IN_METERSTATUS")).value
												PrintMessage "f", "Issue with SQT_INTERFACE_IN Check", gFWstr_ExitIteration_Error_Reason
											End If
											
										Else
											gFWbln_ExitIteration = true
										 	gFWstr_ExitIteration_Error_Reason = "No Records found in SQT_INTERFACE_IN. MTR_CHANGE or CONFIGURATION NMI Ref might have failed or not requested from CIS"
											PrintMessage "f", "SQT_INTERFACE_IN Failed", gFWstr_ExitIteration_Error_Reason
										End If ' end of If objDBRcdSet_TestData_B.RecordCount> 0  Then		
										
									Else
										gFWbln_ExitIteration = true
										gFWstr_ExitIteration_Error_Reason = "No Records found in SQT_INTERFACE_IN. MTR_CHANGE or CONFIGURATION NMI Ref might have failed or not requested from CIS"
										PrintMessage "f", "SQT_INTERFACE_IN Failed", gFWstr_ExitIteration_Error_Reason		
									End If
									
									
								End If 'end of SQT_IN_NMISTATUSCODE <> ""	
							End If 'end of Check_SQT_INTERFACE_IN = "y"
						
					
						If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SQT_INTERFACE_IN_CHECK Failed - (" & gFWstr_ExitIteration_Error_Reason & ")"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
								PrintMessage "f", "Issue with SQT_INTERFACE_IN_CHECK", gFWstr_ExitIteration_Error_Reason
								int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
								Exit do
							Else
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SQT_INTERFACE_IN_CHECK complete" 
								PrintMessage "p", "SQT_INTERFACE_IN check complete", "SQT_INTERFACE_IN check complete" 
								fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
								int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
								Exit Do
							End If
						End If 'end of IF tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
						
					Case "FAIL"
						Exit Do
					Case else ' incase there is no function (which would NEVER happen) name
						Exit Do
						
					End Select
			
				Loop While  (lcase(gFWbln_ExitIteration) = "n" or gFWbln_ExitIteration = false)
		
			' ################################################################# END of first flow ####################################################################################################
			' ################################################################# END of first flow ####################################################################################################

		
			Case else ' Incase there is no elaboration for a flow, we need to report in datasheet and move on to next row
			    gFWbln_ExitIteration = "Y"
			    gFWstr_ExitIteration_Error_Reason = "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
				PrintMessage "f", "FFLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
				objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
				objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
				int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
		      End Select ' end of 	Select Case str_ScenarioRow_FlowName
				
		End If ' end of If  (	( strInScope = cY)  and str_ScenarioRow_FlowName <> "" and (trim(str_Next_Function) = "" or lcase(str_Next_Function) <> "fail" or lcase(str_Next_Function) <> "end") ) Then

		' ####################################################################################################################################################################################
		' #####################################################                 LOGGING STEP 3 - Clear data before moving to next row #########################################################################
		' ####################################################################################################################################################################################
		'fnTestExeLog_ClearRuntimeValues
		

		' Reset error flags
		fn_reset_error_flags
		' clear scratch dictionary
		clear_scratch_Dictionary gdict_scratch

	Next ' end of For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	

	' objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows

	' Reset error flags
	fn_reset_error_flags

	i = 2
	objWB_Master.save ' Save the workbook 
Loop While int_ctr_no_of_Eligible_rows > 0

PrintMessage "i", "Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & " ) - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula 		= now

objWB_Master.save ' Save the workbook 

fnMTS_WinClose

' Once the test is complete, move the results to network drive
copyfolder  gFWstr_RunFolder , Cstr_USB_Final_Result_Location

ExitAction