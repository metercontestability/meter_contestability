﻿
' Open MTS aplication
Dim gFWbln_ExitIteration

'gFWbln_ExitIteration = "n"
'str_Request_Status = "AQ"

SystemUtil.Run "C:\_data\MTS_GUI\mts.exe","","","open"	

fn_Create_NMI_API "y"


'
'
''Navigate to "Transactions;Service Orders;Service Order Request Search"
'fn_MTS_MenuNavigation "transactions;service orders;service order request search"
'
'
'fn_MTS_Serach_Service_Order_Request "001911747", "", "VEPL" ,"6102268911" ,"06/09/2016" ,"19/09/2016" , "", "", "Adds And Alts", "Install Meter", str_Request_Status
'
'fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord "first","","","","","","","","",str_Request_Status,"y"
'
'If str_Request_Status = "AQ" Then
'	
'	fn_MTS_Click_button_any_screen "Search_Service_Order_Request_Screen", "btn_Details"
'	
'	fn_MTS_Service_Order_Tracking_VerifyAQ "SO_MTRADDALTS", "A Meter Add/Alt SO has been received and will need to be raised manually", "OPEN","y"
'	
'End If
'
'Dim str_Request_Status1
'str_Request_Status1 = "REJECTED"
'
'fn_MTS_Click_button_any_screen "Search_Service_Order_Request_Screen", "btn_Close"
'
'fn_MTS_MenuNavigation "transactions;service orders;service order request search"
'
'fn_MTS_Serach_Service_Order_Request "1593279790", "", "" ,"" ,"01/09/2016" ,"07/09/2016" , "", "", "", "", str_Request_Status1
'
'fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord "first","","","","","","","","",str_Request_Status1,"y"
'
'If str_Request_Status = "REJECTED" Then
'	
'	fn_MTS_Click_button_any_screen "Search_Service_Order_Request_Screen", "btn_Details"
'	
'	fn_MTS_Service_Order_Tracking_VerifyRejection "New Request with previously used RetServiceOrder.", "[Not Provided]", "y"
'	
'End If
'
'
'fn_MTS_Click_button_any_screen "Search_Service_Order_Request_Screen", "btn_Close"

'-------------------------------------------------------------------------------------------------


Function fn_MTS_Serach_Service_Order_Request (pistr_Ret_SO_Num, pistr_CIS_SO_Num, pistr_Retailer, pistr_NMI, pivar_Received_From, pivar_Received_To, _
												pivar_Scheduled_From, pivar_Scheduled_To, pistr_Service_Order_Type, pistr_Service_Order_SubType, pistr_Service_Order_Status)

    '################################################################################
    '
    '  Function Name:- fn_MTS_Serach_Service_Order_Request
    '  Description:-  This function would be Search for a B2B Service Order Request in MTS
    '  Parameters:-
    '  Return Value:- Nothing
    '  Creator:- Preeti Choudhary
    '################################################################################

' Check if there was any error and whether the function should continue
    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
        Reporter.ReportEvent micDone, "fn_MTS_Serach_Service_Order_Request function", "Exiting function as there was some error in previous step"
        Exit function
    End If

    PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").Activate
    
    If trim(pistr_Ret_SO_Num) <> "" Then
        PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Search").SetCellData "#1","so_number", pistr_Ret_SO_Num
    End If

    If trim(pistr_CIS_SO_Num) <> "" Then
        PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Search").SetCellData "#1","cis_so_number",pistr_CIS_SO_Num
    End If

    If trim(pistr_Retailer) <> "" Then
		PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Search").SetCellData "#1","#3",pistr_Retailer @@ hightlight id_;_526142_;_script infofile_;_ZIP::ssf274.xml_;_
    End If
    
     If trim(pistr_NMI) <> "" Then
        PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Search").SetCellData "#1","nmi",pistr_NMI 
    End If

    If trim(pivar_Received_From) <> "" Then
        PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Search").SetCellData "#1","date_from",pivar_Received_From
    End If

    If trim(pivar_Received_To) <> "" Then
		PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Search").SetCellData "#1","date_to",pivar_Received_To @@ hightlight id_;_526142_;_script infofile_;_ZIP::ssf274.xml_;_
    End If
    
    If trim(pivar_Scheduled_From) <> "" Then
        PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Search").SelectCell "#1",pivar_Scheduled_From
    End If

    If trim(pivar_Scheduled_To) <> "" Then
		PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Search").SetCellData "#1","sched_dt_to",pivar_Scheduled_To @@ hightlight id_;_526142_;_script infofile_;_ZIP::ssf274.xml_;_
    End If
    
     If trim(pistr_Service_Order_Type) <> "" Then
		PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Search").SetCellData "#1","so_type",pistr_Service_Order_Type @@ hightlight id_;_526142_;_script infofile_;_ZIP::ssf274.xml_;_
    End If
    
    If trim(pistr_Service_Order_SubType) <> "" Then
        PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Search").SetCellData "#1","so_subtype",pistr_Service_Order_SubType
    End If

    If trim(pistr_Service_Order_Status) <> "" Then
		PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Search").SetCellData "#1","so_status",pistr_Service_Order_Status @@ hightlight id_;_526142_;_script infofile_;_ZIP::ssf274.xml_;_
    End If

    ' Click on display
    PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbButton("btn_Display").Click
    
 End Function 'fn_MTS_Serach_Service_Order_Request
 	
 	
 
 Function fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord (pivar_record_no, pistr_so_number, pistr_network_so_number, pistr_so_type, pistr_so_subtype, pistr_action_type, pistr_Retailer, _
									 pistr_NMI, pistr_sched_dt, pistr_business_status, WhetherCaptureScreenshot_YN)


    '################################################################################
    '
    '  Function Name:- fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord
    '  Description:-  Function used to search and verify that Request Status of a B2B Service Order in MTS
    '  Parameters:-    1)
    '  Return Value:-
    '  Creator:- Preeti Choudhary
    '################################################################################

    Dim str_scratch, var_record_no
    str_scratch = ""

    ' Check if there was any error and whether the function should continue
    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
        Reporter.ReportEvent micDone, "fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord function", "Exiting function as there was some error in previous step"
        Exit function
    End If

    PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").Activate

    if  Not PbWindow("MTS").Dialog("DialogMTS").Static("NoRecordsWereFoundForTheSpecifiedSearchCriteria").Exist(1)  then

        Select case lcase(trim(pivar_record_no))
        Case "first"
            var_record_no = 1
        Case "last"
        	PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Service_Order_Request").RowCount
        End Select

        If var_record_no > 0  Then
           PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Service_Order_Request").SelectCell "#" & var_record_no,"business_status"
        Else
            ' code to traverse through the table and select the correct value
        End If

        If pistr_business_status <> "" Then

           str_scratch = PbWindow("MTS").PbWindow("Search_Service_Order_Request_Screen").PbDataWindow("panel_Service_Order_Request").GetCellData("#" & var_record_no, "business_status")

        End If

            If lcase(trim(pistr_business_status)) = lcase(trim(str_scratch))  Then
                Reporter.ReportEvent micPass,"fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord function", "Notification Status is same. Expected value (" & pistr_business_status &") matched actual value ("& str_scratch &")"
            Else
                gFWbln_ExitIteration = "Y"
                gFWstr_ExitIteration_Error_Reason = "fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord function - Notification Status is different. actual is " & str_scratch
                Reporter.ReportEvent micFail,"fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord function", "Notification Status is different. Expected value was (" & pistr_business_status &") whereas actual is ("& str_scratch &")"
                fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord = "FAIL"
            End If

    Else
        PbWindow("MTS").Dialog("DialogMTS").Activate
        PbWindow("MTS").Dialog("DialogMTS").WinButton("btn_OK").Click
        gFWbln_ExitIteration = "Y"
        gFWstr_ExitIteration_Error_Reason = "fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord function - No record found on "
        Reporter.ReportEvent micDone, "fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord function", "NO RECORD FOUND...exiting"
        fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord = "FAIL"
    End if

    ' At the end of the function, check if there was any error during any operation and if error needs to be generated

    ' Write information to Run Log (database)

End Function ' end of function fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord


Function fn_MTS_Service_Order_Tracking_VerifyAQ (pistr_AQ_Reason_Code, pistr_AQ_Desc, pistr_AQ_Status, WhetherCaptureScreenshot_YN)

    '################################################################################
    '
    '  Function Name:- fn_MTS_Service_Order_Tracking_VerifyAQ
    '  Description:-  Function used to verify the AQ details on the Service Order Tracking screen
    '  Parameters:-    1)
    '  Return Value:-
    '  Creator:- Preeti Choudhary
    '################################################################################

    Dim str_AQ_Reason, str_AQ_Desc, str_AQ_Status
    ' Check if there was any error and whether the function should continue
    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
        Reporter.ReportEvent micDone, "fn_MTS_Service_Order_Tracking_VerifyAQ function", "Exiting function as there was some error in previous step"
        Exit function
    End If

    PbWindow("MTS").PbWindow("Sevice_Order_Tracking").Activate

        If pistr_AQ_Reason_Code <> "" Then

            str_AQ_Reason = PbWindow("MTS").PbWindow("Sevice_Order_Tracking").PbDataWindow("panel_AQ_Details").GetCellData("#1","aq_reason")

        End If

        If pistr_AQ_Desc <> "" Then

           str_AQ_Desc = PbWindow("MTS").PbWindow("Sevice_Order_Tracking").PbDataWindow("panel_AQ_Details").GetCellData( "#1","reason_desc")
            
        End If
        
         If pistr_AQ_Status <> "" Then

            str_AQ_Status = PbWindow("MTS").PbWindow("Sevice_Order_Tracking").PbDataWindow("panel_AQ_Details").GetCellData( "#1","aq_status")

        End If

        If lcase(trim(str_AQ_Reason)) = lcase(trim(pistr_AQ_Reason_Code))  and  lcase(trim(str_AQ_Desc)) = lcase(trim(pistr_AQ_Desc)) and lcase(trim(str_AQ_Status)) = lcase(trim(pistr_AQ_Status))  Then
            Reporter.ReportEvent micPass,"fn_MTS_Service_Order_Tracking_VerifyAQ function", "AQ is same. Expected value (" & pistr_AQ_Reason_Code & " - " & pistr_AQ_Desc & " - " & pistr_AQ_Status & ") matched actual value ("& str_AQ_Reason & " - " & str_AQ_Desc & " - " & str_AQ_Status & ")"
        Else
            gFWbln_ExitIteration = "Y"
            gFWstr_ExitIteration_Error_Reason = "fn_MTS_Service_Order_Tracking_VerifyAQ function - AQ Reason is different. actual is " & str_AQ_Reason
            Reporter.ReportEvent micFail,"fn_MTS_Service_Order_Tracking_VerifyAQ function", "AQ Reason is different. Expected value was (" & pistr_AQ_Reason_Code & " - " & pistr_AQ_Desc & " - " & pistr_AQ_Status & ") whereas actual is ("& str_AQ_Reason & " - " & str_AQ_Desc & " - " & str_AQ_Status & ")"
            fn_MTS_Service_Order_Tracking_VerifyAQ = "FAIL"
        End If

    PbWindow("MTS").PbWindow("Sevice_Order_Tracking").Activate
    PbWindow("MTS").PbWindow("Sevice_Order_Tracking").PbButton("btn_Close").Click

    ' At the end of the function, check if there was any error during any operation and if error needs to be generated

    ' Write information to Run Log (database)


End Function ' end of function fn_MTS_Service_Order_Tracking_VerifyAQ

Function fn_MTS_Service_Order_Tracking_VerifyRejection (pistr_Reject_Reason, pistr_Explanation, WhetherCaptureScreenshot_YN)

    '################################################################################
    '
    '  Function Name:- fn_MTS_Service_Order_Tracking_VerifyRejection
    '  Description:-  Function used to verify the Rejection details on the Service Order Tracking screen
    '  Parameters:-    1)
    '  Return Value:-
    '  Creator:- Preeti Choudhary
    '################################################################################

    Dim str_Reject_Reason, str_Explanation
    ' Check if there was any error and whether the function should continue
    If lcase(trim(gFWbln_ExitIteration)) = "y" Then
        Reporter.ReportEvent micDone, "fn_MTS_Service_Order_Tracking_VerifyRejection function", "Exiting function as there was some error in previous step"
        Exit function
    End If

    PbWindow("MTS").PbWindow("Sevice_Order_Tracking").Activate

        If pistr_Reject_Reason <> "" Then

            str_Reject_Reason = PbWindow("MTS").PbWindow("Sevice_Order_Tracking").PbDataWindow("panel_Business_Accept_Reject").GetCellData("#1","event_cd")

        End If

        If pistr_Explanation <> "" Then

           str_Explanation = PbWindow("MTS").PbWindow("Sevice_Order_Tracking").PbDataWindow("panel_Business_Accept_Reject").GetCellData( "#1","explanation")
            
        End If
        
        If lcase(trim(str_Reject_Reason)) = lcase(trim(pistr_Reject_Reason))  Then
            Reporter.ReportEvent micPass,"fn_MTS_Service_Order_Tracking_VerifyRejection function", "Rejection Reason is same. Expected value (" & pistr_Reject_Reason & " - " & pistr_Explanation & ") matched actual value ("& str_Reject_Reason & " - " & str_Explanation & ")"
        Else
            gFWbln_ExitIteration = "Y"
            gFWstr_ExitIteration_Error_Reason = "fn_MTS_Service_Order_Tracking_VerifyRejection function - Rejection Reason is different. actual is " & str_Reject_Reason
            Reporter.ReportEvent micFail,"fn_MTS_Service_Order_Tracking_VerifyRejection function", "Rejection Reason is different. Expected value was (" & pistr_Reject_Reason & " - " & pistr_Explanation & ") whereas actual is ("& str_Reject_Reason & " - " & str_Explanation & ")"
            fn_MTS_Service_Order_Tracking_VerifyRejection = "FAIL"
        End If

    PbWindow("MTS").PbWindow("Sevice_Order_Tracking").Activate
    PbWindow("MTS").PbWindow("Sevice_Order_Tracking").PbButton("btn_Close").Click

    ' At the end of the function, check if there was any error during any operation and if error needs to be generated

    ' Write information to Run Log (database)


End Function ' end of function fn_MTS_Service_Order_Tracking_VerifyRejection