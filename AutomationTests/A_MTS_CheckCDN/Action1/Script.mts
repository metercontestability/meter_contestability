﻿Window("MTS_Window").WinMenu("Menu").Select "Transactions;Customer / Site Details;Search Customer Details Notifications Received"

strDate = Right("0" & DatePart("d",Date), 2) &"/"& Right("0" & DatePart("m",Date), 2)&"/"&DatePart("yyyy",Date)

PbWindow("MTS").PbWindow("w_cust_det_notif_srch").PbDataWindow("dw_search").SetCellData "#1","NMI",Parameter.Item("pNMI") @@ hightlight id_;_1574434_;_script infofile_;_ZIP::ssf1.xml_;_
 @@ hightlight id_;_1574434_;_script infofile_;_ZIP::ssf2.xml_;_
PbWindow("MTS").PbWindow("w_cust_det_notif_srch").PbDataWindow("dw_search").SetCellData "#1","From",strDate @@ hightlight id_;_1574434_;_script infofile_;_ZIP::ssf3.xml_;_
 @@ hightlight id_;_1574434_;_script infofile_;_ZIP::ssf4.xml_;_
PbWindow("MTS").PbWindow("w_cust_det_notif_srch").PbDataWindow("dw_search").SetCellData "#1","To",strDate @@ hightlight id_;_1574434_;_script infofile_;_ZIP::ssf5.xml_;_

PbWindow("MTS").PbWindow("w_cust_det_notif_srch").PbButton("Display").Click @@ hightlight id_;_2951022_;_script infofile_;_ZIP::ssf6.xml_;_
 @@ hightlight id_;_4064812_;_script infofile_;_ZIP::ssf10.xml_;_
 
strStatus = PbWindow("MTS").PbWindow("w_cust_det_notif_srch").PbDataWindow("dw_slct").GetCellData ("#1","req_status_cd") @@ hightlight id_;_4064812_;_script infofile_;_ZIP::ssf11.xml_;_
 @@ hightlight id_;_2623338_;_script infofile_;_ZIP::ssf26.xml_;_
 If strStatus = "COMPLETED" Then
 	Reporter.ReportEvent micPass,"PASS","CDN successfully created"
	Else 
	Reporter.ReportEvent micFail,"FAIL","CDN Failed to Create with status as "&strStatus
 End If
 
PbWindow("MTS").PbWindow("w_cust_det_notif_srch").PbButton("Close").Click @@ hightlight id_;_67660_;_script infofile_;_ZIP::ssf27.xml_;_