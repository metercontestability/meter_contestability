﻿'Get the input parameters
'Company: CITI, PCOR, SAPN
'ENV: DEV, SYSTEST, UAT
'RunCount: Number of iterations
Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")
Environment.Value("RUNCOUNT")=Parameter.Item("RunCount")



Environment.Value("DbQuery_Scenario") = "MTS_FIND_AMI_CDN"
'Load the environment details for database access and get the database queries
Loaddata

'Set Temporary folder for the test
Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL")&"CDN\"

'Save Datamine query in a variable to use it for all iterations
Environment.Value("FindNMI") = Environment.Value("DBQuery")


'Open MTS

RunAction "A_MTS_OpenMTS [A_MTS_OpenMTS]", oneIteration


For i = 1 To Environment.Value("RUNCOUNT")
Environment.Value("DBQuery") = Environment.Value("FindNMI")

'Get the RRIM MeterCompan
strNMI = ExecuteDatabaseQuery

'Get the query for fetching LNSP
GetRole strNMI,"FRMP"
strFRMP = ExecuteDatabaseQuery

'Get the query for fetching LNSP
GetRole strNMI,"LNSP"
strLNSP = ExecuteDatabaseQuery


strFileName = "CDN_"&strNMI&"_"&datepart("d",date)&hour(time)&minute(time)&second(time)

CreateCDNXML strFRMP,strLNSP,strNMI,strFileName

ZipFile strFileName&".xml",Environment.Value("TESTFOLDER")

copyfile strFileName&".zip",Environment.Value("TESTFOLDER"),Environment.Value("MTS_SO_OUTBOX")

wait 25

ackDestinationFolder = Environment.Value("MTS_SO_INBOX")

expectedACKFileName = strFileName&".ack"

Reporter.ReportEvent micPass, "ack file location", ackDestinationFolder &expectedACKFileName
Dim ackFile
Set ackFile=CreateObject("Scripting.FileSystemObject")
If (ackFile.FileExists(ackDestinationFolder &expectedACKFileName )) Then
    Reporter.ReportEvent micPass, "Ack file generated", "Ack  file generated:" & ackDestinationFolder &expectedACKFileName
    'check ack file has text 'status="Accept" 'in the file
    If (TextExistsInFile(ackDestinationFolder &expectedACKFileName, "status=" & Chr(34) & "Accept" & Chr(34))) AND NOT (TextExistsInFile(ackDestinationFolder &expectedACKFileName, "status=" & Chr(34) & "Reject" & Chr(34)))Then
        Reporter.ReportEvent micPass, "Ack file check for text 'status=" & Chr(34) &"Accept" & Chr(34) & "'", "Found text in ack file"
    Else
        Reporter.ReportEvent micFail, "Ack file check for text 'status=" & Chr(34) &"Accept" & Chr(34) & "'", "Text NOT found in ack file"
    End If
Else 
    Reporter.ReportEvent micWarning, "Ack file generated", "Ack  file NOT generated at:" & ackDestinationFolder &expectedACKFileName
End If

'Check the status of CDN
RunAction "A_MTS_CheckCDN [A_MTS_CheckCDN]", oneIteration,strNMI
Next



RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration