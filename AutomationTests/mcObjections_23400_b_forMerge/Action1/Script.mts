﻿	' GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG CHANGE on 17th Dec GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG
' Set cellReportStatus_DataPermutation = objWS_DataParameter.range("rgWS_cellReportStatus_DataPermutation")
' Multistage scripts have changed cellReportStatus_DataPermutation  to cellReportStatus_SingleScenario
' and rgWS_cellReportStatus_DataPermutation to rgWS_cellReportStatus_SingleScenario
' Updated this script on 15th Dec, this comment needs to be removed once the run is successful
' All RANGE are renamed from rgWS_DataPermutation to rgWS_SingleScenario
' GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG CHANGE GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG


Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong


'fName = "Dani"
'lName = "Vainstein"
Set hFWobj_SB = DotNetFactory.CreateInstance( "System.Text.StringBuilder" ) 
Set gFWobj_SystemDate = DotNetFactory.CreateInstance( "System.DateTime" )
' hFWobj_SB.AppendFormat "My name is {0} {1}, I own a {2} and {3} name is {4}", fName, lName,  "dog", "his", "Star"
' Print gFWobj_SB.ToString


' push the RunStats to the DriverWS report-area
Dim cellTopLeft, iTS, iTS_Max, rowTL, colTL

Dim strRunLog_Folder

' -----

Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq



dim qtTest, qtResultsOpt, qtApp

Set qtApp = CreateObject("QuickTest.Application")	' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
If qtApp.launched <> True then
	qtApp.Launch
End If

qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
qtApp.Options.Run.RunMode = "Fast"
qtApp.Options.Run.ViewResults = False


Set qtTest = qtApp.Test											' http://www.geekinterview.com/question_details/64501
Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions") 	' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539

' qtTest.Settings.Run.OnError = "NextStep" ' Instruct QuickTest to perform next step when error occurs
' qtTest.Run qtResultsOpt,true ' Run the test
' ' msgbox qtTest.LastRunResults.Status
' qtTest.Close ' Close the test

' msgbox "Err number - " & err.number
' msgbox "Base Dir - " & BASE_AUTOMATION_DIR
' msgbox "Test Name - " & qtTest.Name

gDt_Run_TS = tsOfRun
Dim scratch
scratch = fnAttachStr(BASE_AUTOMATION_DIR,"\","R", true)
' msgbox "scratch  - " & scratch 

strRunLog_Folder = hFWobj_SB.AppendFormat ("{0}DataSheets\runs\{1}_{2}_{3}_{4}\"					, _
		fnAttachStr(BASE_AUTOMATION_DIR,"\","R", gcLibBln_OnlyIfNotAlreadyThere)	, _
		qtTest.Name																	, _
		Parameter.Item("Environment")												, _
		Parameter.Item("Company") 													, _
		fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`ddDdD_HH`mm")	).toString


' msgbox "Err number after - " & err.number
' msgbox "Run log folder - " & strRunLog_Folder


Path_MultiLevel_CreateComplete strRunLog_Folder 
gFWstr_RunFolder = strRunLog_Folder 

' ----------------------------------

qtResultsOpt.ResultsLocation = strRunLog_Folder
'
'\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\DataSheets\runs
'
'\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\DataSheets\runs

' option explicit ' qq reinstate later
'Get the input parameters
'Company: CITI, PCOR, SAPN
'ENV: DEV, SYSTEST, UAT
'	RunCount: Number of iterations

' qq the framework does not use QTP's iteration-management (i.e. via datasheet),
'    instead, it uses the objWS_Driver rowtype=rtDataPermutation, so all row-loops in all stages, need to manage this themselves,
'    using suggested variables like the below :
'	fnAFW_wIncrementGlobalCounter()
'                gFwInt_DataParameterRow_ErrorsTotal 
'                gFwInt_AbandonIteration_ReasonCount



		'''''''''''		PbWindow("MTS").CaptureBitmap gFwStrScreenCapFqFileName,True 'p2 is overwrite=true/false
		'''''''''''		' qq wrap these lines into a fnXX, and '
		'''''''''''    	reporter.ReportEvent  micWarning, "NMI `" & piStr_NMI & "` not found.", "", gFwStrScreenCapFqFileName
		gFwInt_DataParameterRow_ErrorsTotal = 0 : gFwInt_AbandonIteration_ReasonCount = 0
		gFWbln_ExitIteration = false
    	
		Set gFWoDnf_SysDiags = Dotnetfactory.CreateInstance("System.Diagnostics.StackFrame")
		
    	dim MethodName 
    	On error resume next
    '	MethodName = new oDnf_SysDiags.StackTrace().GetFrame(0).GetMet hod().Name
    	MethodName = oDnf_SysDiags.GetMethod().Name
    	On error goto 0
    	
    	
    	


' =========  
' =========  Stage 00   - Setup the Stage-reporting elements =============
' =========  

Dim micFinalStatus, intStageSuccessCount, intWantedSuccessCount : intStageSuccessCount = 0
Dim ctSucc, intStageSuccessAr : intStageSuccessAr = split(",0,0,0,0", ",") : intWantedSuccessCount = uBound(intStageSuccessAr)
Dim BASE_XML_Template_DIR

' =========  
' =========  Stage 0a   - Expand the Template Rows into DataPermutation Rows  =============
' =========  

Dim tsAr_EventStartedAtTS : tsAr_EventStartedAtTS = split(",,,,,,,,,,", ",")
Dim tsAr_EventEndedAtTS : tsAr_EventEndedAtTS = split(",,,,,,,,,,", ",")
Dim tsAr_EventPermutationsCount : tsAr_EventPermutationsCount = split("0,0,0,0,0,0,0,0,0,0,0", ",")
const iTS_Stage_0 = 0
const iTS_Stage_I = 1
const iTS_Stage_II = 2
const iTS_Stage_III = 3

tsAr_EventStartedAtTS(iTS_Stage_0) = now()

Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")

Dim objNet : Set objNet = CreateObject("WScript.NetWork") '  objNet.UserName objNet.ComputerName 

' Environment.Value("RUNCOUNT")=Parameter.Item("RunCount") ' not required



    Dim intColNames_RowNr, intColNames_ColNr_Start, intColNames_ColNr_End
    Dim r_rtTemplate, r_rtDataPermutation, intColNr_RowType, intColNr_DataCol_First, intColNr_DataCol_Last, intColNr_InScope, intColNr_ScenarioStatus


Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO" ' "MTS_FIND_MRIM_SO"

' Load the MasterWorkBook objWB_Master
'Dim strWB_noFolderContext_onlyFileName : strWB_noFolderContext_onlyFileName = "MeterContestablity_Objections.v2.xls"'
'Dim strWB_noFolderContext_onlyFileName : strWB_noFolderContext_onlyFileName = "MeterContestablity_Objections.v4.xls"
' Dim strWB_noFolderContext_onlyFileName : strWB_noFolderContext_onlyFileName = "MeterContestablity_Objections.v4_NEWSQLs.xls"
Dim strWB_noFolderContext_onlyFileName : strWB_noFolderContext_onlyFileName = "MeterContestablity_Objections.v12.xls"

 
fnExcel_CreateAppInstance  objXLapp, true
'LoadData_RunContext objXLapp, objWB_Master, objWS_TestRunContext, 1,  BASE_AUTOMATION_DIR & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr
LoadData_RunContext objXLapp, objWB_Master, objWS_TestRunContext, 1,  BASE_AUTOMATION_DIR & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder

' load the DataParameter worksheet objWS_DataParameter - this contains the DataScenarios to test
'
' set objWS_DataParameter    = objWB_Master.worksheets("wsDT_MeterCntestblty_Objections")
set objWS_DataParameter    = objWB_Master.worksheets("wsSsDT_mrCntestbty_Objectns") 
set objWS_templateXML_TagDataSrc_Tables    = objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' store XML Template WS name

MethodName = gFWoDnf_SysDiags.GetMethod().Name

objWS_DataParameter.range("rgWS_RunConfig_ENV"             ).cells(1,1).formula = "'" & Parameter.Item("Environment")
objWS_DataParameter.range("rgWS_runConfig_COY"             ).formula = "'" & Parameter.Item("Company")
objWS_DataParameter.range("rgWS_cellReportStatus_runPC"    ).formula = "'" & objNet.ComputerName 
objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula = "'" & objNet.UserName 

'objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula = "'" & fnWindows_UserName(false)'
'objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula = "'" & fnFW_GetComputerName()



int_NrOf_InScope_Rows      = objWS_DataParameter.range("rgWS_Count_NrOf_needed_NMIs").value
int_NrOf_InScope_Rows      = cInt(int_NrOf_InScope_Rows * 1.5) + 10 ' get some extra rows in case some of those identified are reserved for other cases
int_NrOf_NMI_rows_required = int_NrOf_InScope_Rows * (intSize_listOldNew + 1)

Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE, cellReportStatus_SingleScenario
set cellReportStatus_FwkProcessStage = objWS_DataParameter.range("rgWS_cellReportStatus_FwkProcessStage")
set cellReportStatus_ROLE            = objWS_DataParameter.range("rgWS_cellReportStatus_ROLE")


Set cellReportStatus_SingleScenario = objWS_DataParameter.range("rgWS_cellReportStatus_SingleScenario")


'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"

set cellTopLeft = objWS_DataParameter.range("rgWS_RunReport_TopLeftCell_Stage0_StartTS")
' rowTL = cellTopLeft.row : colTL = cellTopLeft.column
rowTL = 1 : colTL = 1

'  <<<=========   expand the rows (already done)
'                 expand the rows (already done) - in the objWS_DataParameter in the MasterWB
'                 expand the rows (already done)
'                 expand the rows (already done)
'                 expand the rows (already done)
' sb_rtTemplate_Expand objWS_DataParameter, dictWSDataParameter_KeyName_ColNr


' objExcel.Application.Run "test.xls!sheet1.csi"
' objXLapp.Run gStrWB_noFolderContext_only_RunVersion_FileName & "!sb_rtTemplate_Expand"     ' qqq <<<====   needs fixing, wouldn't run the macro

' objXLapp.Calculation = xlAutomatic qq later, and ensure that    xlAutomatic     is defined


' =========  
' =========  Stage 0b   - Technical Setup for Running the DataPermutation Rows   =============
' =========  


Dim objDBConn_TestData_A, objDBRcdSet_TestData_A 
dim r

On error resume next
Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
r = 0
set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")


Dim strAr_listOldNew, intSize_listOldNew, str_listOldNew
'str_listOldNew = ucase(Parameter.Item("listOldNew")) ' for debugging
'if left(str_listOldNew,1) <> "," then str_listOldNew = "," & str_listOldNew
'Environment.Value("listOldNew") = str_listOldNew 

'strAr_listOldNew = split(Environment.Value("listOldNew"), ",")  ' this list will be one of :  old  new old,new  so we split the list with a comma to find out what permutations we're going to test
'intSize_listOldNew = uBound(strAr_listOldNew)


Dim objXLapp, objWB_Master, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
dim objWS_DataParameter, int_NrOf_InScope_Rows, int_NrOf_NMI_rows_required, dictWSDataParameter_KeyName_ColNr



'fnWS_LoadKey_ColOffsets_toDictionary objWS_TestRunContext, 1, 1, 50, "", dictWSTestRunContext_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
Dim DB_CONNECT_STR_MTS 
DB_CONNECT_STR_MTS = "Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & Environment.Value("HOSTNAME") & ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & Environment.Value("SERVICENAME") & "))); User ID=" & Environment.Value("USERNAME") & ";Password=" & Environment.Value("PASSWORD") & ";"


' dim strTestName, uftapp : Set uftapp = CreateObject("QuickTest.Application") 
' strTestName = uftapp.Test.Name
' Set uftapp = nothing

Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL")&"SORD\"

Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize 
Dim strQueryTemplate, dtD, Hyph, strNMI

Dim strList_NMIsizes, strAr_NMIsizes, nmiSizes
strList_NMIsizes = ",SMALL,LARGE"
strAr_NMIsizes = split(strList_NMIsizes, ",")

Dim oldnew, oldnewMax, n
oldnewMax = intSize_listOldNew

Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
intTestData_RowIsInScope_ColNr 	= dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
intCatsCR_ColNr                	= dictWSDataParameter_KeyName_ColNr("CATS_CR")
intColNR_BaseDate				= dictWSDataParameter_KeyName_ColNr("BaseDate") 
intColNR_DateOffset				= dictWSDataParameter_KeyName_ColNr("DayOffset")
intColNr_sizeOfNMI				= dictWSDataParameter_KeyName_ColNr("list_tNMI")

Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0

Dim intSQL_Pkg_ColNr, strSQL_RunPackage


Dim intRowNr_DPs_SmallLarge : intRowNr_DPs_SmallLarge = objWS_DataParameter.range("rgWS_RowNr_DPs_SmallLarge").row

Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn


' Setup the DataPermutation row range to run over - qq this was done when TemplateRows had already been expanded to DataPermutationRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
'                                                 - qq this was done when TemplateRows had already been expanded to DataPermutationRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
'                                                 - qq this was done when TemplateRows had already been expanded to DataPermutationRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
' Setup the DataPermutation row range to run over - qq this was done when TemplateRows had already been expanded to DataPermutationRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
intRowNr_LoopWS_RowNr_StartOn = objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
'intRowNr_LoopWS_RowNr_StartOn = 20

'intRowNr_LoopWS_RowNr_StartOn = fnSetIfTrueFalse(objNet.UserName, "EQ", "brmoloney", fnVntOrIntRowNr_objWS_RangeRowNr (objWS_DataParameter, "rgWS_CR1xxx_RowNr_First", 0), intRowNr_LoopWS_RowNr_StartOn)
'intRowNr_LoopWS_RowNr_StartOn = fnSetIfTrueFalse(objNet.UserName, "EQ", "brmoloney", fnVntOrIntRowNr_objWS_RangeRowNr (objWS_DataParameter, "rgWS_CR6xxx_RowNr_First", 1), intRowNr_LoopWS_RowNr_StartOn)

'intRowNr_LoopWS_RowNr_StartOn = fnSetIfTrueFalse(objNet.UserName, "EQ", "uanand", 1225, intRowNr_LoopWS_RowNr_StartOn)


intRowNr_LoopWS_RowNr_FinishOn = objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
'intRowNr_LoopWS_RowNr_FinishOn = fnSetIfTrueFalse(objNet.UserName, "EQ", "brmoloney", intRowNr_LoopWS_RowNr_StartOn+2, intRowNr_LoopWS_RowNr_FinishOn)
'intRowNr_LoopWS_RowNr_FinishOn = fnSetIfTrueFalse(objNet.UserName, "EQ", "brmoloney", fnVntOrIntRowNr_objWS_RangeRowNr (objWS_DataParameter, "rgWS_CR6xxx_RowNr_First", 1), intRowNr_LoopWS_RowNr_FinishOn)
' Setup the DataPermutation row range to run over - qq this was done when TemplateRows had already been expanded to DataPermutationRows by Stage 0; how will this be managed when Stage 0 is reinstated ?

' intRowNr_LoopWS_RowNr_FinishOn = fnSetIfTrueFalse(objNet.UserName, "EQ", "uanand", 1231, intRowNr_LoopWS_RowNr_FinishOn)

'intRowNr_LoopWS_RowNr_FinishOn = 22

objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").formula = "'" & intRowNr_LoopWS_RowNr_StartOn
objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).formula = "'" & intRowNr_LoopWS_RowNr_FinishOn


tsAr_EventEndedAtTS(iTS_Stage_0) = now()

' =========  
' =========  Stage I   - Gather NMI's & other test-data that will serve as inputs to the test =============
' =========  



tsAr_EventStartedAtTS(iTS_Stage_I) = tsAr_EventEndedAtTS(iTS_Stage_0)

cellReportStatus_FwkProcessStage.formula = "'" & "=========  Stage I   - Gather NMI's & other test-data that will serve as inputs to the test"
cellReportStatus_ROLE.formula = "'" 
cellReportStatus_SingleScenario.formula = "'" 

    
'   qq load the dictionary from the rgWS_ColumName_Row
    intColNr_RowType = dictWSDataParameter_KeyName_ColNr.Item("RowType")
' qq - FrameWork.Rule - retain this single evaluation here so that work can be done concurrently by different test-engineers
 
 
 	select case objNet.ComputerName 
 		case "PCA18123", "PCA15187", "PCA16100", "CORPVMUFT08"    ' Umesh
			intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("InScope")
 		case "PCA15187" '  Brian
			intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("debug_InScope") 
		Case else
			intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("InScope")
 	End select
 	
 	
 	
 	

'	set the state to run the dataPermutations over, based on the company being tested
	Select Case ucase(Parameter.Item("Company"))
		Case "CITI", "PCOR"
			strScratch = "VIC"
		Case "SAPN"
			strScratch = "SA"
		Case else
			strScratch =  hFWobj_SB.AppendFormat ("Case `COMPANY` parm was passed, but value was invalid (`{0}`), i.e. not one in {CITI`POWERCOR`SAP}.", Parameter.Item("Company") ).ToString
			reporter.ReportEvent micFail, strScratch, ""
			ExitAction ' exit the test case
	End Select
	
	Dim tgtCol_State : tgtCol_State = dictWSDataParameter_KeyName_ColNr("list_tState")
	For intScratch = intRowNr_LoopWS_RowNr_StartOn to intRowNr_LoopWS_RowNr_FinishOn 
		' If objWS_DataParameter.cells( intScratch, intColNr_RowType).value = "rtDataPermutation" Then
		If objWS_DataParameter.cells( intScratch, intColNr_RowType).value = "rtSingleScenario" Then
			objWS_DataParameter.cells( intScratch,tgtCol_State ).formula = strScratch
		End If
	Next
 	
 
' qq select case strMachineName    set     intColNr_InScope
' qq select case strMachineName    set     intColNr_InScope
' qq select case strMachineName    set     intColNr_InScope
    
    
    Dim vntAr_RowTypes, vntAR_ColumnTypes
    
'   Create new DataPermutation rows from the DataTemplate rows

    
    
'   <<==  Remove all rtDataParameter Rows
'         Remove all rtDataParameter Rows
'         Remove all rtDataParameter Rows
'         Remove all rtDataParameter Rows
 '            sb_rtTemplate_RemoveDataParm_Rows_WS objWS_DataParameter, dictWSDataParameter_KeyName_ColNr
    
'   Create new DataPermutation rows from the DataTemplate rows
    
    Dim intArSz_Role, intArSz_State, intArSz_NorC, intArSz_sizeNMI
    Dim iRole, iState, iNorC, isizeNMI
    Dim colNr_Role, colNr_State, colNr_NorC, colNr_sizeNMI
    Dim strAr_acrossA_Role, strAr_downA_State, strAr_downB_NorC, strAr_downC_sizeNMI
    Dim strRow_DataPermutation_Template, strRow_DataPermutation: strRow_DataPermutation_Template = "tr<TemplateRow>rl<RoleNr>st<State>nc<NorC>nmi<NMI>"
    Dim objCell_NewRowCell
    Dim strRole_NorC_Negative, cStr_OmitThisCombination: cStr_OmitThisCombination = "-"
    Dim intNrOfNewRows: intNrOfNewRows = 0
    Dim intDisplayChanges_Interval: intDisplayChanges_Interval = 0
    Dim intColNr_SQL_DataParameter, strMasterWS_ColumnName
    Dim intColNr_XML_DataTemplate_FileNameQQ
    Dim strSQL_TemplateName_Current, strSQL_TemplateName_Previous, strScratch, intScratch, cellScratch
    Dim strXML_TemplateName_Current, strXML_TemplateName_Previous
    Dim intColNr_NMI, intColNr_TestResult
    Dim strSuffix_Expected, strSuffix_Actual, strSuffix_NMI
    Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true
    Dim qintColNr, qintColsCount, strSqlCols_NamesList, strSqlCols_ValuesList, strSql_Suffix, qintFieldFirst_Nr : qintFieldFirst_Nr = 0
    Dim strAr_SqlCol_NamesAr, strAr_SqlCol_ValuesAr, dictSqlData_forThisRow_ColValues
    Dim colNr_SqlCol_Names, colNr_SqlCol_Values
    Dim strInScope_SmallLarge_Ar, intCt_AdditionalCriteria_All, cellNMI, cellExpectedValue, cellActualValue
    
    strSuffix_Expected = objWS_DataParameter.range("rgWS_Suffix_Actual").value
    strSuffix_Actual   = objWS_DataParameter.range("rgWS_Suffix_Expected").value
    strSuffix_NMI      = objWS_DataParameter.range("rgWS_nameNMI").Value

'	this column contains the name of the SQL to be used for this group of Cats_CR's
	intColNr_SQL_DataParameter = dictWSDataParameter_KeyName_ColNr("SQL_Template_Name")

'   the columns that the DataParms appear in, are constant, for    State, NorC and sizeNMI
    colNr_State   = dictWSDataParameter_KeyName_ColNr("list_tState")
	colNr_NorC    = dictWSDataParameter_KeyName_ColNr("this_tNorC")
    colNr_sizeNMI = dictWSDataParameter_KeyName_ColNr("list_tNMI")
    
    colNr_SqlCol_Names  = dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")
    colNr_SqlCol_Values = dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")


'   the DataParms for Role are constant for the run
    strAr_acrossA_Role = Split(fnAttachStr(objWS_DataParameter.Range("rgWS_Role_List").value, ",", "L", True), ","): intArSz_Role = UBound(strAr_acrossA_Role)
    
'	Retrieve the ColNr for the SQL_Template names
    intColNr_SQL_DataParameter = dictWSDataParameter_KeyName_ColNr("SQL_Template_Name")
    strSQL_TemplateName_Previous = ""
    
'	Retrieve the ColNr for the XML_Template names
	intColNr_XML_DataTemplate_FileNameQQ = dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")
	

	strAr_downC_sizeNMI = Split(fnAttachStr(objWS_DataParameter.Range("rgWS_listOf_sizeOf_NMI").value, ",", "L", True), ","): intArSz_sizeNMI = UBound(strAr_downC_sizeNMI)
	For isizeNMI = 1 To intArSz_sizeNMI
	
		strSQL_TemplateName_Previous = "<none>"
		strSQL_TemplateName_Current = "" 

    '	iterate over all the DataPermutation Rows
    	r_rtDataPermutation          = intRowNr_LoopWS_RowNr_StartOn
	    Do
	    
	    	cellReportStatus_SingleScenario.formula = "'" & r_rtDataPermutation
	        
    	' InScope Row                
	        If ( (UCase(objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_InScope).Value) = "Y") and _
	             (objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_InScope).EntireRow.Height <> 0 ) ) _
	        Then ' InScope       and RowIsNot_Hidden_ieHeightIsZero       ' qq remove the row.Hidden part ?  or retain for flexibility
	
	           ' objWS_DataParameter.Rows(r_rtDataPermutation).Select

	            
	            If objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_RowType).Value = cStr_rtSingleScenario Then
	
				'	if the SizeOfNMIs the current query is returning is the same as this row ...
					If strAr_downC_sizeNMI(iSizeNMI) = objWS_DataParameter.Cells( r_rtDataPermutation, colNr_sizeNMI ).value Then
	            		
		
					'	report the NMI size being processed
						objWS_DataParameter.range("rgWS_cellReportStatus_NMI_Size").formula = "'" & strAr_downC_sizeNMI(iSizeNMI)
						
						strScratch = objWS_DataParameter.Cells( r_rtDataPermutation, intColNr_SQL_DataParameter ).value 
					' The RULE HERE is that when an SQL is specified, so will be the corresponding list of SQL-sourced-DataTags
						 If ((strScratch <> "") or (strSQL_TemplateName_Previous = "<none>")) Then ' ignore blank cells  
							strSQL_TemplateName_Current = strScratch
							If strSQL_TemplateName_Current <> strSQL_TemplateName_Previous Then
							
							
							
    							strMasterWS_ColumnName = strSQL_TemplateName_Current 
								intSQL_ColNr           = dictWSTestRunContext_KeyName_ColNr( strMasterWS_ColumnName )  '   "MTS_FIND_NMI_withNo_METER"
    							strQueryTemplate       = objWS_TestRunContext.cells(2,intSQL_ColNr).value ' 
    							' strQueryTemplate       = objWS_TestRunContext.cells(r_rtDataPermutation, intSQL_ColNr).value
    							
								
								strSQL_FindActiveNmi_ofSize = strQueryTemplate
								strSQL_FindActiveNmi_ofSize = replace ( strSQL_FindActiveNmi_ofSize , "<NMI_Size>", strAr_downC_sizeNMI(isizeNMI), 1, -1, vbTextCompare)
								strSQL_FindActiveNmi_ofSize = replace ( strSQL_FindActiveNmi_ofSize , "<rownum>"  , int_NrOf_InScope_Rows   , 1, -1, vbTextCompare) ' qq

							'	write the SQL to the worksheet, save the objWB, and ...
								Set cellScratch = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("SQL_Populated"))
								With cellScratch
									.formula = "'" & strSQL_FindActiveNmi_ofSize
									.WrapText = False
								End With
								Set cellScratch = nothing
								objWB_Master.save
							'	... run the SQL
								fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_FindActiveNmi_ofSize, int_NrOf_InScope_Rows
						
								strSQL_TemplateName_Previous = strSQL_TemplateName_Current 
								
								qintColsCount = objDBRcdSet_TestData_A.Fields.Count
	                    		 strSqlCols_NamesList = "" : strSql_Suffix = "," ' we'll make the list 1-based
	                    	'	 strSqlCols_ValuesList = "" : strSql_Suffix = ","  ' we'll make the list 1-based
	                    		
								For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count 
									strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
								'	strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
									strSql_Suffix = ","
								Next		                    	

							End If
						Else' What happens if strScratch = "" 
							strSQL_TemplateName_Current = "" ' added by Umesh 
						End If
						            
					
		            '   the DataParms for     State, NorC and sizeNMI    change with every row
		            '    strAr_downA_State   = Split(fnAttachStr(objWS_DataParameter.Range("rgWS_listOf_States").Value, ",", "L", True), ","): intArSz_State = UBound(strAr_downA_State)
		            '    strAr_downB_NorC    = Split(fnAttachStr(objWS_DataParameter.Range("rgWS_listOf_NorC").Value, ",", "L", True), ","): intArSz_NorC = UBound(strAr_downB_NorC)
		                For iRole = 1 To intArSz_Role

' qq this loop should report an error if it fails to find a role in the strAr_acrossA_Role array
' qq this loop should report an error if it fails to find a role in the strAr_acrossA_Role array\
' qq -  as part of the setup / initialize / verify process
' qq this loop should report an error if it fails to find a role in the strAr_acrossA_Role array
' qq this loop should report an error if it fails to find a role in the strAr_acrossA_Role array
' qq this loop should report an error if it fails to find a role in the strAr_acrossA_Role array

							cellReportStatus_ROLE.formula = "'"  & strAr_acrossA_Role(iRole)
							intColNr_ScenarioStatus = dictWSDataParameter_KeyName_ColNr("ps" & strAr_acrossA_Role(iRole))

		                	
		                '   the columns that the DataParms appear in, for Role, change as the role changes
		                    colNr_Role = dictWSDataParameter_KeyName_ColNr(strAr_acrossA_Role(iRole))
		                    ' colNr_Role+0
							objWS_DataParameter.range(	objWS_DataParameter.Cells(r_rtDataPermutation, intWsColNr_ParmsStart), _
	                									objWS_DataParameter.Cells(r_rtDataPermutation, colNr_Role+3) ).select		
	                									
'							sbRunningRange_BorderShow objWS_DataParameter.range(	objWS_DataParameter.Cells(r_rtDataPermutation, colNr_Role+0), _
'	                									objWS_DataParameter.Cells(r_rtDataPermutation, colNr_Role+3) )
							' objWS_DataParameter.selection
							
							
							strScratch = strAr_acrossA_Role(iRole) & "_" & strSuffix_NMI ' strSuffix_Actual    strSuffix_Expected 
		                    intColNr_NMI = dictWSDataParameter_KeyName_ColNr(strScratch)

		                    
		                    intCt_AdditionalCriteria_All = 0
		                    strInScope_SmallLarge_Ar = split(trim(ucase(objWS_DataParameter.Cells(intRowNr_DPs_SmallLarge, colNr_Role).value)), ",", -1, vbBinaryCompare)
		                  																		  
							set cellNMI = objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_NMI)
							
		                    Select Case ucase(trim(objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_sizeOfNMI).Value))
		                    	case "SMALL" : 
			                    	If strInScope_SmallLarge_Ar(0) = "SMALL+" Then 
			                    		intCt_AdditionalCriteria_All = intCt_AdditionalCriteria_All + 1
			                    	else
			                    		'	objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_NMI)
			                    		cellNMI.Formula = "'nr.SMALL-"  
			                    		cellNMI.HorizontalAlignment = -4108 ' xlCenter
			                    	End if
		                    	case "LARGE" : 
			                    	If strInScope_SmallLarge_Ar(1) = "LARGE+" Then 
			                    		intCt_AdditionalCriteria_All = intCt_AdditionalCriteria_All + 1
			                    	else
			                    		cellNMI.Formula = "'nr.LARGE-"  
			                    		cellNMI.HorizontalAlignment = -4108 ' xlCenter
			                    	End if
		                    	Case else
		                    End Select
		                    
		                    
		                    If intCt_AdditionalCriteria_All = 1 Then ' In-Scope for SMALL+-/LARGE+-
		                    	
			                    strScratch = objWS_DataParameter.Cells(r_rtDataPermutation, colNr_Role).Value
			                    
			                    select case ucase(trim(strScratch)) 
			                    '   DataPERMUTATION rows only ever contain one value in these ROLE columns, in this case,   N  or C   or  -
			                    
			                    	Case "N", "C" : ' this is an In-Scope DataPermutation, so put the testData-NMI into the current Role's NMI-Column
			                    		intCt_AdditionalCriteria_All = intCt_AdditionalCriteria_All + 1
			                    	Case else
										set cellNMI = objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_NMI)
			                    		If left(cellNMI.value,3).value <> "nr." Then
			                    			cellNMI.formula = "nr." & cellNMI.value
			                    		End if
			                    End select

		                    End If
		                    set cellNMI = nothing

		                    
'		                    objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_NMI).select
		                    if intCt_AdditionalCriteria_All <> 2 then ' the role is out of scope for one or both 
		                    
		                    '	shade the cells to indicate OutOfScope
	                		    With objWS_DataParameter.range(	objWS_DataParameter.Cells(r_rtDataPermutation, colNr_Role+0), _
	                		    								objWS_DataParameter.Cells(r_rtDataPermutation, colNr_Role+3) ).Interior
							        .Pattern = xlSolid
							        .PatternColorIndex = xlAutomatic
							        .ThemeColor = xlThemeColorDark1
							        .TintAndShade = -0.499984740745262
							        .PatternTintAndShade = 0
							    End With
									    
							else' the row is In-Scope for BOTH    SMALL+-/LARGE+- AND    New+-/Current+-
		                    
		                    		
		                    		strNMI = objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
		                    		tsAr_EventPermutationsCount(iTS_Stage_I) = tsAr_EventPermutationsCount(iTS_Stage_I) + 1 ' increment the permutations count
		                    	'	set the initial scenario status to fail
		                    		' c.cells(r_rtDataPermutation, intColNr_ScenarioStatus).value = objWS_DataParameter.range("rgWS_DataPermutation_RunStatus_BusinessProcess_Fail").value
		                    		c.cells(r_rtDataPermutation, intColNr_ScenarioStatus).value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_BusinessProcess_Fail").value
 

		                  ' =====>>> NMI Reservation
		                  ' =====>>> NMI Reservation
		                  ' =====>>> NMI Reservation
		                    		blnDataReservationIsActive_DefaultTrue = false  ' <<<===  NMI Reservation
		                    		if blnDataReservationIsActive_DefaultTrue then
	 									intReserveDataRC = fnReserveDataForCase_NMI ( strNMI, "CATS_OUT_2001_Objection_a", "", GstrDate_Run_TS_Suffix, "", strReserveData_ResultDesc)
										While intReserveDataRC < gDataRsvnSt_Approved
											objDBRcdSet_TestData_A.MoveNext	
											strNMI = objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
											intReserveDataRC = fnReserveDataForCase_NMI ( strNMI, "CATS_OUT_2001_Objection_a", "", piStr_RunNR, "", strReserveData_ResultDesc)
										Wend ' qq also handle Rs.EOF
									End if
									
									objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_NMI).select
		                    		objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_NMI).Formula = "'" & strNMI ' objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
		                    		
		                    		qintColsCount = objDBRcdSet_TestData_A.Fields.Count
		                    	'	 strSqlCols_NamesList = "" : strSql_Suffix = ","  ' we'll make the list 1-based
		                    		strSqlCols_ValuesList = "" : strSql_Suffix = ","  ' we'll make the list 1-based									
			                    		 
			                    	' retrieve the SQL_Query's fields and store their values
			                    	For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count 
			                    		 

									'	strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
										strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
										strSql_Suffix = ","
									Next		                    	
									
									objWS_DataParameter.Cells(r_rtDataPermutation, colNr_SqlCol_Names ).formula = "'" & strSqlCols_NamesList
									
									strScratch = strAr_acrossA_Role(iRole)
									intScratch = dictWSDataParameter_KeyName_ColNr(strScratch & objWS_DataParameter.range("rgWS_objRS_ColumnValues_roleSuffix").value)
									objWS_DataParameter.Cells(r_rtDataPermutation, intScratch         ).formula = "'" & strSqlCols_ValuesList
								'	objWS_DataParameter.Cells(r_rtDataPermutation, colNr_SqlCol_Values).formula = "'" & strSqlCols_ValuesList
									
		                    		' load the column names and values to the Driver.RunSheet       ' qq PARTICIPANT_NAME


								'	now move to the next row for the next in-Scope row
		                    		objDBRcdSet_TestData_A.MoveNext ' move to the next row in the TestData-RecordSet
		                    		
		                    		' - qq this is applicable for this script only but need a permanent solution.
		                    		' - We would move the recordset to first row as the NMI can be re-used in this script
		                    		If objDBRcdSet_TestData_A.EOF Then
		                    			objDBRcdSet_TestData_A.MoveFirst
		                    		End If

		                    		
		                    end if 
'							sbRunningRange_BorderHide objWS_DataParameter.range(	objWS_DataParameter.Cells(r_rtDataPermutation, colNr_Role+0), _
'	                									objWS_DataParameter.Cells(r_rtDataPermutation, colNr_Role+3) )
							' objWS_DataParameter.selection

		
		            '        For iState = 1 To intArSz_State
		            '            For iNorC = 1 To intArSz_NorC
									'       strRow_DataPermutation = Replace(strRow_DataPermutation_Template, "<TemplateRow>", r_rtTemplate, 1, -1, vbTextCompare)  ' "rlstnc<NorC>nmi<NMI>"
									'       strRow_DataPermutation = Replace(strRow_DataPermutation, "<RoleNr>", iRole, 1, -1, vbTextCompare)        ' "rlstnc<NorC>nmi<NMI>"
									'       strRow_DataPermutation = Replace(strRow_DataPermutation, "<State>", iState, 1, -1, vbTextCompare)
									'       strRow_DataPermutation = Replace(strRow_DataPermutation, "<NorC>", iNorC, 1, -1, vbTextCompare)
									'       strRow_DataPermutation = Replace(strRow_DataPermutation, "<NMI>", isizeNMI, 1, -1, vbTextCompare)
									
			            			'		objWS_DataParameter.Cells(r_rtDataPermutation, 1).Formula = strRow_DataPermutation
			                       	'		objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_RowType).Select
			                                
								'	not required when not inserting / deleting rows
		                       	'	... intRowNr_End = objWS_DataParameter.Range("rgWS_DataRow_Last_PlusOne").Row - 1 

		                    '    Next ' NorC
		                '    Next ' State
		                Next ' Role
	        
		        	End If ' the sizeOfNMI for this row is the same as the query is currently returning
	        	End If ' rt = cStr_rtDataParameter
	        End If ' InScope Row
	     	r_rtDataPermutation = r_rtDataPermutation + 1
	     	
	     	strNMI = "" ' Clear the last NMI for next data row
	     	
	    Loop While r_rtDataPermutation <= intRowNr_LoopWS_RowNr_FinishOn ' (piWS_DataSheet.Range("rgWS_DataRow_Last_PlusOne").Row - 1)
	    

	Next ' sizeNMI loop6
	
	objDBRcdSet_TestData_A.Close
	objDBConn_TestData_A.Close
	
	'	report that all NMI-sizes are finished being processed
	objWS_DataParameter.range("rgWS_cellReportStatus_NMI_Size").formula = "Finished."

	
	objWB_Master.save
	tsAr_EventEndedAtTS(iTS_Stage_I) = now()



iTS_Max = 3 ' Ubound(tsAr_EventEndedAtTS)
For iTS = iTS_Stage_0 To iTS_Stage_III
	cellTopLeft.cells(rowTL    , colTL + iTS).value = tsAr_EventStartedAtTS(iTS)
	cellTopLeft.cells(rowTL + 1, colTL + iTS).value = tsAr_EventEndedAtTS(iTS)
	'        row             +2&3 is the duration, endTS-startTS 
	cellTopLeft.cells(rowTL + 4, colTL + iTS).value = tsAr_EventPermutationsCount(iTS)
Next

' =========  
' =========  Stage II  - merge the Harvested Data with the XML Template, and load the txns onto the queue =============
' =========  


tsAr_EventStartedAtTS(iTS_Stage_II) = tsAr_EventEndedAtTS(iTS_Stage_I)


cellReportStatus_FwkProcessStage.formula = "'" & "=========  Stage II  - merge the Harvested Data with the XML Template, and load the txns onto the queue "
cellReportStatus_ROLE.formula = "'" 
cellReportStatus_SingleScenario.formula = "'" 


Dim dictWSDataParameter_KeyColName_ItemColValue
Set dictWSDataParameter_KeyColName_ItemColValue = CreateObject("Scripting.Dictionary")
dictWSDataParameter_KeyColName_ItemColValue.CompareMode = vbTextCompare

Dim StrXMLTemplateLocation, Str_TransactionID, Date_TransactionDate, Str_Role, Str_RoleStatus, Str_Parcipant, Str_ChangeReasonCode
Dim date_ChgDt_Type_A_P, Int_NMI, Str_list_tState, Str_list_tNMI, Int_DayOffset, strJurisdiction
Dim str_DataParameter_PopulatedXML_FqFileName, int_UniqueReferenceNumberforXML

' load the SQL{ColNames and ColValues) into splitAr's
Dim strAr_ColumnNames, int_ArSize_ColNames, strAr_ColumnValue, int_ArSize_ColValues, intArSQL_elNr
Dim vntSqlCell, strSqlCellContents

Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage

gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
' Get headers of table in a dictionary
Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare


int_MaxColumns = ubound(gvntAr_RuleTable,2)

' Load the first/title row in a dictionary
fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_RuleTable, 1, int_MaxColumns, gdictWStemplateXML_TagDataSrc_Tables_ColValue


Dim strScratch_MatchStr

	For iRole = 1 To intArSz_Role
	
		
	
		Str_Role = strAr_acrossA_Role(iRole)' "LNSP" ' Role parameter from framework
		cellReportStatus_ROLE.formula = "'" & Str_Role
		intColNr_ScenarioStatus = dictWSDataParameter_KeyName_ColNr("ps" & strAr_acrossA_Role(iRole))

   		colNr_Role = dictWSDataParameter_KeyName_ColNr(strAr_acrossA_Role(iRole))
		
		strXML_TemplateName_Previous = ""
		
		strScratch = strAr_acrossA_Role(iRole) & "_" & strSuffix_NMI ' strSuffix_Actual    strSuffix_Expected 
        intColNr_NMI = dictWSDataParameter_KeyName_ColNr(strScratch)
        
        
	'	iterate over all the DataPermutation Rows
	    r_rtDataPermutation = intRowNr_LoopWS_RowNr_StartOn
		Do
	    
	    	cellReportStatus_SingleScenario.formula = "'" & r_rtDataPermutation
	    	
	    	if r_rtDataPermutation = 175 then
	    		r_rtDataPermutation = r_rtDataPermutation
	    	End if

			    	' InScope Row                
	        If ( (UCase(objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_InScope).Value) = "Y") and _
	             (objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_InScope).EntireRow.Height <> 0 ) ) _
	        Then ' InScope       and RowIsNot_Hidden_ieHeightIsZero       ' qq remove the row.Hidden part ?  or retain for flexibility
	

		    	if r_rtDataPermutation = 175 then
		    		r_rtDataPermutation = r_rtDataPermutation
		    	End if

			' objWS_DataParameter.Rows(r_rtDataPermutation).Select

	          '	DataPermutation Rows only (i.e. not DataTemplate)
	            If objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_RowType).Value = cStr_rtSingleScenario Then
	            
	            	' If DataPermutation is In-Scope of the data-rules
					set cellNMI           = objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_NMI)
					set cellExpectedValue = objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_NMI+1) 
					set cellActualValue   = objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_NMI+2) 
	
	                If left(cellNMI.value,3) <> "nr." Then ' qq optimal is case-InSensitive
	                
	                '   this is not a loop, it is the mechanism to do a goto
	                	Do
	                	gFWbln_ExitIteration = false
	                '	the DataPermutation-row's data-combination ** IS ** In-Scope of the data-rules
	                
						' so now, we :    a) merge the DataPermutation with the TemplateXML, giving the RunXML,
						'                a) merge the DataPermutation with the TemplateXML, giving the RunXML,
						'                a) merge the DataPermutation with the TemplateXML, giving the RunXML,
						'       then ...
												
						' here, now, we use a static XML Template;
						'           in future, we'll choose the XML template from the DataParameter row

								' strPopulatedXML = strTemplateXML ' logic here - qq commented by Umesh as implementing a tactical solution at this moment
								
							'	Umesh's function
							
								tsAr_EventPermutationsCount(iTS_Stage_II) = tsAr_EventPermutationsCount(iTS_Stage_II) + 1 ' increment the permutations count

															
								BASE_XML_Template_DIR = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\" ' qq hardcoded but need to come from framework
								
							' The RULE HERE is that when an SQL is specified, so will be the corresponding list of SQL-sourced-DataTags
								strScratch = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
								If strScratch <> "" Then ' ignore blank cells
									strSQL_TemplateName_Previous = strXML_TemplateName_Current
									strXML_TemplateName_Current = strScratch
								End if

								StrXMLTemplateLocation = BASE_XML_Template_DIR & strXML_TemplateName_Current
'								StrXMLTemplateLocation = BASE_XML_Template_DIR & "CN`1xxx_200BadMeter.xmlTP_AvB.xml" ' qq hardcoded but need to come from framework
								int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
								' "CCYYMMDDHHmmss99hhmmss"
								Str_TransactionID =  int_UniqueReferenceNumberforXML ' "txnID_" &
								
								
								' qq - This would need to be taken out once we have multiple stage in place
								With cellNMI
							        .AddComment
								    .Comment.Visible = False
								    .Comment.Text ""
								    .Comment.Text "," & int_UniqueReferenceNumberforXML & "," 
								    ' .Select
							    End With
							    

								' Date_TransactionDate = now ' "<dataRunDt_SrcFWK_fmtAllBut_TimeZone>+10:00" ' sample #11/24/2016 7:12:53 PM#
								Date_TransactionDate = now ' year(now) & "-" & month(now) & "-" & day(now)
								
								
								Str_RoleStatus = objWS_DataParameter.Cells(r_rtDataPermutation, colNr_Role).value ' "N" ' with the role status parameter from worksheet
								
								
								
' qq the below code needs to be refactored. Commenting as this is not required for 1xxx

'							'	ColumnNames cell							
'								Set vntSqlCell = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("DsListSqlColumn_Names"))
'								strSqlCellContents = vntSqlCell.value
'								
'								strAr_ColumnNames = split ( mid(strSqlCellContents, 2), left(strSqlCellContents,1), -1, vbBinaryCompare)
'								int_ArSize_ColNames = Ubound(strAr_ColumnNames)
'								Set vntSqlCell = nothing
'
'							'	ColumnValues cell		
'								Set vntSqlCell = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("DsListSqlColumn_Values"))
'								strSqlCellContents = vntSqlCell.value
'								
'								strAr_ColumnValue = split ( mid(strSqlCellContents, 2), left(strSqlCellContents,1), -1, vbBinaryCompare)
'								int_ArSize_ColValues = Ubound(strAr_ColumnValue)
'								Set vntSqlCell = nothing
'								
'								
'								If int_ArSize_ColNames <> int_ArSize_ColValues then
'								' qq report error
'									
'								End if
'								
'								' Iterate over the splitAr's, loading the value-pairs to the dictionary, as Key=ColName, Item=ColValue
'
'								dictWSDataParameter_KeyColName_ItemColValue.RemoveAll 
'								
'								for intArSQL_elNr = 0 to int_ArSize_ColNames
'									
'									' If dictWSDataParameter_KeyColName_ItemColValue.exists (strColName)  Then
'									dictWSDataParameter_KeyColName_ItemColValue.Add strAr_ColumnName(intArSQL_elNr), strAr_ColumnValue(intArSQL_elNr)
'									
'								next
'								
'
								' set the XML tag-value from the dictionary(ColName)
								' strRoleColumnName = "ROLE_" & strAr_acrossA_Role(iRole)
								'Str_Parcipant = "ROLE_" & strAr_acrossA_Role(iRole) ' As per Brian M, there is no need to execute a SQL for 1xxx

								
								' dictWSDataParameter_KeyColName_ItemColValue.RemoveAll 

								
'								Str_Parcipant = "SQL_Fetch1" ' <Participant><dataParticipant`A_SrcSQL></Participant> ' with piStr_Parcipant 
								' qq GET QUERY FROM BRIAN
																											' qq standardize VarNames, in this case, to intColNr_CatsCR
								Str_ChangeReasonCode = objWS_DataParameter.Cells(r_rtDataPermutation, intCatsCR_ColNr).value ' 1000   ' ' with CR_Code parameter from WS
								Int_NMI = cellNMI.value ' 12345678901
								
								' = dictWSDataParameter_KeyName_ColNr("CATS_CR")
								
								
								
								
							'	str_ChgDt_Type_A_P = "actual" ' with ChgDt_Type_A_P parameter from WS
							' qqrr Does the ** Case **  Matter ?
								str_ChgDt_Type_A_P = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("ChgDt_Type_A_P")).value
								
							'	qqrr should this be 
								date_ChgDt_Type_A_P = now ' with ChgDt_Type_A_P parameter from WS
								
								
							'	Str_list_tState = "VIC" ' ' from WS - list_tState field
								StrTR_list_tState = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("list_tState")).value
								
							'	Str_list_tNMI = "SMALL" ' WS - list_tNMI
								Str_list_tNMI = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("list_tNMI")).value

	'							Int_DayOffset = "1" ' WS - DayOffset
								Int_DayOffset = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("DayOffset")).value


								strJurisdiction = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("list_tState")).value
								

								'Create an XML file
								
								Dim strCaseGroup, strCaseNr, strCsList1_TagsNeedingFlexibleManagement_Current, strCsList1_TagsNeedingFlexibleManagement_Previous
								strCaseGroup = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("ObjectionCD")).value
								strCaseNr    = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("CATS_CR")).value
								
								
								strScratch = objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("xmlTemplate_TagsNeedingFlexiMgt")).value
								If strScratch <> "" Then ' ignore blank cells
									strCsList1_TagsNeedingFlexibleManagement_Previous = strCsList1_TagsNeedingFlexibleManagement_Current
									strCsList1_TagsNeedingFlexibleManagement_Current = strScratch
								End if

								
'C:\_data\XMLs\MC
								Dim strFolderNameWithSlashSuffix_Scratch
								err.clear
								strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"
								strScratch = strAr_acrossA_Role(iRole) & objWS_DataParameter.range("rgWS_objRS_ColumnValues_roleSuffix").value
							'	strFolderNameWithSlashSuffix_Scratch = "F:\qqq\"
								gFWint_NrOf_UnReplaced_Tags_inThe_PopulatedXML_file = 0
								
								str_ChangeStatusCode = "REQ" ' qq - This is hardcoded for this script (build 1) but in a multistage process, this should be the process name. A tag in XML would be updated with this value
								
'								Select Case objNet.ComputerName
'									case "PCA16100", "CORPVMUFT08":
										' fnCreateCATSXML Str_CaseGroup & "_" & Str_CATS_CR, StrXMLTemplate_Folder, StrXMLTemplate_FileNameWoFolder, objWS_DataParameter, "1", gvntAr_RuleTable, gdictWStemplateXML_TagDataSrc_Tables_ColValue


								' create a match string, of the form     <NMI_SIZE>_BADMETER_1000,BADMETER_1000     i.e.  SMALL_BADMETER_1000,BADMETER_1000      or      LARGE_BADMETER_1000,BADMETER_1000
								strScratch_MatchStr = Str_list_tNMI & "_" & strCaseGroup & "_" & strCaseNr
								strScratch_MatchStr = strScratch_MatchStr & "," & strCaseGroup & "_" & strCaseNr

								str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML(Environment.Value("COMPANY"), strScratch_MatchStr, str_ChangeStatusCode, BASE_XML_Template_DIR, strXML_TemplateName_Current, _
																			strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, r_rtDataPermutation, gvntAr_RuleTable, _
																			gdictWStemplateXML_TagDataSrc_Tables_ColValue, Date_TransactionDate, int_UniqueReferenceNumberforXML, _
																			Str_Role, Int_NMI, "csListSqlColumn_Names", strScratch)

										
'									case else
'										str_DataParameter_PopulatedXML_FqFileName = fnUpdate_CNxxxx_2_3_400_xml( _
'											strCaseGroup, strCaseNr, _
'											objWS_DataParameter, r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", strScratch, _  
'											strCsList1_TagsNeedingFlexibleManagement_Current, _
'											BASE_XML_Template_DIR, strXML_TemplateName_Current , strFolderNameWithSlashSuffix_Scratch, "", _
'											int_UniqueReferenceNumberforXML, Str_TransactionID, Date_TransactionDate, Str_Role, Str_RoleStatus, Str_ChangeReasonCode,_
'											str_ChgDt_Type_A_P, date_ChgDt_Type_A_P, Int_NMI, strJurisdiction, STR_list_tState, Str_list_tNMI, Int_DayOffset)
'								End Select
								
								
								
									'	ActiveSheet.Hyperlinks.Add Anchor:=Selection, Address:= _
        							' "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\runs\61030290223`20161201101518`CN`1xxx`200BadMeter`xml`1101542.xml"

								set cellScratch = objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_ScenarioStatus )
								strScratch = strFolderNameWithSlashSuffix_Scratch & str_DataParameter_PopulatedXML_FqFileName
								' qq - need to fix as hyperlinks are not working
								
						    	'objWS_DataParameter.Hyperlinks.Add cellScratch		, strScratch
								'objWS_DataParameter.Hyperlinks.Add cellExpectedValue, strScratch
								' cellScratch.value = objWS_DataParameter.range("rgWS_DataPermutation_RunStatus_InProgress").value
								cellScratch.value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_InProgress").value
								
								
								

								intScratch = len(str_DataParameter_PopulatedXML_FqFileName)
								If  intScratch > gFWint_AEMO_HUB_MaxFileNameLength_Limit Then
									strScratch = ""
									with cellScratch
										.value = "TF"
										On error resume next
											strScratch = .Comment.Text
										On error goto 0
								        .AddComment
									    .Comment.Visible = False
'									    strScratch = strScratch & hFWobj_SB.AppendFormat ("HUB.Parameter\{MaximumFileNameLength} was exceeded, value is `{0}`, `{1}` was submitted.", gFWint_AEMO_HUB_MaxFileNameLength_Limit, intScratch  ).ToString
									    strScratch = strScratch & hFWobj_SB.AppendFormat ("HUB.Parameter`FileName.MaximumLength` was exceeded, value is `{0}`, `{1}` was submitted.", gFWint_AEMO_HUB_MaxFileNameLength_Limit, intScratch  ).ToString
									    .Comment.Text strScratch
									    Reporter.ReportEvent micFail, strScratch, str_DataParameter_PopulatedXML_FqFileName
									'	set the scenario status to TechFail
		                    			' .value = objWS_DataParameter.range("rgWS_DataPermutation_RunStatus_Framework_TechFail").value
		                    			.value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_Framework_TechFail").value
									    gFWbln_ExitIteration = true
								    End With
									' Set cellScratch = nothing	    
								End If
								
								if gFWint_NrOf_UnReplaced_Tags_inThe_PopulatedXML_file > 0 then
									strScratch = ""
									with cellScratch
										.value = "TF"
										On error resume next
											strScratch = .Comment.Text
										'On error goto 0
								        .AddComment
									    .Comment.Visible = False
									    strScratch = strScratch & hFWobj_SB.AppendFormat ("The PopulatedXML still contains `{0}` Un-Replaced tags; iteration abandoned.  Fix and rerun.", gFWint_NrOf_UnReplaced_Tags_inThe_PopulatedXML_file).ToString
									    'On error resume next ' a) in case the cell already has a hyperlink b) qq move this to a function that handles that concern
									    On error goto 0
									    .Comment.Text strScratch
									    Reporter.ReportEvent micFail, strScratch, str_DataParameter_PopulatedXML_FqFileName
									'	set the scenario status to TechFail
		                    			' .value = objWS_DataParameter.range("rgWS_DataPermutation_RunStatus_Framework_TechFail").value ' qq should this move to the fnLib_Automation, if so, push the value to this range as part of FW.TestCase_Setup
		                    			.value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_Framework_TechFail").value ' qq should this move to the fnLib_Automation, if so, push the value to this range as part of FW.TestCase_Setup
		                    			
									    gFWbln_ExitIteration = true
									 End with
									' Set cellScratch = nothing	    
								End if

								Set cellScratch = nothing
						    	
								if gFWbln_ExitIteration = true then exit do ' gFWbln_ExitIteration is reset to true at the beginning of the iteration
								
								strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
								
								' copy the PopulatedXML file from the temp folder to the input folder
								copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, BASE_XML_Template_DIR & "runs\"  ' Environment.Value("CATSDestinationFolder")
								
								
								' ZIP XML File
								ZipFile str_DataParameter_PopulatedXML_FqFileName, BASE_XML_Template_DIR & "runs\" ' Environment.Value("TESTFOLDER")
								
								GetCATSDestinationFolder Environment.Value("COMPANY"),Environment.Value("ENV"),Str_Role ' qq - this needs to move out. We need to get rid of this and read from framework (DS)
								'' msgbox Environment.Value("CATSDestinationFolder")
								copyfile left(str_DataParameter_PopulatedXML_FqFileName, len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",BASE_XML_Template_DIR & "runs\" ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
								

                                ' warn if any {data tags are left in the strPopulatedXML in the ColNr xmlPopulatedTemplate_UnResolvedTags_Count
                                                                                                
                                                                                                
'								strPopulatedXML.saveToFile(x)
'								x.copyToInboundQ
'								x.MoveToRunFolder
'

						
	                
						' so now, we :   b) submit the RunXML 
						'                b) submit the RunXML 
						'                b) submit the RunXML 
						
						
						'                  *****************
						'       then ...  (in the next stage, iterate via Role/DataPermutationRow, verify in the AUT GUI, the expected business outcome)
						'                  *****************
					Exit do	
					loop
	                End if
                    set cellNMI = nothing
	            		
	            
	            End if ' DataPermutation Rows only (i.e. not DataTemplate)
	
	


	        End If ' InScope Row
	        
	     	r_rtDataPermutation = r_rtDataPermutation + 1
	    Loop While r_rtDataPermutation <= intRowNr_LoopWS_RowNr_FinishOn ' (piWS_DataSheet.Range("rgWS_DataRow_Last_PlusOne").Row - 1)
	
	Next ' Role
	objWB_Master.save
		       


	tsAr_EventEndedAtTS(iTS_Stage_II) = now()


iTS_Max = 3 ' Ubound(tsAr_EventEndedAtTS)
For iTS = iTS_Stage_0 To iTS_Stage_III
	cellTopLeft.cells(rowTL    , colTL + iTS).value = tsAr_EventStartedAtTS(iTS)
	cellTopLeft.cells(rowTL + 1, colTL + iTS).value = tsAr_EventEndedAtTS(iTS)
	'        row             +2&3 is the duration, endTS-startTS 
	cellTopLeft.cells(rowTL + 4, colTL + iTS).value = tsAr_EventPermutationsCount(iTS)
Next


' =========  
' =========  Stage III  - very in the AUT GUI that the AUT gave a reject{RC=CatsCrCode) response  =============
' =========  

' Set dnSysDtTm = DotNetFactory.CreateInstance("System.DateTime")


tsAr_EventStartedAtTS(iTS_Stage_III) = tsAr_EventEndedAtTS(iTS_Stage_II)


cellReportStatus_FwkProcessStage.formula = "'" & "=========  Stage III  - very in the AUT GUI that the AUT gave a reject{RC=CatsCrCode) response  "
cellReportStatus_ROLE.formula = "'" 
cellReportStatus_SingleScenario.formula = "'" 
Dim intSeconds 

'While (now()-tsAr_EventStartedAtTS(iTS_Stage_II)) < DateAdd("n", 90, 0)) ' 90 seconds since the first XML was sent
do
	intSeconds = DatePart("s", now()-tsAr_EventStartedAtTS(iTS_Stage_II))
	If intSeconds >= 48 then exit do ' seconds since the first XML was sent ' qq fix seconds
	'if gFWobj_SystemDate.TimeSpan(now()-tsAr_EventStartedAtTS(iTS_Stage_II)).TotalSeconds > 60 then exit do
	wait 5 : intScratch = Extern.MessageBeep(&HFFFFFFFF) ' intScratch is 1 on success
loop

' wait 300

Dim str_MTS_Objection_Reason_Code
Dim intColNr_CatsCrCode_Actual 

' Close any open instance of the application
fnMTS_WinClose ' qq cleanup RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration

' Open MTS aplication
fnMTS_Open_wEnvVars ' qq remove following :   RunAction "A_MTS_OpenMTS [A_MTS_OpenMTS]", oneIteration

	' We need Environment.Value("COMPANY")
	' We need Environment.Value("ENV")
	' strSource = "C:\_data\GUILoader\MTS\" ' this source folder needs to be there
	' "C:\_data\GUILoader\MTS\" needs to have ini files
	
' To call the function please use the next line:
' str_MTS_Objection_Reason_Code =  fnCheckCats_MTS(strRequestId, strNMI, "", "", "")


	For iRole = 1 To intArSz_Role
	
		Str_Role = strAr_acrossA_Role(iRole)' "LNSP" ' Role parameter from framework
		cellReportStatus_ROLE.formula = "'" & Str_Role
		intColNr_ScenarioStatus = dictWSDataParameter_KeyName_ColNr("ps" & strAr_acrossA_Role(iRole))

		
		strScratch = strAr_acrossA_Role(iRole) & "_" & strSuffix_NMI ' strSuffix_Actual    strSuffix_Expected 
        intColNr_NMI = dictWSDataParameter_KeyName_ColNr(strScratch)
        set cellNMI = objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_NMI)


	    intColNr_CatsCrCode_Actual = _
	    	dictWSDataParameter_KeyName_ColNr( strAr_acrossA_Role(iRole) & objWS_DataParameter.range("rgWS_Suffix_Actual").value )

	'	iterate over all the DataPermutation Rows
	    r_rtDataPermutation = intRowNr_LoopWS_RowNr_StartOn
	    Do
	    
	    	cellReportStatus_SingleScenario.formula = "'" & r_rtDataPermutation
	    	
	    	gFwInt_DataParameterRow_ErrorsTotal = 0 : gFwInt_AbandonIteration_ReasonCount = 0
	    	
			' qq pull the gFwInt_DataParameterRow_ErrorsTotal up to the WS.DataPermutation 
            '  = gFwInt_DataParameterRow_ErrorsTotal = objWSqqq

			    	' InScope Row                
	        If ( (UCase(objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_InScope).Value) = "Y") and _
	             (objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_InScope).EntireRow.Height <> 0 ) ) _
	        Then ' InScope       and RowIsNot_Hidden_ieHeightIsZero       ' qq remove the row.Hidden part ?  or retain for flexibility
	
	           ' objWS_DataParameter.Rows(r_rtDataPermutation).Select

	          '	DataPermutation Rows only (i.e. not DataTemplate)
	            If objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_RowType).Value = cStr_rtSingleScenario Then
	            
	            
       ' qq  if the ScenarioStatus is TechFail, the exit the iteration
       ' qq  if the ScenarioStatus is TechFail, the exit the iteration
       ' qq  if the ScenarioStatus is TechFail, the exit the iteration
	            
	            	' If DataPermutation is In-Scope of the data-rules
					set cellNMI = objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_NMI)
	                If left(cellNMI.value,3) <> "nr." Then 
	                do
	                strScratch = ""
	                '	the DataPermutation-row's data-combination ** IS ** In-Scope of the data-rules

						' launch MTS application and get the reason code
						
						' str_MTS_Objection_Reason_Code = fnCheckCats_MTS(3301125155331, 6103029390, "", "", "")
						
	' ==>>>> here the GUI check is invoked	
	' ==>>>> here the GUI check is invoked	
	

						set cellScratch = objWS_DataParameter.Cells(r_rtDataPermutation, intColNr_ScenarioStatus )
						
						
						'if cellScratch.value = objWS_DataParameter.range("rgWS_DataPermutation_RunStatus_Framework_TechFail").value then 
						if cellScratch.value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_Framework_TechFail").value then 
							set cellScratch = nothing
							intScratch = Extern.MessageBeep(&HFFFFFFFF)
							wait 0, 200
							intScratch = Extern.MessageBeep(&HFFFFFFFF)
							Exit do
						End if
						set cellScratch = nothing

						cellNMI.select
						tsAr_EventPermutationsCount(iTS_Stage_III) = tsAr_EventPermutationsCount(iTS_Stage_III) + 1 ' increment the permutations count

					' old	str_MTS_Objection_Reason_Code = fnCheckCats_MTS("", cellNMI.value, "", "", "") ' first parameter is the request ID
					' ====>>>>
                        str_MTS_Objection_Reason_Code = fnCheckCats_MTS(split(cellNMI.Comment.Text , ",")(1), cellNMI.value, "", "", "") ' first parameter is the request ID

					'	Write this objection code in the actual parameter
					'	objWS_DataParameter.cells( r_rtDataPermutation, intColNr_CatsCrCode_Actual ).formula = "'" & str_MTS_Objection_Reason_Code
	
					'	Write this objection code in the actual parameter
						If trim(str_MTS_Objection_Reason_Code) = "" Then
							objWS_DataParameter.cells( r_rtDataPermutation, intColNr_CatsCrCode_Actual ).formula = "'<empty>"
						Else
							objWS_DataParameter.cells( r_rtDataPermutation, intColNr_CatsCrCode_Actual ).formula = "'" & str_MTS_Objection_Reason_Code
						End If
						
						set cellScratch = objWS_DataParameter.cells( r_rtDataPermutation, intColNr_CatsCrCode_Actual )
						
						with cellScratch
							On error resume next
								strScratch = .Comment.Text
							On error goto 0
					        .AddComment
						    .Comment.Visible = False
						    strScratch = strScratch & ",ORC Explanation[" & g_strObjectionExplanation & "],"
						    .Comment.Text strScratch
					    End With
						
'						If g_strObjectionExplanation <> "" Then
'							objWS_DataParameter.cells( r_rtDataPermutation, intColNr_CatsCrCode_Actual ).Comment.Text ",ORC Explanation[" & g_strObjectionExplanation & "],"  ' Write g_strObjectionExplanation in comments
'						Else
'							objWS_DataParameter.cells( r_rtDataPermutation, intColNr_CatsCrCode_Actual ).Comment.Text ",ORC Explanation[blank],"  ' Write g_strObjectionExplanation in comments
'						End If
'						
						
						' qq screen-capture the WHOLE set of results
						If 	str_MTS_Objection_Reason_Code = _
							objWS_DataParameter.Cells(r_rtDataPermutation, dictWSDataParameter_KeyName_ColNr("ObjectionCD")).value then
						'	set the scenario status to BusinessProcess-Pass
							' objWS_DataParameter.cells(r_rtDataPermutation, intColNr_ScenarioStatus).value = objWS_DataParameter.range("rgWS_DataPermutation_RunStatus_BusinessProcess_Pass").value
							objWS_DataParameter.cells(r_rtDataPermutation, intColNr_ScenarioStatus).value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_BusinessProcess_Pass").value
						else
							' objWS_DataParameter.cells(r_rtDataPermutation, intColNr_ScenarioStatus).value = objWS_DataParameter.range("rgWS_DataPermutation_RunStatus_BusinessProcess_Fail").value
							objWS_DataParameter.cells(r_rtDataPermutation, intColNr_ScenarioStatus).value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_BusinessProcess_Fail").value
							
						End If
			
						' qq - need to fix as hyperlinks are not worknig
						' objWS_DataParameter.Hyperlinks.Add cellActualValue		, gFW_strRunLog_ScreenCapture_fqFileName
						
					Exit do
					loop
	                End if
                    set cellNMI = nothing
                    ' qq push the gFwInt_DataParameterRow_ErrorsTotal up to the WS.DataPermutation 
                    ' qq = gFwInt_DataParameterRow_ErrorsTotal 
                    ' qq and append the reasonList to the WS.reasonList
	            
	            End if ' DataPermutation Rows only (i.e. not DataTemplate)
	
	        End If ' InScope Row
	     	r_rtDataPermutation = r_rtDataPermutation + 1
	    Loop While r_rtDataPermutation <= intRowNr_LoopWS_RowNr_FinishOn ' (piWS_DataSheet.Range("rgWS_DataRow_Last_PlusOne").Row - 1)
	
	Next ' Role		       

	fnMTS_WinClose

	objWB_Master.save
'	objWB_Master.close

tsAr_EventEndedAtTS(iTS_Stage_III) = now()



iTS_Max = 3 ' Ubound(tsAr_EventEndedAtTS)
For iTS = iTS_Stage_0 To iTS_Stage_III
	cellTopLeft.cells(rowTL    , colTL + iTS).value = tsAr_EventStartedAtTS(iTS)
	cellTopLeft.cells(rowTL + 1, colTL + iTS).value = tsAr_EventEndedAtTS(iTS)
	'        row             +2&3 is the duration, endTS-startTS 
	cellTopLeft.cells(rowTL + 4, colTL + iTS).value = tsAr_EventPermutationsCount(iTS)
Next

Set cellTopLeft = nothing

Set qtResultsOpt = Nothing ' Release the Run Results Options object
Set qtTest = Nothing ' Release the Test object 


' qq Save the UFT.RunLog to the RunFolder (or just configure the runlog.folder @ startOfScript, to the RunFolder ? - is that possible )

' ExitAction