﻿'Get the input parameters
'Company: CITI, PCOR, SAPN
'ENV: DEV, SYSTEST, UAT
'RunCount: Number of iterations
Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")
Environment.Value("RUNCOUNT")=Parameter.Item("RunCount")

'Set the folder for results
Environment.Value("TEST_RUN_SOURCE_FOLDER")=GenerateTestRunFolder

Environment.Value("DbQuery_Scenario") = "Find_MRIM_CDR"
'Load the environment details for database access and get the database queries
Loaddata

'Open MTS GUI
RunAction "A_MTS_OpenMTS [A_MTS_OpenMTS]", oneIteration

'Datamine NMI , Create as CDR and check if successful
'Do as many times as iteration count in the input parameter
For i = 1 To Environment.Value("RUNCOUNT")
	RunAction "DataMineNMI_MRIM [DataMineNMI_MRIM]", oneIteration,strNMI
	    
	RunAction "A_MTS_CreateCDRNew [A_MTS_CreateCDRNew]", oneIteration,I
	wait 25
	    
	RunAction "A_MTS_CheckCDRNew [A_MTS_CheckCDRNew]", oneIteration,strNMI
Next

'Close MTS GUI
RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration