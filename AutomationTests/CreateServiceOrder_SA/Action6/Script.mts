﻿'Get the input parameters
'Company: CITI, PCOR, SAPN
'ENV: DEV, SYSTEST, UAT
'RunCount: Number of iterations
Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")
Environment.Value("RUNCOUNT")=Parameter.Item("RunCount")
Environment.Value("DbQuery_Scenario") = "MTS_FIND_MRIM_SO"

Loaddata

Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL")&"SORD\"
'msgbox Environment.Value("TESTFOLDER") 

'Save Datamine query in a variable to use it for all iterations
Environment.Value("FindNMI") = Environment.Value("DBQuery")

strWorkType = "Supply Abolishment"
strSOPrefix = "SA"

'Open MTS GUI
RunAction "A_MTS_OpenMTS [A_MTS_OpenMTS]", oneIteration

Environment.Value("DBQuery") = Environment.Value("FindNMI")

Dim intIterationsRequiredCount : intIterationsRequiredCount = 3

For i = 1 To intIterationsRequiredCount ' Environment.Value("RUNCOUNT")
' qq why iterations ?
' no .EOF test requried because of the use of intIterationsRequiredCount when querying
	strNMI = ExecuteDatabaseQuery ' qq here the DriverSheet data_out cell would be populated
	
			Dim obj_CurrentRun, str_RunName, strReservationDesc, intReservationStatus
			'Set obj_CurrentRun = QCUtil.CurrentRun
			str_RunName = "CreateServiceOrder_SA" ' obj_CurrentRun.Name
	intReservationStatus = fnReserveDataForCase_NMI(strNMI, str_RunName, "", "", "", strReservationDesc)
					
if intReservationStatus < gDataRsvnSt_Approved then
	reporter.ReportEvent micFail , "NMI `" & strNMI & "` could not be reserved", strReservationDesc
	exittest
End if
strSQL = Environment.Value("MTS_FIND_METER")
strMySQL = replace (strSQL, "<nmi>", strNMI,1,-1,vbTextCompare)
	msgbox strMySQL
	'objRecordSet_A.MoveNext
strMeter = ExecuteSQL(strMySQL, DB_CONNECT_STR_MTS) ' ExecuteDatabaseQuery


msgbox strMeter

'Get the query for fetching LNSP
GetRole strNMI,"LNSP"
strLNSP = ExecuteDatabaseQuery

'Get the query fetching FRMP and passing it as prosretailer 
GetRole strNMI,"FRMP"
strProsRetailer = ExecuteDatabaseQuery

'Get the prospective retailer that is different to current FRMP
'GetProspectiveRetailer strFRMP
'strProsRetailer = ExecuteDatabaseQuery
'msgbox strProsRetailer

strFileName = "so_"&strNMI&"_"&datepart("d",date)&hour(time)&minute(time)&second(time)
CreateSOXML strSOPrefix,strNMI,strLNSP,strProsRetailer,strWorkType,strFileName
msgbox strFileName
wait 5

ZipFile strFileName&".xml",Environment.Value("TESTFOLDER")

CopyFile strFileName&".zip",Environment.Value("TESTFOLDER"),Environment.Value("MTS_SO_OUTBOX")

' check ack file generated
wait 20
expectedACKFileName=strFileName&".ack"
ackDestinationFolder = GetServiceOrderAckDestinationFolder(Environment.Value("COMPANY"), Environment.Value("ENV"))
If Right(ackDestinationFolder,1) <> "\" Then
    ackDestinationFolder=ackDestinationFolder & "\"
End If

Reporter.ReportEvent micPass, "ack file location", ackDestinationFolder &expectedACKFileName
Dim ackFile
Set ackFile=CreateObject("Scripting.FileSystemObject")
If (ackFile.FileExists(ackDestinationFolder &expectedACKFileName )) Then
    Reporter.ReportEvent micPass, "Ack file generated", "Ack  file generated:" & ackDestinationFolder &expectedACKFileName
    'check ack file has text 'status="Accept" 'in the file
    If (TextExistsInFile(ackDestinationFolder &expectedACKFileName, "status=" & Chr(34) & "Accept" & Chr(34))) AND NOT (TextExistsInFile(ackDestinationFolder &expectedACKFileName, "status=" & Chr(34) & "Reject" & Chr(34)))Then
        Reporter.ReportEvent micPass, "Ack file check for text 'status=" & Chr(34) &"Accept" & Chr(34) & "'", "Found text in ack file"
    Else
        Reporter.ReportEvent micFail, "Ack file check for text 'status=" & Chr(34) &"Accept" & Chr(34) & "'", "Text NOT found in ack file"
    End If
Else 
    Reporter.ReportEvent micWarning, "Ack file generated", "Ack  file NOT generated at:" & ackDestinationFolder &expectedACKFileName
End If
	    
RunAction "A_MTS_SearchSO [A_MTS_Search_SO]", oneIteration,strNMI
wait 10
	    
'Close MTS GUI
RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration

Next