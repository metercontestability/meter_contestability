﻿'Get the input parameters
RunAction "A_MTS_SearchSO [GUITest7]", oneIteration
'Company: CITI, PCOR, SAPN
'ENV: DEV, SYSTEST, UAT
'RunCount: Number of iterations
Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")
Environment.Value("RUNCOUNT")=Parameter.Item("RunCount")

Set uftapp = CreateObject("QuickTest.Application") 
strTest = uftapp.Test.Name

Environment.Value("TESTFOLDER") = "H:\Automation\Meter Contestability\Folders\"&"DataSheets\"&Environment.Value("COMPANY")&"\"&strTest&"\"
msgbox Environment.Value("TESTFOLDER")
'SetTestFolder strTest

'Set the folder for results
Environment.Value("TEST_RUN_SOURCE_FOLDER")=GenerateTestRunFolder

Environment.Value("DbQuery_Scenario") = "Find_MRIM_CDR"

'Load the environment details for database access and get the database queries
Loaddata

strWorkType = "Re-energisation"
strSOPrefix = "RN_"


strProposedDate = date


For i = 1 To Environment.Value("RUNCOUNT")

strNMI = ExecuteDatabaseQuery
'msgbox strNMI
'strNMI = "61021530027"


'Get the query for fetching LNSP
GetRole strNMI,"FRMP"
strFRMP = ExecuteDatabaseQuery

'Get the query for fetching LNSP
GetRole strNMI,"LNSP"
strLNSP = ExecuteDatabaseQuery

'Get the query for fetching MDP
'GetRole strNMI,"MDP"
'strMDP = ExecuteDatabaseQuery

''Get the query for fetching RP
'GetRole strNMI,"RP"
'strRP = ExecuteDatabaseQuery'

'CATS CR Code
'strCRcode = "1040"

'Get the prospective retailer that is different to current FRMP
GetProspectiveRetailer strFRMP
strProsRetailer = ExecuteDatabaseQuery



strFileName = "so_"&strNMI&"_"&datepart("d",date)&hour(time)&minute(time)&second(time)
CreateSOXML strSOPrefix,strNMI,strLNSP,strRetailer,strWorkType,strFileName
msgbox strFileName
ZipFile strFileName&".xml",Environment.Value("TESTFOLDER")

'Create xml REQ LNSP
'strRole = "LNSP"
'strChangeStatusCode = "REQ"
'CreateSOXML strSOPrefix,strNMI,strLNSP,strRetailer,strWorkType,strFileName
'msgbox strFileName
'XMLCreateFile = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\DataSheets\CITI\CreateCATS1040\CATSIN_new.xml"
''
''xmlDoc.Save(XMLCreateFile)

CopyFile strFileName&".zip",Environment.Value("TESTFOLDER"),"\\corpvmcmtst02\Data\CITIPP\b2b\outbox\"

GetServiceOrderAckDestinationFolder "CITI","SYSTEST"

'TextExistsInFile strFileName&".ack", "status=Accept"

TextExistsInFile "\\corpvmcmtst02\Data\CITIPP\b2b\inbox\so_61020045521_7145828.ack", "status=Accept"

SystemUtil.Run "C:\Program Files (x86)\MTS_GUI - 1.6\mts.exe"



Next